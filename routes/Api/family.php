<?php

namespace App\Http\Controllers\Api\Family;

use App\Http\Controllers\Api\FamilyController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::group(['middleware' => ['auth:api_family','jwt.auth']], function () {
        Route::get('profile', [AuthController::class, 'profile']);
        Route::put('profile', [AuthController::class, 'updateProfile']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('create_product', [ProductController::class, 'storeProductByFamily']);
    });
});

Route::group(['middleware' => ['jwt.auth','auth:api_family']], function () {
    Route::resource('carts',CartController::class);
    Route::resource('family-products',ProductController::class);
});

Route::post('create_family',[FamilyController::class,'createFamily']);
