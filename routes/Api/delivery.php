<?php

namespace App\Http\Controllers\Delivery;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::group(['middleware' => ['auth:Delivery','jwt.auth']], function () {
        Route::get('profile', [AuthController::class, 'profile']);
        Route::put('profile', [AuthController::class, 'updateProfile']);
        Route::post('logout',[AuthController::class,'logout']);

    });
});

Route::group(['middleware' => ['auth:Delivery', 'jwt.auth']], function () {
    Route::get('orders', [OrdersController::class, 'index']);
    Route::post('orders/{cart}', [RequestsController::class, 'store']);
    Route::get('requests', [RequestsController::class, 'index']);
    Route::get('requests/{request}', [RequestsController::class, 'show']);
    Route::post('requests/pick-up/{deliveryRequest}', [RequestsController::class, 'pickUpOrder']);
    Route::post('requests/finish/{deliveryRequest}', [RequestsController::class, 'finishOrder']);
});
