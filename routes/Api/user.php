<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//this route used for sending notifications from mobile

Route::post('notify',SendNotificationController::class);

Route::group(['prefix' => 'auth'], function () {
    Route::post('register', [AuthController::class, 'register']);
    Route::post('login', [AuthController::class, 'login']);
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::get('profile', [AuthController::class, 'profile']);
        Route::put('profile', [AuthController::class, 'updateProfile']);
        Route::post('logout',[AuthController::class,'logout']);
    });
    Route::post('confirmation',[AuthController::class,'confirmation']);
    Route::post('confirmation/resend',[AuthController::class,'resendConfirmation']);
    Route::post('forget-password',ForgetPasswordController::class);
});
Route::group(['middleware' => 'jwt.check'], function () {
    Route::post('contact',ContactController::class);
    Route::get('home', HomeController::class);
    Route::get('settings',SettingsController::class);
    Route::get('cities',CityController::class);
    Route::resource('products', ProductController::class);
    Route::resource('sub-categories', SubCategoryController::class);
    Route::get('families-by-category/{category}',[FamilyController::class,'familyByCategory']);
    Route::get('families-by-sub-category/{sub_category}',[FamilyController::class,'familyBySubCategory']);
    Route::get('families',[FamilyController::class,'index']);
    Route::post('coupon/check',CouponController::class);
    Route::get('family/products/{family}',[ProductController::class,'ProductsByFamily']);
    Route::group(['middleware' => ['jwt.auth', 'auth:api']], function () {
        Route::get('notifications',NotificationController::class);
        Route::resource('addresses', AddressController::class);
        Route::post('like/{product}',[LikeController::class,'like']);
        Route::get('likes',[LikeController::class,'index']);
        Route::resource('carts',CartController::class);
        Route::post('rate/product/{cartItem}',[RateController::class,'product']);
        Route::post('rate/family/{cart}',[RateController::class,'family']);
    });
    Route::post('fees/{family}',FeesController::class);
});
