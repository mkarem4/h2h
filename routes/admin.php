<?php
namespace  App\Http\Controllers\Dashboard;
use App\Http\Controllers\Dashboard\Subscriptions\PlanController;
use App\Http\Controllers\Dashboard\Subscriptions\SubscriptionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});



Route::group(['as'=>'admin.','prefix'=>'dashboard/admin','middleware' => 'auth:Admin'],function (){
    Route::get('/',[IndexController::class,'index'])->name('index');
    Route::get('print/orders/{order}',[OrderController::class,'print'])->name('order.print');
    Route::resources([
        'admins'=>AdminController::class,
        'families'=>FamilyController::class,
        'categories'=>CategoryController::class,
        'sub-categories'=>SubCategoryController::class,
        'products'=>ProductController::class,
        'product-colors'=>ProductColorController::class,
        'product-sizes'=>ProductSizeController::class,
        'deliveries'=>DeliveryController::class,
        'banners'=>BannerController::class,
        'coupons'=>CouponController::class,
        'orders'=>OrderController::class,
        'users'=>UserController::class,
        'cities'=>CityController::class,
        'contacts'=>ContactController::class,
        'notifications'=>NotificationController::class,
        'roles'=>RoleController::class,
        'plans'=>PlanController::class,
        'settings'=>SettingsController::class,
        'supported-cities'=>SupportedCityController::class
    ]);
    Route::put('delivery/assign/{cart}',[DeliveryController::class,'assignOrder'])->name('delivery.assign');
    Route::get('login/{family}',[FamilyController::class,'login'])->name('families.login');
    Route::post('subscription/{family}/{type}',[SubscriptionController::class,'subscription'])->name('subscription');
    Route::post('subscription/{family}',[SubscriptionController::class,'updateSubscription'])->name('update.subscription');
    Route::get('gallery-destroy/{media}',[ProductController::class,'destroyGallery'])->name('gallery.destroy');
    Route::get('nearly-ended-subscriptions',[SubscriptionController::class,'nearlyEnded'])->name('nearlyEnded');
    Route::get('roles/{role}',[RoleController::class,'changeStatus'])->name('active.role');
    Route::get('product-size-default/{product}/{productSize}',[ProductSizeController::class,'toggleDefault'])->name('product-size-toggle');
    Route::get('rate/product',[RateController::class,'product'])->name('rate.product');
    Route::get('rate/family',[RateController::class,'family'])->name('rate.family');
    Route::get('orders/{order}/{status}',[OrderController::class,'changeStatus'])->name('order.change.status');
    Route::view('tracking','dashboard.deliveries.tracking')->name('deliveries.tracking');
});

