<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Family\Auth\LoginController;
use Route;

Route::group(['namespace' => 'Family'], function () {
    Route::get('/', [HomeController::class, 'index'])->name('family.dashboard');

    // Login
    Route::get('login', [LoginController::class, 'showLoginForm'])->name('family.login');
    Route::post('login', [LoginController::class, 'login']);
    Route::post('logout', [LoginController::class, 'logout'])->name('family.logout');


    // Passwords
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('family.password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('family.password.request');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('family.password.reset');

});

Route::group(['middleware' => 'auth:family', 'as' => 'family.'], function () {
    Route::get('deliveries', [DeliveryController::class, 'index'])->name('delivery.index');
    Route::get('gallery-destroy/{media}', [ProductController::class, 'destroyGallery'])->name('gallery.destroy');

    Route::resource('products', ProductController::class);
//        ->except('create', 'store');
    Route::resources([
        'product-colors' => ProductColorController::class,
        'product-sizes' => ProductSizeController::class,
        'orders' => OrderController::class,
        'profile' => ProfileController::class
    ]);
    Route::get('product-size-default/{product}/{productSize}', [ProductSizeController::class, 'toggleDefault'])->name('product-size-toggle');
    Route::get('orders/{order}/{status}', [OrderController::class, 'changeStatus'])->name('order.change.status');
    Route::get('/print', [HomeController::class, 'print'])->name('print');
});
