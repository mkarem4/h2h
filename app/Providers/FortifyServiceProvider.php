<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;

class FortifyServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Fortify::viewPrefix('admin');
        Fortify::authenticateUsing(function ($request){
            $auth=auth('Admin');
            $request->validate(['email'=>'required|email','password'=>'required']);
            $credentials=$request->only('email','password');
            if (!$auth->attempt($credentials)){
                return ;
            }
            return  $auth->user();
        });
        Fortify::loginView(function (){
            return view('dashboard.layout.login');
        });
        Fortify::resetPasswordView(function (){
            return view('reset-password.reset-password');
        });
       /* Fortify::createUsersUsing(CreateNewUser::class);
        Fortify::updateUserProfileInformationUsing(UpdateUserProfileInformation::class);
        Fortify::updateUserPasswordsUsing(UpdateUserPassword::class);
        Fortify::resetUserPasswordsUsing(ResetUserPassword::class);*/
    }
}
