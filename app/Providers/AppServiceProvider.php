<?php

namespace App\Providers;

use App\Models\Cart;
use App\Observers\CartObserver;
use App\services\DatabaseMacros;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        \Schema::defaultStringLength(191);

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Cart::observe(CartObserver::class);
        Blueprint::macro('address',function (){
            $this->text('address');
            $this->double('lat');
            $this->double('lng');
        });
    }
}
