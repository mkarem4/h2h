<?php


namespace App\Traits;


trait filterDate
{
    public function scopeDateFilter($query, $from, $to)
    {
        $query->whereDate('created_at', '>=', $from)->whereDate('created_at', '<=', $to);
    }
}
