<?php

namespace App\Traits;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

trait ApiResponse
{
    public $paginateNumber = 10;

    public function apiResponse($data = null, $error = null, array $extra = [])
    {
        $array = [
            'status' => $error === null,
            ($error ? 'msg' : 'data') => $error ?? $data,

        ];
        if (count($extra) > 0) {
            $array['data'] = $extra;
        }

        return response($array, 200);
    }

    public function success($data)
    {
        return response()->json(['status'=>true,'data'=>$data]);
    }

    public function error($data)
    {
       return response()->json(['status'=>false,'msg'=>$data]);
    }

    public function createdResponse($data)
    {
        $data = [
            'message' => 'تم الاضافه بنجاح',
            'data' => $data,
        ];

        return $this->apiResponse($data);
    }

    public function deleteResponse()
    {
        return $this->apiResponse(true);
    }

    public function notFoundResponse()
    {
        return $this->apiResponse(null, __('messages.not_found'));
    }

    public function unKnowError()
    {
        return $this->apiResponse(null, 'Un know error');
    }

    public function userSuspend($msg)
    {

        $array = [
            'value' => false,
            'msg' => $msg,
        ];

        return response($array, 420);
    }

    public function apiValidation($request, $array)
    {

        $validate = Validator::make($request->all(), $array);

        $errors = [];

        if ($validate->fails()) {
            foreach ($validate->getMessageBag()->toArray() as $key => $messages) {
                $errors[$key] = $messages[0];

                return $this->apiResponse(null, $errors[$key]);
                break;
            }
        }
    }

    /**
     * @param $items
     * @param  null  $page
     * @param  array  $options
     * @return LengthAwarePaginator
     */
    public function CollectionPaginate($items, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        $items = $items->values();

        return new LengthAwarePaginator($items->forPage($page, $this->paginateNumber)->values(), $items->count(),
            $this->paginateNumber, $page, $options);
    }


}
