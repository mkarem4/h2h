<?php

namespace App\DataTables;

use App\Models\User;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('created_at',function ($q){
                return $q->created_at->toDateTimeString();
            })
            ->editColumn('gender',function ($q){
                return $q->gender=='male'?'ذكر':'انثي';
            })
            ->addColumn('action', 'dashboard.users.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(User $model)
    {
        return $model->withCount(['carts'=>function($q){
            $q->where('status','finished');
        }])->withSum(['carts'=>function($q){
            $q->where('status','finished');
        }],'grand_total')->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('user-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(5,'desc')
            ->buttons(
                Button::make('create'),
                Button::make('export'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload')
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('id')->title('#'),
            Column::make('name')->title('الاسم'),
            Column::make('phone')->title('الجوال'),
            Column::make('carts_count')->title('عدد الطلبات')->searchable(false),
            Column::make('carts_sum_grand_total')->title('اجمالي المدفوعات')->searchable(false),
            Column::make('created_at')->title('تاريخ الانشاء'),
            Column::computed('gender')->title('الجنس'),
            Column::make('birth_date')->title('تاريخ الميلاد'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
