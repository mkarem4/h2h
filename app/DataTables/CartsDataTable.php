<?php

namespace App\DataTables;

use App\Models\Cart;
use Carbon\Carbon;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CartsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('delivery_name',function ($q){
                return @$q->deliveries->reverse()->first()->name;
            })->editColumn('created_at',function ($q){
                return $q->created_at->toDateTimeString();
            })->editColumn('status',function ($q){
                return __($q->status);
            })->editColumn('date',function ($q){
                return Carbon::parse($q->date)->toDateTimeString();
            })
            ->addColumn('action', 'dashboard.orders.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param Cart $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Cart $model)
    {
        $carts=$model->with('user','family')->newQuery();
        if (request()->has('status')) {
            $carts->whereIn('status', request('status'));
        }
        if (request()->has('from') and request()->has('to') and request('from') != '' and request('to') != '') {
            $carts->whereDate('created_at', '>=', request('from'))->whereDate('created_at', '<=', request('to'));
        }
        $carts->when($this->request->user_id,function ($q){
            $q->where('user_id',request('user_id'));
        });
        $carts->when($this->request->family_id,function ($q){
            $q->where('family_id',request('family_id'));
        });
        $carts->when($this->request->delivery_id,function ($q){
            $q->whereHas('deliveries',function ($q){
                $q->where('delivery_id',request('delivery_id'));
            });
        });
        $carts->when($this->request->coupon_id,function ($q){
            $q->where('coupon_id',$this->request->coupon_id);

        });
        return $carts->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('carts-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->buttons(
                        Button::make('excel'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('id')->title('رقم الطلب'),
            Column::make('delivery_name')->searchable(false)->orderable(false)->title('اسم السائق'),
            Column::make('family.name')->title('الاسرة'),
            Column::make('user.name')->title('العميل'),
            Column::make('user.phone')->title('جوال العميل'),
            Column::make('date')->title('تاريخ الطلب'),
            Column::make('status')->title('الحالة'),
            Column::make('grand_total')->title('الاجمالي'),
            Column::make('created_at')->title('تاريخ الانشاء'),
            Column::computed('action')->title('العمليات')
                ->exportable(false)
                ->printable(false)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Carts_' . date('YmdHis');
    }
}
