<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('rate',function ($q){
                return round($q->rate,2);})
            ->addColumn('sizes',function ($product){
                return view('dashboard.products.sizes.button',compact('product'));
            })
            ->addColumn('colors',function ($product){
                return view('dashboard.products.colors.button',compact('product'));
            })
            ->addColumn('actions',function ($product){
                return view('dashboard.products.actions',compact('product'));
            })

            ;
    }

    /**
     * Get query source of dataTable.
     *
     * @param Product $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Product $model)
    {
        return Product::with('family','sub_category','sub_category.category')->latest();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html()
    {
        return $this->builder()
            ->setTableId('products-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(0)
            ->buttons(
                Button::make('create'),
                Button::make('excel'),
                Button::make('print'),
                Button::make('reset'),
                Button::make('reload'),
            );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('id')->title('#')->orderable(true),
            Column::make('name')->title('اسم المنتج'),
            Column::make('info')->title('الوصف')->orderable(false),
            Column::make('family.name')->title('الاسرة')->orderable(false),
            Column::make('sub_category.category.name')->title('القسم الرئيسي')->orderable(false),
            Column::make('sub_category.name')->title('القسم الفرعي')->orderable(false),
            Column::computed('sizes')->title('المقاسات')->printable(false),
            Column::computed('colors')->title('الالوان')->printable(false),
            Column::computed('rate')->title('التقييم')->orderable(true),
            Column::computed('actions')->title('العمليات')->printable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Products_' . date('YmdHis');
    }
}
