<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\View\View;

class Select extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    public $title;
    public $items;
    /**
     * @var bool
     */
    public $float;

    public function __construct($title,$name,$items,$float=true
    )
    {
        $this->name=$name;
        $this->title=$title;
        $this->items=$items;
        $this->float = $float;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        return view('components.select');
    }
}
