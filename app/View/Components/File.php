<?php

namespace App\View\Components;

use Illuminate\View\Component;

class File extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    public $title;
    public $is_float;
    public function __construct($title,$name,$is_float=false)
    {
        $this->name=$name;
        $this->title=$title;
        $this->is_float=$is_float;
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return view('components.file');
    }
}
