<?php

namespace App\View\Components;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use Rinvex\Subscriptions\Models\Plan;

class Plans extends Component
{
    private $model;

    /**
     * Create a new component instance.
     *
     * @return void
     */

    public function __construct($model=null)
    {
        //
        $this->model = $model;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return View|string
     */
    public function render()
    {
        $plans=Plan::all()->mapWithKeys(function ($q){
            return [$q['id']=>$q['name']];
        });
        return view('components.plans')->with('plans',$plans)->with('model',$this->model);
    }
}
