<?php


namespace App\services;


use Illuminate\Database\Schema\Blueprint;

class DatabaseMacros
{
    function address(){
        return function (){
            Blueprint::macro('address',function (){
                $this->text('address');
                $this->double('lat');
                $this->double('lng');
            });
        };
    }

}
