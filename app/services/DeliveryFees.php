<?php


namespace App\services;


use App\Models\Family;
use Arr;

class DeliveryFees
{
    private $lat;
    private $lng;
    /**
     * @var Family
     */
    private $family;
    private $values;

    public function __construct(Family $family, $lat, $lng,$values)
    {
        $this->lat = $lat;
        $this->lng = $lng;
        $this->family = $family;
        $this->values = $values;
    }

    public function deliveryFees()
    {
        $family = $this->family;
        $settings = $this->values;
        $min = Arr::get($settings, 'min_delivery', 18);
        $max = Arr::get($settings, 'max_delivery', 40);
        $km_cost = Arr::get($settings, 'km_cost', 0.5);
        $distance = $this->vincentyGreatCircleDistance($family->lat, $family->lng, $this->lat, $this->lng);
        $cost = $distance * $km_cost;
        if ($cost <= $min) {
            return $min;
        } elseif ($cost >= $max) {
            return $max;
        } elseif ($cost >= $min && $cost <= $max) {
            return $cost;
        }else{
            return  0;
        }
    }

    public function getDistance()
    {
        return $this->vincentyGreatCircleDistance($this->family->lat,$this->family->lng,$this->lat,$this->lng);
}

    /**
     * Calculates the great-circle distance between two points, with
     * the Vincenty formula.
     * @param float $latitudeFrom Latitude of start point in [deg decimal]
     * @param float $longitudeFrom Longitude of start point in [deg decimal]
     * @param float $latitudeTo Latitude of target point in [deg decimal]
     * @param float $longitudeTo Longitude of target point in [deg decimal]
     * @param float $earthRadius Mean earth radius in [m]
     * @return float Distance between points in [m] (same as earthRadius)
     */
    private function vincentyGreatCircleDistance(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }
}
