<?php

namespace App\Http\Requests\Family;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UpdateCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $cart=$this->route('cart');
        return $cart->family_id==auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cart=$this->route('cart');
       switch ($cart->status){
           case 'pending':
               return [
                   'status'=>'required|in:accepted,rejected',
                   'reject_reason'=>'sometimes|string|required_if:status,rejected'
               ];
               break;
           case 'accepted':
               return ['status'=>'required|in:wait_for_delivery'];
               break;
           default:
               throw ValidationException::withMessages(['خطا في نوع حالة الطلب ']);
       }
    }
}
