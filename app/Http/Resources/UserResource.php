<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'token'=>$this->token,
            'image'=>$this->image,
            'is_confirmed'=>$this->is_confirmed,
            'city'=>$this->whenLoaded('city',[
                'id'=>$this->city_id,
                'name'=>optional($this->city)->name
            ]),
            'birth_date'=>$this->birth_date,
            'gender'=>$this->gender
        ];
    }
}
