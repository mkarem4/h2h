<?php

namespace App\Http\Resources\Cart;

use App\Models\Cart;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CartResource
 * @package App\Http\Resources\Cart
 * @mixin Cart
 */
class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $family=$this->family;
        $user=$this->user;
        $address=$this->address;
        $deliveryRequest=$this->delivery();
        $data= [
            'id'=>$this->id,
            'status'=>$this->status,
            'reject_reason'=>$this->when($this->reject_reason,$this->reject_reason),
            'payment'=>$this->payment,
            'date'=>$this->date,
            'created_at'=>$this->created_at->toDateTimeString(),
            'family'=>[
                'id'=>$family->id,
                'image'=>$family->image,
                'name'=>$family->name,
                'rate'=>$family->rates->avg('value'),
                'address'=>$family->address,
                'city_id'=>$family->city_id,
                'lat'=>$family->lat,
                'lng'=>$family->lng
            ],
            'user'=>[
                'id'=>$user->id,
                'name'=>$user->name,
                'phone'=>$user->phone
            ],
            'address'=>[
                'id'=>$address->id,
                'address'=>$address->address,
                'lat'=>$address->lat,
                'lng'=>$address->lng
            ],
            'total'=>$this->total,
            'tax'=>$this->tax,
            'delivery_cost'=>$this->delivery_cost,
            'discount'=>$this->discount,
            'grand_total'=>$this->grand_total,
            'items'=>CartItemResource::collection($this->items),
            'notes'=>$this->notes,
            'logs'=>$this->logs->transform(function ($q){
                return['status'=>$q->status,'created_at'=>$q->created_at->toDateTimeString()];
            }),
            'is_rated'=>$this->rate_exists
        ];
        if ($deliveryRequest){
            $delivery=$deliveryRequest->delivery;
            $data['delivery']=[
                'id'=>$delivery->id,
                'name'=>$delivery->name,
                'phone'=>$delivery->phone,
                'image'=>$delivery->image,
            ];
        }

        return  $data;
    }
}
