<?php

namespace App\Http\Resources\Cart;

use Illuminate\Http\Resources\Json\JsonResource;

class CartItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product=$this->product;
        $size=$this->size;
        $color=optional($this->color);
        return [
            'id'=>$this->id,
            'quantity'=>$this->quantity,
            'product'=>[
                'id'=>$product->id,
                'name'=>$product->name,
                'image'=>$product->image
            ],
            'size'=>[
                'id'=>$size->id,
                'name'=>$size->name,
                'price'=>$size->price,
                'discount'=>$size->discount,
                'final_price'=>$size->final_total,
            ],
            'color'=>[
                'id'=>$color->id,
                'name'=>$color->name,
                'code'=>$color->code
            ]
        ];
    }
}
