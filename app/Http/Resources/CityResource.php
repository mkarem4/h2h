<?php

namespace App\Http\Resources;

use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CityResource
 * @package App\Http\Resources
 * @mixin City
 */
class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'delivery_cost'=>$this->delivery_cost,
            'supported_cities'=>$this->supportedCities->transform(function ($q){
                return  [
                    'id'=>$q->id,
                    'name'=>$q->name,
                    'delivery_cost'=>$q->pivot->delivery_cost
                ];
            })
        ];
    }
}
