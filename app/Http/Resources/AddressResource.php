<?php

namespace App\Http\Resources;

use App\Models\Address;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use stdClass;

/**
 * Class AddressResource
 * @package App\Http\Resources
 * @mixin Address
 */
class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'lat'=>$this->lat,
            'lng'=>$this->lng,
            'address'=>$this->address,
            'city'=>$this->city?new CityResource($this->city):new stdClass()
        ];
    }
}
