<?php

namespace App\Http\Resources;

use App\Http\Resources\SubCategory\SubCategoryResource;
use App\Models\Family;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class FamilyResource
 * @package App\Http\Resources
 * @mixin Family
 */
class FamilyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'email'=>$this->email,
            'phone'=>$this->phone,
            'is_online'=>$this->is_online,
            'image'=>$this->image,
            'address'=>$this->address,
            'bio'=>$this->bio,
            'lat'=>$this->lat,
            'lng'=>$this->lng,
            'order_type'=>$this->order_type == 'on_delivery' ? 'عند الطلب' : 'تسليم فوري',
            'city'=>[
                'id'=>$this->city_id,
                'name'=>$this->city->name
            ],
            'rate'=>$this->rates_avg_value,
            'subCategories'=>SubCategoryResource::collection($this->subCategories->unique()),
            'token'=>$this->when($this->token,$this->token),
            'logout'=>$this->when($this->logout,$this->logout)
        ];
    }
}
