<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\SubCategory\SubCategoryResource;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'info'=>$this->info,
            'image'=>$this->image,
            'is_active'=>$this->is_active,
            'images'=>$this->getMedia('images')->transform(function ($q){
                return $q->getUrl();
            }),
            'rate'=>$this->rates_avg_value,
            'is_liked'=>$this->is_liked,
            'family'=>[
                'id'=>$this->family_id,
                'name'=>$this->family->name,
                'image'=>$this->family->image,
                'city_id'=>$this->family->city_id,
                'bio'=>$this->family->bio,
                'rate'=>$this->family->rates()->avg('value')
            ],
            'sub_category'=>new SubCategoryResource($this->sub_category),
            'sizes'=>SizeResource::collection($this->sizes),
            'colors'=>ColorResource::collection($this->colors),
            'default_size'=>[
                'id'=>$this->default_size->id,
                'name'=>$this->default_size->name,
                'price'=>$this->default_size->price,
                'discount'=>$this->default_size->discount,
                'final_price'=>$this->default_size->final_total,
            ]
        ];
    }
}
