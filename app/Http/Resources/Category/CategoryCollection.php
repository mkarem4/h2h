<?php

namespace App\Http\Resources\Category;

use App\Http\Resources\SubCategory\SubCategoryCollection;
use Illuminate\Http\Resources\Json\ResourceCollection;
use phpDocumentor\Reflection\Types\This;

class CategoryCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public $collects =  CategoryResource::class;
    public function toArray($request)
    {
        return parent::toArray($request);
    }


}
