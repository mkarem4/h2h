<?php

namespace App\Http\Resources\Category;

use App\Http\Resources\SubCategory\SubCategoryCollection;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'image' => $this->image,
            'name' => $this->name,
            'description' => $this->description,
            'sub_categories' => new SubCategoryCollection($this->sub_categories)
        ];
    }
}
