<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryRequestResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'cart_status'=>$this->cart->status,
            'request_status'=>$this->status,
            'cart_id'=>$this->cart_id,
            'payment'=>$this->cart->payment,
            'date'=>$this->cart->date,
            'family'=>[
                'id'=>$this->cart->family->id,
                'name'=>$this->cart->family->name,
                'phone'=>$this->cart->family->phone,
                'location'=>[
                    'lat'=>$this->cart->family->lat,
                    'lng'=>$this->cart->family->lng,
                    'address'=>$this->cart->family->address,
                ]
            ],
            'user'=>[
                'id'=>$this->cart->user->id,
                'name'=>$this->cart->user->name,
                'phone'=>$this->cart->user->phone,
                'location'=>[
                    'lat'=>$this->cart->address->lat,
                    'lng'=>$this->cart->address->lng,
                    'address'=>$this->cart->address->address,
                ]
            ],
            'grand_total'=>$this->cart->grand_total,
            'items'=>$this->cart->items->count(),
            'created_at'=>$this->created_at->toDateTimeString()
        ];
    }
}
