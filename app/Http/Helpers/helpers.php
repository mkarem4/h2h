<?php

use App\Models\Category;
use App\Models\City;
use App\Models\Coupon;
use App\Models\Delivery;
use App\Models\Family;
use App\Models\Setting;
use App\Models\SubCategory;
use App\Models\User;
use Illuminate\Validation\ValidationException;


/**
 * Get Image
 *
 * @param $filename
 * @return string
 */
function getimg($filename)
{
    if (!empty($filename)) {
        $base_url = url('/');
        return $base_url . $filename;

    } else {
        return '';
    }
}

/**
 * Upload an image
 *
 * @param $value
 * @param string $path
 * @return bool|false|string
 */
function uploader($value, $path = 'uploads')
{
    return '/storage/' . Storage::disk('public')->putFile($path, $value);
}

function applyCoupon($code,$family=null)
{
    $coupon = Coupon::valid($code)->first();
    throw_if(!$coupon, ValidationException::withMessages(['لقد انتهت صلاحية الكوبون !']));
    if ($family){
        if ($coupon->families()->exists()){
            throw_if(!$coupon->families()->where('families.id',$family)->exists() ,ValidationException::withMessages(['عفوا لا يمكن استخدام الكوبون مع هذة الاسرة برجاء المحاولة لاحقاً']));
        }
    }
    return $coupon;
}

function getSetting($name)
{
    $setting = Setting::where('name', $name)->first();
    if (!$setting) {
        return "";
    }

    return strip_tags($setting->ar_value);
}

function categories()
{
    return Category::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}
function deliveries()
{
    return Delivery::with('city')->get()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name'] . '|'.$q->city->name];
    });
}
function users()
{
    return User::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}

function families()
{
    return Family::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}

function sub_categories()
{
    return SubCategory::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}

class responder
{
    public static function success($data)
    {
        return response()->json(['status' => true, 'data' => $data]);
    }

    public static function error($data)
    {
        return response()->json(['status' => false, 'msg' => $data]);
    }
}

function cities()
{
    return City::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}

function coupons()
{
    return Coupon::all()->mapWithKeys(function ($q) {
        return [$q['id'] => $q['name']];
    });
}


function orderStatuses()
{
    return ["pending" => "معلق",
        "canceled" => "ملغي",
        "accepted" => "مقبول",
        "rejected" => "مرفوض",
        "wait_for_delivery" => "في انتظار التوصيل",
        "on_deliver" => "جاري التوصيل",
        "finished" => "انتهي",
        'force_cancel'=>'الغاء من الادارة',
        'force_finish'=>'انتهي بواسطة الادارة'

    ];
}

function sendSMS($phone, $message)
{
    /*https://api.goinfinito.me/unified/v2/send?clientid=bassmahx00lcmw597yb509he&clientpassword=
omtis851xczmh80mabov1g3rf5x47min&to=966594256396&from=BASSMAHAPP&text=this*/

    $request = Http::get('https://api.goinfinito.me/unified/v2/send', [
        'clientid' => 'bassmahx00lcmw597yb509he',
        'clientpassword' => 'omtis851xczmh80mabov1g3rf5x47min',
        'to' => $phone,
        'from' => 'BASSMAHAPP',
        'text' => $message
    ]);
    info($request->body());
}
