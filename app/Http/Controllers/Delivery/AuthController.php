<?php

namespace App\Http\Controllers\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\Delivery;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use JWTAuth;
use responder;

class AuthController extends Controller
{
    private $auth;

    public function __construct()
    {
        $this->auth = auth('Delivery');
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'phone' => 'required|unique:deliveries,phone|numeric',
            'email' => 'required|unique:deliveries|email',
            'password' => 'required',
            'fcm_token_android' => 'required_without:fcm_token_ios',
            'fcm_token_ios' => 'required_without:fcm_token_android',
            'image' => 'required|image'
        ];
        $request->validate($rules);
        $inputs = $request->all();

        $user = Delivery::create($inputs);
        $user->token = JWTAuth::fromUser($user);
        return responder::success(new UserResource($user));
    }


    public function login(Request $request)
    {
        $request->validate([
            "phone" => 'required|exists:deliveries,phone',
            'password' => 'required',
            'fcm_token_android' => 'required_without:fcm_token_ios',
            'fcm_token_ios' => 'required_without:fcm_token_android',
        ]);
        $credentials = request(['phone', 'password']);
        $attemp = $this->auth->attempt($credentials);
        if (!$attemp) {
            return  responder::error('خطأ في بيانات الدخول !');
        }
        $user = $this->auth->user();
        $user->update($request->only('fcm_token_android', 'fcm_token_ios'));
        $user->token = JWTAuth::fromUser($user);
        return responder::success(new UserResource($user));
    }

    public function profile()
    {
        $user = $this->auth->user();
        $user->token = JWTAuth::getToken()->get();
        return responder::success(new UserResource($user));
    }

    public function updateProfile(Request $request)
    {
        $user = $this->auth->user();
        $id = $user->id;
        $rules = [
            'phone' => 'sometimes|numeric|unique:deliveries,phone,' . $id,
            'password' => 'sometimes',
            'email' => 'sometimes|email|unique:deliveries,email,' . $id,
            'name' => 'sometimes|string',
            'image' => 'sometimes|image',
            'fcm_token_android' => 'sometimes',
            'fcm_token_ios' => 'sometimes',
        ];

        $request->validate($rules);
        $inputs = $request->all();
        $user->update($inputs);
        $user->token = JWTAuth::fromUser($user);

        return responder::success(new UserResource($user));
    }

    public function confirmation(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('confirmation_code', $request->code);
        if (!$user->exists()) {
            return $this->apiResponse(null, __('wrong code'));
        }
        $user->first()->update([
            'is_confirmed' => 1,
            'confirmed_at' => now()->toDateTimeString(),
            'confirmation_code' => Str::random(15)
        ]);


        return $this->apiResponse(__('user confirmed successfully'));

    }

    public function forget_password(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone);
        $user->first()->update([
            'reset_code' => 1234,
            'reset_at' => now()->toDateTimeString()
        ]);

        return $this->apiResponse(__('reset code sent successfully !'));
    }

    public function check_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code)->exists();


        return $this->apiResponse($user);
    }

    public function reset_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required',
            'password' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code);
        if (!$user->exists()) return $this->apiResponse(__('wrong reset code '));

        $user->update([
            'password' => bcrypt($request->password),
            'reset_code' => Str::random(15)
        ]);

        return $this->apiResponse(__('reset password completed !'));
    }


    public function logout()
    {
        $this->auth->user()->update([
            'fcm_token_android'=>null
            ,'fcm_token_ios'=>null]);
        $this->auth->logout();
        return responder::success(__('logout successfully !'));
    }
}
