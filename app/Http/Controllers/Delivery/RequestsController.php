<?php

namespace App\Http\Controllers\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\DeliveryRequestResource;
use App\Models\Cart;
use App\Models\DeliveryRequest;
use App\Notifications\OrderStatusChanged;
use Carbon\Carbon;
use responder;

class RequestsController extends Controller
{
    //

    public function index()
    {
        $user=auth()->user();
        $requests=DeliveryRequest::query();
        $requests->where('user_id',$user->id)->where('status','!=','canceled');
        if (request()->has('status')){
            $requests->whereHas('cart',function ($q){
                $q->whereIn('status',request('status'));
            });
        }
        return responder::success(new BaseCollection($user->requests()->with('cart','cart.family','cart.user','cart.address')->paginate(15),DeliveryRequestResource::class));
    }

    public function show(DeliveryRequest  $request)
    {
        return responder::success(new DeliveryRequestResource($request));
    }
    public function store(Cart $cart)
    {
        if ($cart->status!='wait_for_delivery'){
            return responder::error('خطا لايمكنك تنفذ هذا الطلب !');
        }
        if ($cart->requests()->exists()){
            return responder::error('عفواً الطلب قيد التوصيل بالفعل !');
        }

        $cart->deliveries()->attach(auth()->id());

        return  responder::success('يمكنك التوجه الي الفرع لاستلام الطلب !');
    }

    public function pickUpOrder(DeliveryRequest $deliveryRequest)
    {
        $user=auth()->user();
       $request= $user->requests()->where('status','!=','canceled')->where('id',$deliveryRequest->id)->first();

       if (!$request or $request->cart->status!='wait_for_delivery' or Carbon::parse($request->cart->date)->toDateString() != now()->toDateString() ) return responder::error('خطا في الطلب !');

       $request->cart->update([
           'status'=>'on_deliver'
       ]);

       $request->update(['status'=>'on_the_way_client']);
       info($request->cart->user);
        $request->cart->user->notify(new OrderStatusChanged($request->cart));

       return responder::success('تم استلام الطلب بنجاح من فضلك توجه الي عنوان العميل');
    }
    public function finishOrder(DeliveryRequest $deliveryRequest)
    {
        $user=auth()->user();
        $request= $user->requests()->where('status','!=','canceled')->where('id',$deliveryRequest->id)->first();
        if (!$request or $request->cart->status!='on_deliver' ) return responder::error('خطا في الطلب !');

        $request->cart->update([
            'status'=>'finished'
        ]);

        $request->update(['status'=>'finished']);
        info($request->cart->user);
        $request->cart->user->notify(new OrderStatusChanged($request->cart));

        return responder::success('تم انهاء الطلب بنجاح !');
    }
}
