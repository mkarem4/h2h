<?php

namespace App\Http\Controllers\Delivery;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Cart\CartResource;
use App\Models\Cart;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index()
    {
        $orders=Cart::where('status','wait_for_delivery')->doesntHave('requests')->orderByDesc('created_at')->paginate(15);
        return \responder::success(new BaseCollection($orders,CartResource::class));
    }



}
