<?php

namespace App\Http\Controllers;

use App\Models\Family;
use Illuminate\Http\Request;
use Rinvex\Subscriptions\Models\Plan;

class HomeController extends Controller
{
    public function index()
    {
        return view('family');
    }


    public function storeFamily(Request $request)
    {

        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'required|confirmed',
            'email' => 'sometimes|email|unique:families,email',
            'phone' => 'required|numeric|unique:families,phone',
            'image' => 'required|image',
            'address' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'bio' => 'required|string',
            'city_id' => 'required|exists:cities,id',
//            'order' => 'nullable|sometimes|integer|min:0',
//            'order_until' => 'sometimes|nullable|date|after:today',
//            'plan_id' => 'required|exists:plans,id',
//            'start_subscription_at' => 'required_with:plan_id|date|date_format:Y-m-d',
            'order_type' => 'required'
        ]);
        \DB::beginTransaction();
        $plan = Plan::firstOrFail();
        $validator['creator_name'] = $request->name;
        $family = Family::latest()->first();
        $order = 0;
        if ($family) $order = $family->order ?? 0;
        $user = Family::create($validator + [
                'is_active' => 0,
                'plan_id' => $plan->id,
                'order' => $order + 1,

            ]);
        $user->newSubscription('main', $plan, today()->addDay());
        $user->cities()->sync($request['cities']);
        \DB::commit();
        toast('تم التسجيل بنجاح برجاء انتظار التفعيل !', 'success');
        return back();
    }
}
