<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Notifications\OrderStatusChanged;
use App\Traits\FireBase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use function request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        $carts=Cart::query();
        $carts->where('family_id',auth()->id());
        if(request()->has('status')){
            $carts->whereIn('status', request('status'));
        }
        if (request()->has('from') and request()->has('to') and request('from') != '' and request('to') != '') {
            $carts->whereDate('created_at', '>=', request('from'))->whereDate('created_at', '<=', request('to'));
        }
        return  view('family.orders.index',['carts'=>$carts->latest()->get()]);
    }


    /**
     * Display the specified resource.
     *
     * @param Cart $cart
     * @return Application|Factory|View|void
     */
    public function show(Cart $order)
    {
        return view('family.orders.show')->with('cart',$order);
    }

    public function changeStatus(Request $request,Cart $order, $status)
    {
        $order->update(['status' => $status,'reject_reason'=>$request->reject_reason]);

        $order->update(['status'=>$status]);
        $order->user->notify(new OrderStatusChanged($order));
        if ($status=='wait_for_delivery'){
            FireBase::sendFCMTopic('/topics/city-'.$order->family->city_id,[
                'title'=>'تطبيق من البيت للبيت',
                'body'=>'هناك طلب جديد في منطقتك',
                'order_id'=>$order->id,
                'type'=>'newOrder'
            ]);
        }
        toast('تم تحديث حالة الطلب بنجاح !','success');
        return back();
    }


}
