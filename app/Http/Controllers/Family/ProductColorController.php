<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductColor;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductColorController extends Controller
{


    public function create()
    {
        $product=Product::findOrFail(\request('product'));
        return view('family.products.colors.add')->with('product',$product);
    }


    public function store(Request $request)
    {
        $validated=$request->validate([
            'name'=>'required|string',
            'code'=>'required|string',
            'product_id'=>'required|exists:products,id'
        ]);
        ProductColor::create($validated);
        toast('تم اضافة اللون للمنتج بنجاح !');
        return back();

    }
    /**
     * Display the specified resource.
     *
     * @param Product $productColor
     * @return Application|Factory|View|void
     */
    public function show(Product $productColor)
    {
        return view('family.products.colors.index')->with('product',$productColor);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View|Response
     */
    public function edit(ProductColor $productColor)
    {
        return  view('family.products.colors.edit')->with('color',$productColor);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param ProductColor $productColor
     * @return RedirectResponse
     */
    public function update(Request $request, ProductColor $productColor)
    {
        $validated=$request->validate(['name'=>'required|string','code'=>'required|string']);
        $productColor->update($validated);
        toast('تم تعديل لون المنتج بنجاح ','success');
        return  redirect()->route('family.product-colors.show',$productColor->product_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductColor $productColor
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(ProductColor $productColor)
    {
        $productColor->delete();
        toast('تم حذف لون المنتج بنجاح !','success');
        return back();
    }
}
