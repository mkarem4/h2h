<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    //

    public function index()
    {
        return view('family.profile.profile')->with('user',auth()->user())  ;
    }

    public function store(Request $request)
    {
        $family=auth()->user();
        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'nullable|confirmed',
            'email' => 'required|email|unique:families,email,' . $family->id,
            'phone' => 'required|numeric|unique:families,phone,' . $family->id,
            'image'=>'sometimes|nullable|image',
            'address'=>'required|string',
            'lat'=>'required|numeric',
            'lng'=>'required|numeric',
            'bio'=>'required|string',
            'city_id'=>'required|exists:cities,id',
            'is_online'=>'required|boolean'

        ]);
        $validator= array_filter($validator, function($value) { return !is_null($value) && $value !== ''; });
        $family->update($validator);
        toast('تم تعديل الاسرة  بنجاح !', 'success');
        return back();
    }
}
