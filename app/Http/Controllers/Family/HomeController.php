<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class HomeController extends Controller
{

    protected $redirectTo = '/family/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:family');
    }

    /**
     * Show the Family dashboard.
     *
     * @return Response
     */
    public function index() {
        return view('family.layout.home');
    }

}
