<?php

namespace App\Http\Controllers\Family;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        return view('family.products.index', ['items' => Product::where('family_id',auth()->id())->with('sizes', 'colors', 'sub_category', 'family')->latest()->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('family.products.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'info' => 'required|string',
            'image' => 'required|image',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'images' => 'required|array',
            'images.*' => 'required|image',
            'size_name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'color_name' => 'sometimes|nullable|string',
            'color_code' => 'string|nullable',
            'is_active'=>'required|boolean'
        ]);
        DB::beginTransaction();
        $validated['family_id']=auth()->id();
        $product = Product::create($validated);
        foreach ($request['images'] as $image) {
            $product->addMedia($image)->toMediaCollection('images');
        }
        $product->sizes()->create(['name' => $request['size_name'], 'price' => $request['price'], 'is_default' => 1]);
        if ($request->has('color_name') and $request->color_code!='#010101') {
            $product->colors()->create(['name' => $request['color_name'], 'code' => $request['color_code']]);
        }
        DB::commit();
        toast('تم اضافة المنتج بنجاح !', 'success');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function edit(Product $product)
    {
        return view('family.products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Category $category
     * @return RedirectResponse
     */
    public function update(Request $request, Product $product)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'info' => 'required|string',
            'image' => 'nullable|image',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'images' => 'sometimes|array',
            'images.*' => 'image',
            'is_active'=>'required|boolean'

        ]);
        $product->update($validated);
      if($request->images)
        foreach ($request['images'] as $image) {
            $product->addMedia($image)->toMediaCollection('images');
        }
        toast('تم تعديل المنتج بنجاح ', 'success');
        return redirect()->route('family.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Category $category
     * @return RedirectResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();
        toast('تم حذف المنتج بنجاح !', 'success');
        return back();
    }

    public function destroyGallery(Media $media): RedirectResponse
    {
        $media->delete();
        toast('تم حذف المنتج بنجاح !', 'success');
        return back();
    }
}
