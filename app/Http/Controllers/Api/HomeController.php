<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BannerResource;
use App\Http\Resources\Category\CategoryCollection;
use App\Models\Banner;
use App\Models\Category;
use App\Traits\ApiResponse;

class HomeController extends Controller
{
    use ApiResponse;
    public function __invoke()
    {
        $banners=Banner::when(request('city_id'),function ($q){
            return $q->where('city_id',request('city_id'));
        })->get();
        $data['categories']=new CategoryCollection(Category::with('sub_categories')->get());
        $data['banners']=  BannerResource::collection($banners);
        return $this->success($data);
    }
}
