<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\FamilyResource;
use App\Models\Family;
use App\Traits\ApiResponse;
use responder;

class SubCategoryController extends Controller
{
    //
    use ApiResponse;

    public function show($subCategory)
    {
    /*    $products = Product::query();
        if ($subCategory != 0) {
            $products->where('sub_category_id', $subCategory);
        }
        if (request()->has('city_id')) {
            $city = request('city_id');
            $products->whereHas('family', function ($q) use ($city) {
                return $q->where('families.city_id', $city);
            });
        }
        if (request()->has('name')) {
            $name = request('name');
            $products->where(function ($q) use ($name) {
                return $q->where('name', 'like', "%$name")->orWhere('name', 'like', "%$name%")->orWhere('name', 'like', "$name%");
            })->orWhereHas('family', function ($q) use ($name) {
                return $q->where('name', 'like', "%$name")->orWhere('name', 'like', "%$name%")->orWhere('name', 'like', "$name%");
            });
        }*/

        $family = Family::query();
        if (request()->has('city_id')) {
            $city = request('city_id');
            $family->whereCityId($city);
        }
        $family->with('city','products.sub_category')->whereHas('products.sub_category',function ($q)use ($subCategory){
            $q->where('sub_category_id',$subCategory);
        });
        return responder::success(new BaseCollection($family->with('cities')->withAvg('rates','value')->whereIsOnline(1)->paginate(10),FamilyResource::class));
    }
}
