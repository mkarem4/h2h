<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use Illuminate\Http\Request;

class RateController extends Controller
{
    public function product(Request $request,CartItem $cartItem)
    {
        $validated=$request->validate([
            'value'=>'required|min:0|max:5|integer'
        ]);
       return $this->rate($cartItem,$request['value']);
    }

    public function family(Request $request,Cart $cart)
    {
        $validated=$request->validate([
            'value'=>'required|min:0|max:5|integer'
        ]);
        return $this->rate($cart,$request['value']);

    }

    private function rate($model,$value){
        $rate= $model->rate();
        if ($rate->exists()){
            return  \responder::error('عفوا تم التقييم من قبل !');
        }
        $validated['user_id']=auth()->id();
        $validated['value']=$value;
        $model->rate()->create($validated);
        return \responder::success('تم التفييم بنجاح !');
    }


}
