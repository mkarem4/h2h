<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use responder;

class ContactController extends Controller
{
    //
    public function __invoke(Request $request)
    {
        $validated=$request->validate([
            'name'=>'required|string',
            'phone'=>'required|numeric',
            'email'=>'required|email',
            'message'=>'required|string'
        ]);
        Contact::create($validated);

        return responder::success('تم ارسال رسالتك إلي الادارة بنجاح !');

    }
}
