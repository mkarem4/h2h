<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Family;
use App\Models\Product;
use App\Traits\ApiResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use responder;

class ProductController extends Controller
{
    use  ApiResponse;


    public function index()
    {
        $products=Product::query();
        $products->where('is_active',1);
        $products->whereHas('family',function ($q){
            $q->valid();
        });
        if (request('name')){
            $name=request('name');
            $products->where(function ($q) use ($name){
                $q->where('name','like',"%$name")
                    ->orWhere('name','like',"%$name%")
                    ->orWhere('name','like',"$name%");
            });
        }
        return responder::success(new BaseCollection($products->withAvg('rates','value')->with('sub_category','sizes','colors','media')->latest()->paginate(8),ProductResource::class));
    }
    public function show(Product $product)
    {
        return $this->success(new ProductResource($product));
    }

    public function ProductsByFamily(Family $family): JsonResponse
    {
        $products=Product::query();
        $products->where('family_id',$family->id)->where('is_active',1);
        if (request()->has('sub_category_id')){
            $products->where('sub_category_id',request('sub_category_id'));
        }
        return responder::success(new BaseCollection($products->withAvg('rates','value')->with('sub_category','colors','sizes','media','family')->latest()->paginate(10),ProductResource::class));
    }
}
