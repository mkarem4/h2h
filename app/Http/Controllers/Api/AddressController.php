<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AddressResource;
use App\Models\Address;
use Illuminate\Http\Request;
use responder;

class AddressController extends Controller
{
    public function index()
    {
        return responder::success(AddressResource::collection(auth()->user()->addresses));
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'address' => 'required|string',
            'city_id'=>'required|exists:cities,id'
        ]);
        auth()->user()->addresses()->create($validated);
        return responder::success('تم تسجيل العنوان بنجاح !');
    }

    public function update(Request $request, Address $address)
    {
        $validated = $request->validate([
            'lat' => 'sometimes|numeric',
            'lng' => 'sometimes|numeric',
            'address' => 'sometimes|string',
            'city_id'=>'sometimes|exists:cities,id'
        ]);
        $address->update($validated);
        return responder::success('تم تعديل العنوان بنجاح !');

    }

    public function destroy( Address $address)
    {
        $address->delete();
        return responder::success('تم حذف العنوان بنجاح !');
    }


}
