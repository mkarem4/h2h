<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use JWTAuth;
use responder;

class AuthController extends Controller
{
    use ApiResponse;

    private $auth;

    public function __construct()
    {
        $this->auth = auth('api');
    }

    public function register(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'phone' => 'required|unique:users,phone|numeric',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'fcm_token_android' => 'required_without:fcm_token_ios',
            'fcm_token_ios' => 'required_without:fcm_token_android',
            'image' => 'sometimes|image',
            'gender' => 'sometimes|in:male,female',
            'birth_date' => 'sometimes|date'
        ];
        $request->validate($rules);
        $inputs = $request->all();
        $inputs['confirmation_code'] = 1234;
//        if (config('sms.env') == 'local') {
//            $inputs['confirmation_code'] = 1234;
//        } else {
//            $inputs['confirmation_code'] = rand(1000, 9999);
//            sendSMS($request->phone, "Your OTP {$inputs['confirmation_code']}");
//        }
        $user = User::create($inputs);
        $user->token = JWTAuth::fromUser($user);
        return $this->success(new UserResource($user));
//        return $this->apiResponse(new UserResource($user));
    }


    public function login(Request $request)
    {
        $request->validate([
            "phone" => 'required|exists:users,phone',
            'password' => 'required',
            'fcm_token_android' => 'required_without:fcm_token_ios',
            'fcm_token_ios' => 'required_without:fcm_token_android',
        ]);
        $credentials = request(['phone', 'password']);
        $attemp = $this->auth->attempt($credentials);
        if (!$attemp) {
            return $this->apiResponse(null, 'خطأ في بيانات الدخول !');
        }
        $user = $this->auth->user();
        $user->update($request->only('fcm_token_android', 'fcm_token_ios'));
        $user->token = JWTAuth::fromUser($user);
        return $this->apiResponse(new UserResource($user));
    }

    public function profile()
    {
        $user = $this->auth->user();
        $user->token = JWTAuth::getToken()->get();
        return $this->apiResponse(new UserResource($user));
    }

    public function updateProfile(Request $request)
    {
        $user = $this->auth->user();
        $id = $user->id;
        $rules = [
            'phone' => 'sometimes|unique:users,phone,' . $id,
//            'phone' => 'sometimes|phone:sa,eg|unique:users,phone,' . $id,
            'password' => 'sometimes',
            'email' => 'sometimes|email|unique:users,email,' . $id,
            'name' => 'sometimes',
            'image' => 'sometimes|image',
            'fcm_token_android' => 'sometimes',
            'fcm_token_ios' => 'sometimes',
            'gender' => 'sometimes|in:male,female',
            'birth_date' => 'sometimes|date'
        ];

        $request->validate($rules);
        $inputs = $request->all();
        $user->update($inputs);
        $user->token = JWTAuth::fromUser($user);

        return $this->apiResponse(new UserResource($user));
    }

    public function confirmation(Request $request)
    {
        $inputs = $request->validate([
            'phone' => 'required|exists:users,phone',
//            'phone' => 'required|phone:sa,eg|exists:users,phone',
            'confirmation_code' => 'required'
        ]);
        $user = User::where($inputs)->first();
        if (!$user) {
            return $this->apiResponse(null, __('wrong code'));
        }
        $user->update([
            'is_confirmed' => 1,
            'confirmed_at' => now()->toDateTimeString(),
            'confirmation_code' => Str::random(15)
        ]);

        return $this->apiResponse(__('user confirmed successfully'));
    }

    public function resendConfirmation(Request $request)
    {
        $inputs = $request->validate([
            'phone' => 'required|exists:users,phone',
//            'phone' => 'required|phone:sa,eg|exists:users,phone',
        ]);
        $user = User::where($inputs + ['is_confirmed' => 0])->first();
        if (!$user) {
            return $this->apiResponse(null, "عفواً الطلب غير مسموح");
        }
        $confirmation_code = 1234;
//        if (config('sms.env') == 'local') {
//            $confirmation_code = 1234;
//        } else {
//            $confirmation_code = rand(1000, 9999);
//            sendSMS($user->phone, "Your OTP $confirmation_code");
//        }
        $user->update(['confirmation_code' => $confirmation_code]);

        return responder::success("تم ارسال رمز التفعيل بنجاح !");
    }

    public function forget_password(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone);
        $user->first()->update([
            'reset_code' => 1234,
            'reset_at' => now()->toDateTimeString()
        ]);

        return $this->apiResponse(__('reset code sent successfully !'));
    }

    public function check_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code)->exists();


        return $this->apiResponse($user);
    }

    public function reset_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required',
            'password' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code);
        if (!$user->exists()) return $this->apiResponse(__('wrong reset code '));

        $user->update([
            'password' => bcrypt($request->password),
            'reset_code' => Str::random(15)
        ]);

        return $this->apiResponse(__('reset password completed !'));
    }

    public function logout()
    {
//        Password::broker('families')->sendResetLink(['email'=>'gemejim@mailinator.com']);
        $this->auth->user()->update(['fcm_token_android' => null, 'fcm_token_ios' => null]);
        $this->auth->logout();
        return responder::success(__('logout successfully !'));
    }


}
