<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CouponResource;
use Illuminate\Http\Request;
use responder;

class CouponController extends Controller
{

    public function __invoke(Request $request)
    {
        $request->validate(['code'=>'required|exists:coupons','family_id'=>'required|exists:families,id']);

        return responder::success(new CouponResource(applyCoupon($request['code'],$request['family_id'])));
    }
}
