<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function __invoke()
    {
        return \responder::success(Setting::all()->mapWithKeys(function ($q){
            return [$q['name']=>$q['ar_value']];
        }));
    }
}
