<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Cart\CartResource;
use App\Models\Address;
use App\Models\Cart;
use App\Models\Family;
use App\Models\Product;
use App\Models\Setting;
use App\Notifications\NewOrderNotification;
use App\services\DeliveryFees;
use App\Traits\FireBase;
use Arr;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use responder;

class CartController extends Controller
{
    public function store(Request $request)
    {
        $validated = $request->validate([
            'coupon_code' => 'sometimes|exists:coupons,code',
            'carts' => 'required|array',
            'carts.*.family_id' => 'required|exists:families,id',
            'carts.*.payment' => 'required|in:cash,electronic',
            'carts.*.address_id' => 'required|exists:addresses,id',
            'carts.*.date' => 'required|date_format:Y-m-d H:i',
            'carts.*.notes' => 'sometimes|nullable|string',
            'carts.*.products' => 'required|array',
            'carts.*.products.*.product_id' => 'required|exists:products,id',
            'carts.*.products.*.product_size_id' => 'required|exists:product_sizes,id',
            'carts.*.products.*.product_color_id' => 'sometimes|nullable|exists:product_colors,id',
            'carts.*.products.*.quantity' => 'required|integer|min:1',
            'carts.*.payment_id'=>'required_if:carts.0.payment,credit'
        ]);
        if (!auth()->user()->is_confirmed){
            return response()->json(['status'=>false,'msg'=>'يجب تفعيل رقم جوالك','type'=>'phone_confirmation']);
        }
        DB::beginTransaction();
        if ($request->payment_id){
//todo : validate pamynt id form myfattorah
        }
        $this->calcCart($validated['carts'], $request->coupon_code);
        DB::commit();
        return responder::success('تم تسجيل طلبك بنجاح ! ');
    }

    public function calcCart($data, $coupon_code)
    {
        $discount = 0;
        $coupon_id = null;

        $settings = Setting::pluck('ar_value','name');
        foreach ($data as $cart) {
            if (!is_null($coupon_code)) {
                $coupon = applyCoupon($coupon_code,$cart['family_id']);
                $discount = $coupon->discount;
                $coupon_id = $coupon->id;
            }
            $cart['total'] = 0;
            foreach ($cart['products'] as $key => $product) {
                $size = Product::find($product['product_id'])->size($product['product_size_id']);
                $cart['total'] += ($size->price - ($size->price * $size->discount / 100)) * $product['quantity'];
                $cart['products'][$key]['discount'] = $size->discount;
                $cart['products'][$key]['price'] = $size->price;
            }
            $cart['discount'] = $cart['total'] * $discount / 100;
            $address=Address::find($cart['address_id']);
            $family=Family::find($cart['family_id']);
            $delivery_fees=new DeliveryFees($family,$address->lat,$address->lng,$settings);
            $cart['delivery_cost'] =$delivery_fees->deliveryFees();
            $cart['tax'] = ($cart['total'] - $cart['discount']) * Arr::get($settings,'tax',10) / 100;
            $cart['grand_total'] = $cart['total'] + $cart['tax'] + $cart['delivery_cost'] - $cart['discount'];
            $cart['coupon_id'] = $coupon_id;

        }
        $order = auth()->user()->carts()->create($cart);
        $order->items()->createMany($cart['products']);
        $order->family->notify(new NewOrderNotification($order));
    }

    private function calcDeliveryCost(Address $address,Family $family)
    {
        throw_if(!$address->city,ValidationException::withMessages(['city'=>'عفوا يجب تحديث عنوانك برجاء اضافة المدينة التابع لها']));
        if ($family->city_id == $address->city_id) {
            return $address->city->delivery_cost;
        }
        elseif(in_array($address->city_id,$family->city->supportedCities->pluck('id')->toArray())){
            return $family->city->supportedCities->where('id',$address->city_id)->first()->pivot->delivery_cost;
        }else{
            throw  ValidationException::withMessages(['عفوا المدينة غير مدعومة من فضلك تواصل مع الادارة']);
        }
    }

    public function notifiyDelivires(Family $family)
    {
        FireBase::sendFCMTopic('/topics/city-' . $family->city_id, [
            'title' => 'تطبيق من البيت للبيت',
            'body' => 'هناك طلب جديد في منطقتك',
            'type' => 'new_order'
        ]);
    }

    public function index()
    {
        $carts = Cart::query();
        $carts->where('user_id', auth()->id());
        if (\request()->has('status')) {
            $carts->whereIn('status', \request('status'));
        }
        $carts->orderByDesc('created_at');
        $carts->withExists('rate');
        return responder::success(new BaseCollection($carts->with('user','family','requests','address','deliveries','items','items.color','items.size','items.product','logs')->paginate(10), CartResource::class));
    }

    public function show(Cart $cart)
    {
        $cart->rate_exists=$cart->rate()->exists();
        return responder::success(new CartResource($cart));
    }

    /**
     * @param Request $request
     * @param Cart $cart
     * @return JsonResponse
     */
    public function update(Request $request, Cart $cart)
    {
        if ($cart->status != 'pending') return responder::error('عفواً الطلب قيد التنفيذ لايمكنك الغاء الطلب !');

        $cart->update(['status' => 'canceled']);

        return responder::success('تم الغاء الطلب بنجاح !');
    }

}
