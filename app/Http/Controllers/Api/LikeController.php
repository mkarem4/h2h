<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use responder;

class LikeController extends Controller
{
    public function like(Product $product)
    {
        $message='لقد تم تسجيل اعجابك بهذا المنتج بنجاح !';
        if ($product->is_liked){
            $message='لقد تم الغاء اعجبابك بهذا المنتج بنجاح !';
        }
        auth()->user()->liked_products()->toggle($product);
        return responder::success($message);
    }

    public function index()
    {
        return responder::success(new BaseCollection(auth()->user()->liked_products()->withAvg('rates','value')->paginate(15),ProductResource::class));
    }
}
