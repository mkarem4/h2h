<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\FamilyResource;
use App\Models\Category;
use App\Models\Family;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use responder;
use Rinvex\Subscriptions\Models\Plan;

class FamilyController extends Controller
{
    //

    public function index()
    {
        $families=Family::query();
        $families->valid();
        $families->when(request('name'),function ($q){
            $q->where(function ($query){
                $query->where('name','like',request('name')."%")->orWhere('name','like',"%".request('name'))->orWhere('name','like',"%".request('name')."%");
            });
        });

        return responder::success(new BaseCollection($families->paginate(10),FamilyResource::class));
    }
    public function familyByCategory(Category $category): JsonResponse
    {
        $sub_categories=$category->sub_categories()->pluck('sub_categories.id');
        $families=Product::whereIsActive(1)->whereIn('sub_category_id',$sub_categories)->pluck('family_id')->unique();
        /*$family_ids=$category->products()->distinct()->pluck('products.family_id');

        $families=Family::whereIn('id',$family_ids)->paginate(15);*/
        return responder::success(new BaseCollection(Family::with('city','subCategories')->valid()->withAvg('rates','value')->whereIsOnline(1)->whereIn('id',$families)->paginate(15),FamilyResource::class));
    }
    public function familyBySubCategory(SubCategory $subCategory): JsonResponse
    {
        $id_families=Product::whereIsActive(1)->where('sub_category_id',$subCategory->id)->pluck('family_id')->unique();
        $families=Family::with('city','subCategories')->when(request('city_id'),function ($q){
            $q->where('city_id',request('city_id'));
        })->when(request('order_type'),function ($q){
            $q->where('order_type',request('order_type'));
        })->valid()->withAvg('rates','value')->whereIsOnline(1)->whereIn('id',$id_families)->paginate(15);
        return responder::success(new BaseCollection($families,FamilyResource::class));
    }

    public function createFamily(Request $request): JsonResponse
    {
        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'required',
            'email' => 'sometimes|email|unique:families,email',
            'phone' => 'required|numeric|unique:families,phone',
            'image' => 'required|image',
            'address' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'bio' => 'required|string',
            'city_id' => 'required|exists:cities,id',
            'order_type' => 'required'
        ]);
        \DB::beginTransaction();
        $plan = Plan::firstOrFail();
        $validator['creator_name'] = $request->name;
        $family = Family::latest()->first();
        $order = 0;
        if ($family) $order = $family->order ?? 0;
        $user = Family::create($validator + [
                'is_active' => 0,
                'plan_id' => $plan->id,
                'order' => $order + 1,

            ]);
        $user->newSubscription('main', $plan, today()->addDay());
        $user->cities()->sync($request['cities']);
        \DB::commit();
        return \responder::success('تم التسجيل بنجاح برجاء انتظار التفعيل !');
    }
}
