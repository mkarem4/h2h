<?php

namespace App\Http\Controllers\Api\Family;

use App\Http\Controllers\Controller;
use App\Http\Requests\Family\UpdateCartRequest;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Cart\CartResource;
use App\Models\Cart;
use App\Notifications\OrderStatusChanged;
use App\Traits\FireBase;
use responder;
use function request;

class CartController extends Controller
{

    public function index()
    {
        $orders=Cart::query();
        $orders->where('family_id',auth()->id());
        if (request()->has('status')){
            $orders->whereIn('status', request('status'));
        }
        return responder::success(new BaseCollection($orders->with('user','logs','family','address','deliveries','items','items.color','items.size','items.product','requests')->orderByDesc('created_at')->paginate(15),CartResource::class));
    }

    public function show(Cart $cart)
    {
        return responder::success(new CartResource($cart));
    }

    public function update(UpdateCartRequest $request,Cart $cart)
    {

        $cart->update($request->validated());
        $cart->user->notify(new OrderStatusChanged($cart));
        if ($request['status']=='wait_for_delivery'){
            FireBase::sendFCMTopic('/topics/city-'.$cart->family->city_id,[
                'title'=>'تطبيق من البيت للبيت',
                'body'=>'هناك طلب جديد في منطقتك',
                'order_id'=>$cart->id,
                'type'=>'newOrder'
            ]);
        }
        return responder::success('تم تعديل حالة الطلب بنجاح !');
    }

}
