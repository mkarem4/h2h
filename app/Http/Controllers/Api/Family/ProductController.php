<?php

namespace App\Http\Controllers\Api\Family;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\Product\ProductResource;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use responder;

class ProductController extends Controller
{
    public function index()
    {
        return responder::success(new BaseCollection(auth()->user()->products()->withAvg('rates', 'value')->with('sizes', 'colors', 'sub_category', 'family', 'media')->paginate(15), ProductResource::class));
    }

    public function show(Product $familyProduct)
    {
        return responder::success(new ProductResource($familyProduct));
    }

    public function update(Request $request, Product $familyProduct)
    {
        $validated = $request->validate([
            'is_active' => 'required|boolean'
        ]);
        $familyProduct->update($validated);

        return responder::success('تم تعديل حالة المنتج بنجاح !');
    }


    public function storeProductByFamily(Request $request): JsonResponse
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'info' => 'required|string',
            'image' => 'required|image',
            'sub_category_id' => 'required|exists:sub_categories,id',
            'images' => 'required|array',
            'images.*' => 'required|image',
            'size_name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'color_name' => 'sometimes|nullable|string',
            'color_code' => 'string|nullable',
            'is_active' => 'required|boolean'
        ]);
        DB::beginTransaction();
        $validated['family_id'] = auth()->id();
        $product = Product::create($validated);
        foreach ($request['images'] as $image) {
            $product->addMedia($image)->toMediaCollection('images');
        }
        $product->sizes()->create(['name' => $request['size_name'], 'price' => $request['price'], 'is_default' => 1]);
        if ($request->has('color_name') and $request->color_code != '#010101') {
            $product->colors()->create(['name' => $request['color_name'], 'code' => $request['color_code']]);
        }
        DB::commit();
        return \responder::success('تم اضافة المنتج بنجاح !');
    }
}
