<?php

namespace App\Http\Controllers\Api\Family;

use App\Http\Controllers\Controller;
use App\Http\Resources\FamilyResource;
use Illuminate\Http\Request;
use JWTAuth;
use responder;

class AuthController extends Controller
{
    private $auth;
    public function __construct()
    {
        $this->auth=auth('api_family');
    }

    public function login( Request $request)
    {
        $request->validate([
            "email" => 'required|exists:families,email',
            'password' => 'required',
            'token'=>'required|string',
            'device'=>'required|in:android,ios',
        ]);
        $credentials = request(['email', 'password']);
        $attemp=$this->auth->attempt($credentials);
        if (!$attemp) {
            return responder::error('خطأ في بيانات الدخول !');
        }
        $user=$this->auth->user();
        $user->logout = $user->devices()->firstOrCreate($request->only('token', 'device'))->id;
        $user->token = JWTAuth::fromUser($user);
        return responder::success(new FamilyResource($user));
    }

    public function profile()
    {
        $user = $this->auth->user();
        $user->token = JWTAuth::getToken()->get();
        return responder::success(new FamilyResource($user));
    }

    public function updateProfile(Request $request)
    {
        $user=$this->auth->user();
        $id=$user->id;
        $rules = [
            'phone' => 'sometimes|numeric|unique:families,phone,' . $id,
            'password' => 'sometimes',
            'name' => 'sometimes|unique:users,name,' . $id,
            'image' => 'sometimes|image',
            'bio'=>'sometimes|string',
            'lat'=>'required_with:lng',
            'lng'=>'required_with:lat',
            'address'=>'sometimes|string',
            'city_id'=>'sometimes|exists:cities,id',
            'is_online'=>'sometimes|boolean',
            'order_type'=>'sometimes'
        ];
        $validated=$request->validate($rules);
        $user->update($validated);
        $user->token = JWTAuth::fromUser($user);

        return responder::success(new FamilyResource($user));
    }

    public function logout(Request $request)
    {
        $request->validate([
            'logout'=>'required|exists:devices,id'
        ]);
        $user=$this->auth->user();
        $user->devices()->where('devices.id',$request['logout'])->delete();
        $this->auth->logout();
        return responder::success('تم تسجيل الخروج بنجاح !');
    }

  /*  public function confirmation(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('confirmation_code', $request->code);
        if (!$user->exists()) {
            return $this->apiResponse(null, __('wrong code'));
        }
        $user->first()->update([
            'is_confirmed' => 1,
            'confirmed_at' => now()->toDateTimeString(),
            'confirmation_code' => Str::random(15)
        ]);


        return $this->apiResponse(__('user confirmed successfully'));

    }

    public function forget_password(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone);
        $user->first()->update([
            'reset_code' => 1234,
            'reset_at' => now()->toDateTimeString()
        ]);

        return $this->apiResponse(__('reset code sent successfully !'));
    }

    public function check_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code)->exists();


        return $this->apiResponse($user);
    }

    public function reset_code(Request $request, $role)
    {
        $request->validate([
            'phone' => 'required|exists:' . $this->roleToTableName($role) . ',phone',
            'code' => 'required',
            'password' => 'required'
        ]);
        $user = $this->getModel($role)::where('phone', $request->phone)->where('reset_code', $request->code);
        if (!$user->exists()) return $this->apiResponse(__('wrong reset code '));

        $user->update([
            'password' => bcrypt($request->password),
            'reset_code' => Str::random(15)
        ]);

        return $this->apiResponse(__('reset password completed !'));
    }*/
}
