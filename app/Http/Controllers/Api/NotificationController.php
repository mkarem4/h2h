<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\BaseCollection;
use App\Http\Resources\NotificationResource;
use responder;

class NotificationController extends Controller
{
    //

    public function __invoke()
    {
        $notifications=auth()->user()->unreadNotifications()->paginate(20);

        return responder::success(new BaseCollection($notifications,NotificationResource::class));
    }
}
