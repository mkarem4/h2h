<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Password;
use responder;

class ForgetPasswordController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        //
        $request->validate(['type'=>'in:families,deliveries,users|required','email'=>'required|email']);

        $reset= Password::broker($request['type'])->sendResetLink(['email'=>$request['email']]);
        info($reset);
        return  responder::success("تم ارسال طلب استعادة كلمة السر بنجاح !");
    }
}
