<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Family;
use App\Models\Setting;
use App\services\DeliveryFees;
use Arr;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use responder;

class FeesController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Family $family,Request $request)
    {
        $request->validate([
            'lat'=>'required|min:-90|max:90|numeric',
            'lng'=>'required|min:-180|max:180|numeric'
        ]);
        $settings=Setting::pluck('ar_value','name');
        $delivery_cost=new DeliveryFees($family,$request['lat'],$request['lng'],$settings);
        return responder::success([
            'tax'=> Arr::get($settings,'tax',10),
            'delivery_cost'=>(string) $delivery_cost->deliveryFees(),
            'distance'=>$delivery_cost->getDistance()
        ]);
    }
}
