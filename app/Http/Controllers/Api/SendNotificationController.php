<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use responder;

class SendNotificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $request->validate([
            'user_id'=>'required|integer',
            'user_type'=>'required|in:User,Family,Delivery',
            'data'=>'required|array',
            'data.title'=>'required',
            'data.body'=>'required',
        ]);
        $class = 'App\\Models\\' . $request['user_type'];
        $class::findOrFail($request['user_id'])->selfNotify($request['data']);
        return  responder::success('تم الارسال بنجاح !');
    }
}
