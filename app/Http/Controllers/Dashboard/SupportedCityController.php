<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\City;
use Illuminate\Http\Request;

class SupportedCityController extends Controller
{
    public function show(City $supported_city)
    {
        return view('dashboard.cities.supported-cities')->with('city',$supported_city);
    }

    public function update(Request $request,City $supported_city)
    {
        $request->validate([
            'supported'=>'required|array',
            "supported.$supported_city->id.supported_city_id"=>'required|exists:cities,id',
            "supported.$supported_city->id.delivery_cost"=>'required|numeric'
        ]);
        $supported_city->supportedCities()->attach($request['supported']);
        toast()->success('تم اضافة المدينة المدعومة بنجاح !');
        return back();
    }

    public function destroy(Request $request,City $supported_city)
    {
        $supported_city->supportedCities()->detach($request['supported_city_id']);
        toast()->success('تم حذف المدينة المدعومة بنجاح !');
        return back();


    }
}
