<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SettingsController extends Controller
{
//    use SettingOperation;
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $settings = Setting::get()->groupby('page');

        return view('dashboard.settings.index',compact('settings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        foreach ($request->except('_token') as $key=> $setting) {
            if (count($setting)==1){
                $data=['ar_value'=>$setting[0],'en_value'=>$setting[0]];
            }else{
                $data=['ar_value'=>$setting[0],'en_value'=>$setting[1]];
            }
            Setting::where('name',$key)->update($data);
        }
        toast()->success('تم تعديل الاعدادات بنجاح !');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View|Response
     */
    public function show($slug)
    {
        $settings = Setting::where('slug', $slug)->get();
        $settings_page = $settings->pluck('page')->first();
        return view('dashboard.settings.form')
            ->with('settings_page', $settings_page)
            ->with('settings', $settings);
    }

}
