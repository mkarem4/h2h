<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Rate;

class RateController extends Controller
{
    public function family()
    {
        return view('dashboard.rates.family')->with('rates',Rate::where('rateable_type',Cart::class)->whereHas('rateable')->with('rateable.family','user')->get());
    }

    public function product()
    {
        return view('dashboard.rates.products')->with('rates',Rate::where('rateable_type',CartItem::class)->with('rateable.product','user')->whereHas('rateable')->get());
    }
}
