<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Family;
use DB;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Rinvex\Subscriptions\Models\Plan;

class FamilyController extends Controller
{

    public function login(Family $family)
    {
        auth('family')->login($family);
        return redirect()->route('family.profile.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(Request $request)
    {
        $users = Family::with('city', 'rates')->withSum(['carts' => function ($q) {
            $q->whereIn('status', ['finished', 'force_finish']);
        }], 'grand_total')->withCount('carts')->withAvg('rates', 'value')->latest()->get();
        return view('dashboard.family.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('dashboard.family.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'required|confirmed',
            'email' => 'sometimes|email|unique:families,email',
            'phone' => 'required|numeric|unique:families,phone',
            'image' => 'required|image',
            'address' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'bio' => 'required|string',
            'city_id' => 'required|exists:cities,id',
            'order' => 'nullable|sometimes|integer|min:0',
            'order_until'=>'sometimes|nullable|date|after:today',
            'plan_id' => 'required|exists:plans,id',
            'start_subscription_at'=>'required_with:plan_id|date|date_format:Y-m-d',
            'order_type'=>'required',
            'is_active'=>'required|boolean'

        ]);
        DB::beginTransaction();
        $validator['creator_name']=auth()->user()->name;
        $user = Family::create($validator);
        $user->newSubscription('main', Plan::find($request['plan_id']),$request['start_subscription_at']);
        $user->cities()->sync($request['cities']);
        DB::commit();
        toast('تم اضافة الاسرة  بنجاح !', 'success');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show(Family $family)
    {
        $plans = Plan::all()->mapWithKeys(function ($q) {
            return [$q['id'] => $q['name']];
        });
        return \view('dashboard.family.show')->with(['plans' => $plans, 'user' => $family, 'subscription' => $family->subscriptions()->latest()->first()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Family $family
     * @return Factory|View
     */
    public function edit(Family $family)
    {
        return view('dashboard.family.edit', ['user' => $family]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Family $family
     * @return RedirectResponse
     */
    public function update(Request $request, Family $family)
    {
        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'nullable|confirmed',
            'email' => 'required|email|unique:families,email,' . $family->id,
            'phone' => 'required|numeric|unique:families,phone,' . $family->id,
            'image' => 'sometimes|nullable|image',
            'address' => 'required|string',
            'lat' => 'required|numeric',
            'lng' => 'required|numeric',
            'bio' => 'required|string',
            'city_id' => 'required|exists:cities,id',
            'order' => 'nullable|sometimes|integer|min:0',
            'plan_id' => 'sometimes|exists:plans,id',
            'start_subscription_at'=>'required_with:plan_id|date|date_format:Y-m-d',
            'order_until'=>'sometimes|nullable|date|after:today',
            'order_type'=>'required',
            'is_active'=>'required|boolean'
        ]);
        $validator = array_filter($validator, function ($value) {
            return !is_null($value) && $value !== '';
        });
        $family->update($validator);

        toast('تم تعديل الاسرة  بنجاح !', 'success');
        return redirect()->route('admin.families.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Admin $admin
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Family $family)
    {
        $family->delete();
        toast('تم حذف الاسرة بنجاح !', 'success');
        return back();
    }
}
