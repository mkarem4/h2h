<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Admin;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index(Request $request)
    {

        return \view('dashboard.admin.index')->with('users',Admin::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        $roles = Role::all();
        return view('dashboard.admin.add', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'required|confirmed',
            'role_id' => 'required',
            'email' => 'required|email|unique:admins,email',
        ]);
        $user = Admin::create($validator);
        $user->assignRole(Role::find($request->input('role_id')));
        $user->syncPermissions(Role::find($request->input('role_id'))->permissions()->pluck('id'));

        toast('تم اضافة المدير بنجاح !', 'success');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit(Admin $admin)
    {
        $userRole = $admin->roles->pluck('name', 'id')->all();
        $roles = Role::all();

        return view('dashboard.admin.edit', ['user' => $admin, 'roles' => $roles, 'userRole' => $userRole]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Admin $admin
     * @return RedirectResponse
     */
    public function update(Request $request, Admin $admin)
    {

        $validator = $request->validate([
            'name' => 'required|string',
            'password' => 'nullable|confirmed',
            'role_id' => 'required',
            'email' => 'required|email|unique:admins,email,' . $admin->id,
        ]);
        $validator = array_filter($validator, function ($value) {
            return !is_null($value) && $value !== '';
        });
        $admin->update($validator);
        DB::table('model_has_roles')->where('model_id', $admin->id)->delete();
        DB::table('model_has_permissions')->where('model_id', $admin->id)->delete();

        $admin->assignRole(Role::find($request->input('role_id')));
        $admin->syncPermissions(Role::find($request->input('role_id'))->permissions()->pluck('id'));

        toast('تم تعديل المدير بنجاح !', 'success');
        return redirect()->route('admin.admins.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Admin $admin
     * @return Response
     * @throws Exception
     */
    public function destroy(Admin $admin)
    {
        $admin->delete();
        toast('تم حذف المدير بنجاح !', 'success');
        return back();
    }
}
