<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Traits\FireBase;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        return view('dashboard.notifications.index');
    }

    public function store(Request $request)
    {
        $request->validate(['title'=>'required','body'=>'required','type'=>'required|in:users,families,deliveries,all']);

        FireBase::sendFCMTopic('/topics/general_'.$request['type'],$request->all());

        toast()->success('تم الارسال بنجاح !');
        return back();
    }
}
