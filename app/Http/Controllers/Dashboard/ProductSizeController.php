<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductSize;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductSizeController extends Controller
{


    public function create()
    {
        $product = Product::findOrFail(\request('product'));
        return view('dashboard.products.sizes.add')->with('product', $product);
    }


    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric|min:0',
            'product_id' => 'required|exists:products,id',
            'discount' => 'required|min:0|max:100'
        ]);
        ProductSize::create($validated);
        toast('تم اضافة المقاس للمنتج بنجاح !');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param Product $productSize
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function show(Product $productSize)
    {
        return view('dashboard.products.sizes.index')->with('product', $productSize);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Product $productSize
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(ProductSize $productSize)
    {
        return view('dashboard.products.sizes.edit')->with('size', $productSize);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param ProductSize $productSize
     * @return RedirectResponse
     */
    public function update(Request $request, ProductSize $productSize)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'price' => 'required|numeric|min:1',
            'discount' => 'required|min:0|max:100'
        ]);
        $productSize->update($validated);
        toast('تم تعديل مقاس المنتج بنجاح ', 'success');
        return redirect()->route('admin.product-sizes.show', $productSize->product_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param ProductSize $productSize
     * @return RedirectResponse
     * @throws \Exception
     */
    public function destroy(ProductSize $productSize)
    {
        if ($productSize->is_default) {
            toast('عفوا لا يمكن حذف هذا المقاس حيث انه المقاس الافتراضي !', 'error');
            return back();
        }
        $productSize->delete();
        toast('تم حذف مقاس المنتج بنجاح !', 'success');
        return back();
    }

    public function toggleDefault(Product $product,ProductSize $productSize)
    {
        $product->sizes()->update(['is_default'=>0]);
        $productSize->update(['is_default'=>1]);
        toast('تم تحديد المقاس الافتراضي بنجاح !','success');
        return back();

    }
}
