<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Family;
use DB;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $from = now()->firstOfMonth();
        $to = now()->endOfMonth();
        if ($request->from and $request->to) {
            $from = $request->from;
            $to = $request->to;
        }
        $locations= DB::table('carts')->join('addresses','addresses.id','=','carts.address_id')->select(['addresses.lat','addresses.lng'])->get()->toArray();


        $data = [
            'new_families' => Family::dateFilter($from, $to)->count(),
            'families' => Family::count(),
            'carts' => Cart::dateFilter($from, $to)->count(),
            'carts_finished' => Cart::whereStatus('finished')->dateFilter($from, $to)->count(),
            'carts_pending' => Cart::whereStatus('pending')->dateFilter($from, $to)->count(),
            'carts_total' => Cart::whereStatus('finished')->dateFilter($from, $to)->sum('grand_total'),
            'carts_delivery' => Cart::whereStatus('finished')->dateFilter($from, $to)->sum('delivery_cost'),
            'pending_carts' => Cart::whereStatus('pending')->with('user', 'family')->get(),
            'going_carts' => Cart::whereIn('status', ['accepted', 'wait_for_delivery', 'on_deliver'])->with('user', 'family')->get(),
            'carts_in_days' => Cart::selectRaw('count(id) as carts , date(created_at) as created_at_date')->groupBy('created_at_date')->get()->toArray(),
            'carts_locations' => $locations


        ];
        return view('dashboard.layout.main', $data);
    }
}
