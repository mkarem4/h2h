<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Contact;

class ContactController extends Controller
{
    //

    public function index()
    {
        return view('dashboard.contact.index')->with('contacts',Contact::orderByDesc('created_at')->get());
    }
}
