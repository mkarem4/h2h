<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Banner;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        return view('dashboard.banners.index', ['items' => Banner::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('dashboard.banners.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate(['name' => 'required|string',
            'image' => 'required|image',
            'url' => 'nullable|sometimes|url',
            'city_id'=>'nullable',
            'product_id'=>'nullable'
        ]);
        Banner::create($validated);

        toast('تم اضافة البنر بنجاح !', 'success');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Banner $banner
     * @return Application|Factory|View|Response
     */
    public function edit(Banner $banner)
    {
        return view('dashboard.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Banner $banner
     * @return RedirectResponse
     */
    public function update(Request $request, Banner $banner)
    {
        $validated = $request->validate(['name' => 'required|string',
            'url' => 'nullable|sometimes|url',
            'image' => 'nullable|sometimes|image',
            'city_id'=>'nullable',
            'product_id'=>'nullable'

        ]);
        $banner->update($validated);
        toast('تم تعديل البنر بنجاح ', 'success');
        return redirect()->route('admin.banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Banner $banner
     * @return RedirectResponse
     */
    public function destroy(Banner $banner)
    {
        $banner->delete();
        toast('تم حذف البنر بنجاح !', 'success');
        return back();
    }
}
