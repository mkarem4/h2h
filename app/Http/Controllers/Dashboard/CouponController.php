<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index()
    {
        return view('dashboard.coupons.index', ['items' => Coupon::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('dashboard.coupons.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'code' => 'required|string',
            'is_active' => 'required|boolean',
            'expire_at' => 'required|date|after:today',
            'discount' => 'required|min:1|max:100|integer',
            'families' => 'nullable|array',

        ]);
        $coupon = Coupon::create($validated);
        if ($request->has('families'))
            $coupon->families()->sync($request['families']);
        toast('تم اضافة الكوبون بنجاح !', 'success');
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function edit(Coupon $coupon)
    {
        return view('dashboard.coupons.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Coupon $coupon
     * @return RedirectResponse
     */
    public function update(Request $request, Coupon $coupon)
    {
        $validated = $request->validate([
            'name' => 'required|string',
            'code' => 'required|string',
            'is_active' => 'required|boolean',
            'expire_at' => 'required|date|after:today',
            'discount' => 'required|min:1|max:100|numeric',
            'families' => 'nullable|array',

        ]);
        $coupon->update($validated);
        $coupon->families()->sync($request['families']);
        toast('تم تعديل الكوبون بنجاح ', 'success');
        return redirect()->route('admin.coupons.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Coupon $coupon
     * @return RedirectResponse
     */
    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
        toast('تم حذف الكوبون بنجاح !', 'success');
        return back();
    }
}
