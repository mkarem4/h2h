<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  view('dashboard.sub-categories.index',['items'=>SubCategory::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('dashboard.sub-categories.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated=$request->validate(['name'=>'required|string',
        'category_id'=>'required'
        ,'image'=>'required|image']);
        SubCategory::create($validated);

        toast('تم اضافة القسم الفرعي بنجاح !','success');
        return  back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        return  view('dashboard.sub-categories.edit',compact('subCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param SubCategory $subCategory
     * @return RedirectResponse
     */
    public function update(Request $request, SubCategory  $subCategory)
    {
        $validated=$request->validate(['name'=>'required|string','category_id'=>'required','image'=>'nullable|sometimes|image']);
        $subCategory->update($validated);
        toast('تم تعديل القسم بنجاح ','success');
        return  redirect()->route('admin.sub-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SubCategory $subCategory
     * @return RedirectResponse
     */
    public function destroy(SubCategory  $subCategory)
    {
        $subCategory->delete();
        toast('تم حذف القسم بنجاح !','success');
        return back();
    }
}
