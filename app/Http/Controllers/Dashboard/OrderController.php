<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\CartsDataTable;
use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Notifications\OrderStatusChanged;
use App\Traits\FireBase;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index(CartsDataTable $dataTable)
    {

        return $dataTable->render('dashboard.orders.index');

       /* $carts = Cart::query();
        if (request()->has('status')) {
            $carts->whereIn('status', request('status'));
        }
        if (request()->has('from') and request()->has('to') and request('from') != '' and request('to') != '') {
            $carts->whereDate('created_at', '>=', request('from'))->whereDate('created_at', '<=', request('to'));
        }
        $carts->when($request->user_id,function ($q){
            $q->where('user_id',request('user_id'));
        });
        $carts->when($request->family_id,function ($q){
            $q->where('family_id',request('family_id'));
        });
        $carts->when($request->delivery_id,function ($q){
            $q->whereHas('deliveries',function ($q){
                $q->where('delivery_id',request('delivery_id'));
            });
        });
        return view('dashboard.orders.index', ['carts' => $carts->latest()->get()]);*/
    }


    /**
     * Display the specified resource.
     *
     * @param Cart $cart
     * @return Application|Factory|View|void
     */
    public function show(Cart $order)
    {
        return view('dashboard.orders.show')->with('cart', $order);
    }

    public function changeStatus(Request $request,Cart $order, $status)
    {
        $order->update(['status' => $status,'reject_reason'=>$request->reject_reason]);
        $order->user->notify(new OrderStatusChanged($order));
        if ($status == 'wait_for_delivery') {
            FireBase::sendFCMTopic('/topics/city-' . $order->family->city_id, [
                'title' => 'تطبيق من البيت للبيت',
                'body' => 'هناك طلب جديد في منطقتك',
                'order_id' => $order->id,
                'type' => 'newOrder'
            ]);
        }
        toast('تم تحديث حالة الطلب بنجاح !', 'success');
        return back();
    }

    public function print(Cart $order)
    {
        return \view('dashboard.print')->with('cart',$order);
    }


}
