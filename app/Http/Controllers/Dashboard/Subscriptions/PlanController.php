<?php

namespace App\Http\Controllers\Dashboard\Subscriptions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Rinvex\Subscriptions\Models\Plan;

class PlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.plans.index')->with('items', Plan::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('dashboard.plans.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:plans,name',
            'description' => 'required|string',
            'invoice_period' => 'required|integer|min:1',
            'sort_order' => 'required|integer|min:1',
            'price' => 'required|integer|min:0'
        ]);
        $validated['currency'] = "SAR";
        $validated['invoice_interval'] = "day";
        $validated['signup_fee'] = 0;
        $validated['trial_period'] = 0;
        $validated['trial_interval'] = 'hour';
        Plan::create($validated);
        toast()->success('تم اضافة الباقة بنجاح !');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit(Plan $plan)
    {
        return view('dashboard.plans.edit')->with('plan', $plan);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, Plan $plan)
    {
        $validated = $request->validate([
            'name' => 'required|string|unique:plans,name,' . $plan->id,
            'description' => 'required|string',
            'invoice_period' => 'required|integer|min:1',
            'sort_order' => 'required|integer|min:1',
            'price' => 'required|integer|min:0'
        ]);
        $plan->update($validated);
        toast()->success('تم تعديل الباقة بنجاح !');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
