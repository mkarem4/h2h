<?php

namespace App\Http\Controllers\Dashboard\Subscriptions;

use App\Http\Controllers\Controller;
use App\Models\Family;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Rinvex\Subscriptions\Models\Plan;

class SubscriptionController extends Controller
{

    public function subscription(Request $request, Family $family, $type)
    {
        if ($type == 'renew') {
            $plan = $family->plan;
            $family->update([
                'subscribe_from' => now(),
                'subscribe_until' => Carbon::parse(now())->addDays($plan->invoice_period)
            ]);
        }
        if ($type == 'cancel') {
            $family->update([
                'plan_id' => null,
                'subscribe_from' => null,
                'subscribe_until' => null
            ]);
        }
        toast()->success('تم تعديل الاشتراك بنجاح !');
        return back();
    }

    public function updateSubscription(Request $request, Family $family)
    {
        $validated = $request->validate([
            'plan_id' => 'required|exists:plans,id',
            'subscribe_from' => 'required|date|after:today'
        ]);

        $plan = Plan::find($request['plan_id']);
        $family->update(['plan_id' => $request->plan_id,
            'subscribe_from' => $request['subscribe_from'],
            'subscribe_until' => Carbon::parse($request['subscribe_from'])->addDays($plan->invoice_period)
        ]);
        toast()->success('تم تعديل الاشتراك بنجاح !');
        return back();
    }

    public function nearlyEnded()
    {

        $users = Family::doesntHave('plan')->with('city', 'rates')->withSum(['carts' => function ($q) {
            $q->whereIn('status', ['finished', 'force_finish']);
        }], 'grand_total')->withCount('carts')->withAvg('rates', 'value')->orWhere('subscribe_until', '<=', now())->get();
        return view('dashboard.family.index', ['users' => $users]);
    }
}
