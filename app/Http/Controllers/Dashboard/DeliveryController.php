<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryStoreRequest;
use App\Http\Requests\DeliveryUpdateRequest;
use App\Models\Cart;
use App\Models\Delivery;
use Illuminate\Http\Request;

class DeliveryController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $deliveries = Delivery::all();

        return view('dashboard.deliveries.index')->with('users', $deliveries);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('dashboard.deliveries.add');
    }

    /**
     * @param \App\Http\Requests\DeliveryStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(DeliveryStoreRequest $request)
    {
        $delivery = Delivery::create($request->validated());

        toast('تم اضافة مندوب التوصيل بنجاح !', 'success');
        return back();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Delivery $delivery
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function edit(Request $request, Delivery $delivery)
    {
        return view('dashboard.deliveries.edit')->with('user', $delivery);
    }

    /**
     * @param \App\Http\Requests\DeliveryUpdateRequest $request
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(DeliveryUpdateRequest $request, Delivery $delivery)
    {
        $delivery->update($request->validated());

        toast('تم تحديث بيانات المندوب بنجاح !', 'success');
        return redirect()->route('admin.deliveries.index');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Delivery $delivery
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, Delivery $delivery)
    {
        $delivery->delete();
        toast('تم حذف بيانات المندوب بنجاح !', 'success');

        return redirect()->route('admin.deliveries.index');
    }

    public function assignOrder(Request $request, Cart $cart)
    {
        $request->validate(['delivery_id' => 'required|exists:deliveries,id']);
        if (!$cart->delivery() or $cart->delivery()->id == $request['delivery_id']) toast('عفوا هذا الطلب معين بالفعل لهذا المندوب !', 'error');
        $delivery = Delivery::findOrFail($request['delivery_id']);
        $cart->requests()->update(['status' => 'canceled']);
        $delivery->requests()->create(['cart_id' => $cart->id]);
        $delivery->selfNotify(['title'=>'تم اسناد طلب جديد لك','body'=>sprintf('تم اسناد طلب رقم #%s من قبل الادارة',$cart->id)]);
        toast('تم تعيين مندوب التوصيل بنجاح !', 'success');
        return back();
    }
}
