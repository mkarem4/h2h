<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\UserDataTable;
use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function index(UserDataTable $dataTable)
    {
        return $dataTable->render('dashboard.users.index');
        $users = User::withCount(['carts'=>function($q){
            $q->where('status','finished');
        }])->withSum(['carts'=>function($q){
            $q->where('status','finished');
        }],'grand_total')->get();

        return view('dashboard.users.index')->with('users',$users);
    }

    /**
     * @param Request $request
     * @return Application|Factory|View|Response
     */
    public function create(Request $request)
    {
        return view('dashboard.users.add');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $validated=$request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users',
            'phone'=>'required|numeric|unique:users',
            'image'=>'required|image',
            'password'=>'required'
        ]);
        $delivery = User::create($validated);

        toast('تم اضافة العميل بنجاح !','success');
        return back();
    }

    /**
     * @param Request $request
     * @param User $delivery
     * @return Application|Factory|View|Response
     */
    public function edit(Request $request, User $user)
    {
        return view('dashboard.users.edit')->with('user',$user);
    }

    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $validated=$request->validate([
            'name'=>'required',
            'email'=>'required|email|unique:users,email,'.$user->id,
            'phone'=>'required|numeric|unique:users,phone,'.$user->id,
            'image'=>'nullable|image',
            'password'=>'nullable'
        ]);
        $user->update($validated);

        toast('تم تحديث بيانات العميل بنجاح !','success');
        return redirect()->route('admin.users.index');
    }

    /**
     * @param Request $request
     * @param User $user
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(Request $request, User $user)
    {
        $user->delete();
        toast('تم حذف بيانات العميل بنجاح !','success');

        return redirect()->route('admin.users.index');
    }
}
