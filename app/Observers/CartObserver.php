<?php

namespace App\Observers;

use App\Models\Cart;

class CartObserver
{

    public function creating(Cart $cart)
    {
    }

    /**
     * Handle the cart "created" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function created(Cart $cart)
    {
        $cart->logs()->create(['status' => 'pending']);
    }

    /**
     * Handle the cart "updated" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function updated(Cart $cart)
    {
        $cart->logs()->create(['status' => $cart->status]);
    }

    /**
     * Handle the cart "deleted" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function deleted(Cart $cart)
    {
        //
    }

    /**
     * Handle the cart "restored" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function restored(Cart $cart)
    {
        //
    }

    /**
     * Handle the cart "force deleted" event.
     *
     * @param Cart $cart
     * @return void
     */
    public function forceDeleted(Cart $cart)
    {
        //
    }
}
