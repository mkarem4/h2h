<?php

namespace App\Models;

use App\Traits\filterDate;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Cart
 *
 * @property int $id
 * @property int $user_id
 * @property int $family_id
 * @property int $address_id
 * @property string $date
 * @property string $status
 * @property float $total
 * @property float $tax
 * @property float $delivery_cost
 * @property float $discount
 * @property float $grand_total
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $payment
 * @property int|null $coupon_id
 * @property string|null $notes
 * @property string|null $reject_reason
 * @property-read Address $address
 * @property-read Collection|Delivery[] $deliveries
 * @property-read int|null $deliveries_count
 * @property-read Family $family
 * @property-read Collection|CartItem[] $items
 * @property-read int|null $items_count
 * @property-read Rate|null $rate
 * @property-read Collection|DeliveryRequest[] $requests
 * @property-read int|null $requests_count
 * @property-read User $user
 * @method static Builder|Cart newModelQuery()
 * @method static Builder|Cart newQuery()
 * @method static Builder|Cart query()
 * @method static Builder|Cart whereAddressId($value)
 * @method static Builder|Cart whereCouponId($value)
 * @method static Builder|Cart whereCreatedAt($value)
 * @method static Builder|Cart whereDate($value)
 * @method static Builder|Cart whereDeliveryCost($value)
 * @method static Builder|Cart whereDiscount($value)
 * @method static Builder|Cart whereFamilyId($value)
 * @method static Builder|Cart whereGrandTotal($value)
 * @method static Builder|Cart whereId($value)
 * @method static Builder|Cart whereNotes($value)
 * @method static Builder|Cart wherePayment($value)
 * @method static Builder|Cart whereRejectReason($value)
 * @method static Builder|Cart whereStatus($value)
 * @method static Builder|Cart whereTax($value)
 * @method static Builder|Cart whereTotal($value)
 * @method static Builder|Cart whereUpdatedAt($value)
 * @method static Builder|Cart whereUserId($value)
 * @mixin Eloquent
 * @property-read Collection|CartLog[] $logs
 * @property-read int|null $logs_count
 * @method static Builder|Cart dateFilter($from, $to)
 */
class Cart extends Model
{
    use HasFactory,filterDate;

    protected $fillable = ['user_id', 'family_id', 'address_id', 'date', 'status', 'total', 'tax', 'delivery_cost', 'discount', 'grand_total', 'payment', 'coupon_id','notes','reject_reason','payment_id'];
    protected $casts = [
        'total' => 'double',
        'tax' => 'double',
        'grand_total' => 'double',
        'discount' => 'double',
        'delivery_cost' => 'double'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rate()
    {
        return $this->morphOne(Rate::class, 'rateable');
    }

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class)->withTrashed();
    }

    public function deliveries()
    {
        return $this->belongsToMany(Delivery::class,DeliveryRequest::class)->withPivot(['status'])->withTimestamps();
    }

    public function requests()
    {
        return $this->hasMany(DeliveryRequest::class);
    }

    public function delivery()
    {
        return $this->requests->where('status','!=','canceled')->sortByDesc('id')->first();
    }


    public function logs()
    {
        return $this->hasMany(CartLog::class);
    }
}
