<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Rate
 *
 * @property int $id
 * @property int $user_id
 * @property string $rateable_type
 * @property int $rateable_id
 * @property int $value
 * @property string|null $comment
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent $rateable
 * @property-read User $user
 * @method static Builder|Rate newModelQuery()
 * @method static Builder|Rate newQuery()
 * @method static Builder|Rate query()
 * @method static Builder|Rate whereComment($value)
 * @method static Builder|Rate whereCreatedAt($value)
 * @method static Builder|Rate whereId($value)
 * @method static Builder|Rate whereRateableId($value)
 * @method static Builder|Rate whereRateableType($value)
 * @method static Builder|Rate whereUpdatedAt($value)
 * @method static Builder|Rate whereUserId($value)
 * @method static Builder|Rate whereValue($value)
 * @mixin Eloquent
 */
class Rate extends Model
{
    use HasFactory;
    protected $fillable=['value','comment','user_id'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function rateable()
    {
        return $this->morphTo('rateable');
    }
}
