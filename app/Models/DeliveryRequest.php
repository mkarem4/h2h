<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\DeliveryRequest
 *
 * @property int $id
 * @property int $delivery_id
 * @property int $cart_id
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Cart $cart
 * @property-read Delivery $delivery
 * @method static Builder|DeliveryRequest newModelQuery()
 * @method static Builder|DeliveryRequest newQuery()
 * @method static Builder|DeliveryRequest query()
 * @method static Builder|DeliveryRequest whereCartId($value)
 * @method static Builder|DeliveryRequest whereCreatedAt($value)
 * @method static Builder|DeliveryRequest whereDeliveryId($value)
 * @method static Builder|DeliveryRequest whereId($value)
 * @method static Builder|DeliveryRequest whereStatus($value)
 * @method static Builder|DeliveryRequest whereUpdatedAt($value)
 * @mixin Eloquent
 */
class DeliveryRequest extends Model
{
    use HasFactory;
    protected $with=['delivery'];

    protected $fillable = ['cart_id', 'delivery_id', 'status'];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }
}
