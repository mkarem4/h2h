<?php

namespace App\Models;

use App\Traits\ImageOperations;
use Database\Factories\BannerFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $name
 * @property string|null $url
 * @property string $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property mixed $thubnail
 * @method static Builder|Banner newModelQuery()
 * @method static Builder|Banner newQuery()
 * @method static Builder|Banner query()
 * @method static Builder|Banner whereCreatedAt($value)
 * @method static Builder|Banner whereId($value)
 * @method static Builder|Banner whereImage($value)
 * @method static Builder|Banner whereName($value)
 * @method static Builder|Banner whereUpdatedAt($value)
 * @method static Builder|Banner whereUrl($value)
 * @mixin Eloquent
 * @method static BannerFactory factory(...$parameters)
 */
class Banner extends Model
{
    use HasFactory, ImageOperations;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'url',
        'image',
        'city_id',
        'product_id'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
