<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Device
 *
 * @property int $id
 * @property string $user_type
 * @property int $user_id
 * @property string $device
 * @property string $token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Device newModelQuery()
 * @method static Builder|Device newQuery()
 * @method static Builder|Device query()
 * @method static Builder|Device whereCreatedAt($value)
 * @method static Builder|Device whereDevice($value)
 * @method static Builder|Device whereId($value)
 * @method static Builder|Device whereToken($value)
 * @method static Builder|Device whereUpdatedAt($value)
 * @method static Builder|Device whereUserId($value)
 * @method static Builder|Device whereUserType($value)
 * @mixin Eloquent
 */
class Device extends Model
{
    use HasFactory;

    protected $fillable=['user_id','user_type','device','token'];
}
