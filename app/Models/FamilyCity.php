<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\FamilyCity
 *
 * @property int $id
 * @property int $city_id
 * @property int $family_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|FamilyCity newModelQuery()
 * @method static Builder|FamilyCity newQuery()
 * @method static Builder|FamilyCity query()
 * @method static Builder|FamilyCity whereCityId($value)
 * @method static Builder|FamilyCity whereCreatedAt($value)
 * @method static Builder|FamilyCity whereFamilyId($value)
 * @method static Builder|FamilyCity whereId($value)
 * @method static Builder|FamilyCity whereUpdatedAt($value)
 * @mixin Eloquent
 */
class FamilyCity extends Model
{
    use HasFactory;
}
