<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\SupportedCity
 *
 * @property int $id
 * @property int $city_id
 * @property int $supported_city_id
 * @property string $delivery_cost
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read City $supportedCity
 * @method static Builder|SupportedCity newModelQuery()
 * @method static Builder|SupportedCity newQuery()
 * @method static Builder|SupportedCity query()
 * @method static Builder|SupportedCity whereCityId($value)
 * @method static Builder|SupportedCity whereCreatedAt($value)
 * @method static Builder|SupportedCity whereDeliveryCost($value)
 * @method static Builder|SupportedCity whereId($value)
 * @method static Builder|SupportedCity whereSupportedCityId($value)
 * @method static Builder|SupportedCity whereUpdatedAt($value)
 * @mixin Eloquent
 */
class SupportedCity extends Model
{
    use HasFactory;
    protected $fillable=['city_id','supported_city_id','delivery_cost'];

    public function supportedCity()
    {
        return $this->belongsTo(City::class,'supported_city_id');
    }

}
