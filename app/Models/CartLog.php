<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CartLog
 *
 * @property int $id
 * @property int $cart_id
 * @property string $status
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|CartLog newModelQuery()
 * @method static Builder|CartLog newQuery()
 * @method static Builder|CartLog query()
 * @method static Builder|CartLog whereCartId($value)
 * @method static Builder|CartLog whereCreatedAt($value)
 * @method static Builder|CartLog whereId($value)
 * @method static Builder|CartLog whereStatus($value)
 * @method static Builder|CartLog whereUpdatedAt($value)
 * @mixin Eloquent
 */
class CartLog extends Model
{
    use HasFactory;
    protected $fillable=['cart_id','status'];
}
