<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\City
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|City newModelQuery()
 * @method static Builder|City newQuery()
 * @method static Builder|City query()
 * @method static Builder|City whereCreatedAt($value)
 * @method static Builder|City whereId($value)
 * @method static Builder|City whereName($value)
 * @method static Builder|City whereUpdatedAt($value)
 * @mixin Eloquent
 * @property string $delivery_cost
 * @property-read Collection|City[] $supportedCities
 * @property-read int|null $supported_cities_count
 * @method static Builder|City whereDeliveryCost($value)
 */
class City extends Model
{
    use HasFactory;

    protected $fillable=['name','delivery_cost'];

    public function supportedCities()
    {
        return $this->belongsToMany(City::class,SupportedCity::class,'city_id','supported_city_id')->withPivot('delivery_cost');
    }

    public static function toSelect(City $city)
    {
        $supported=$city->supportedCities->pluck('id');
        return self::whereNotIn('id',$supported)->where('id','!=',$city->id)->get()->mapWithKeys(function ($q){
            return [$q['id']=>$q['name']];
        });
    }

}
