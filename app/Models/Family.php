<?php

namespace App\Models;

use App\Scopes\orderScope;
use App\Traits\filterDate;
use App\Traits\FireBase;
use App\Traits\ImageOperations;
use App\Traits\SetPass;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Rinvex\Subscriptions\Models\Plan;
use Rinvex\Subscriptions\Models\PlanSubscription;
use Rinvex\Subscriptions\Services\Period;
use Rinvex\Subscriptions\Traits\HasSubscriptions;
use Tymon\JWTAuth\Contracts\JWTSubject;


/**
 * App\Models\Family
 *
 * @property int $id
 * @property string $name
 * @property string|null $email
 * @property string|null $email_verified_at
 * @property string|null $phone
 * @property string $password
 * @property string $image
 * @property string $bio
 * @property string $address
 * @property float $lat
 * @property float $lng
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int|null $city_id
 * @property int|null $order
 * @property int $is_online
 * @property string|null $order_until
 * @property string $order_type
 * @property string $creator_name
 * @property-read Collection|City[] $cities
 * @property-read int|null $cities_count
 * @property-read City|null $city
 * @property-read Collection|Device[] $devices
 * @property-read int|null $devices_count
 * @property mixed $thubnail
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @property-read Collection|Rate[] $rates
 * @property-read int|null $rates_count
 * @property-read Collection|SubCategory[] $subCategories
 * @property-read int|null $sub_categories_count
 * @property-read Collection|PlanSubscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @method static Builder|Family newModelQuery()
 * @method static Builder|Family newQuery()
 * @method static Builder|Family query()
 * @method static Builder|Family whereAddress($value)
 * @method static Builder|Family whereBio($value)
 * @method static Builder|Family whereCityId($value)
 * @method static Builder|Family whereCreatedAt($value)
 * @method static Builder|Family whereCreatorName($value)
 * @method static Builder|Family whereEmail($value)
 * @method static Builder|Family whereEmailVerifiedAt($value)
 * @method static Builder|Family whereId($value)
 * @method static Builder|Family whereImage($value)
 * @method static Builder|Family whereIsOnline($value)
 * @method static Builder|Family whereLat($value)
 * @method static Builder|Family whereLng($value)
 * @method static Builder|Family whereName($value)
 * @method static Builder|Family whereOrder($value)
 * @method static Builder|Family whereOrderType($value)
 * @method static Builder|Family whereOrderUntil($value)
 * @method static Builder|Family wherePassword($value)
 * @method static Builder|Family wherePhone($value)
 * @method static Builder|Family whereRememberToken($value)
 * @method static Builder|Family whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static Builder|Family dateFilter($from, $to)
 */
class Family extends Authenticatable implements JWTSubject
{
    use HasSubscriptions, Notifiable, filterDate;

    protected $fillable = ['name', 'creator_name', 'email', 'order_until', 'phone', 'password', 'image', 'bio', 'address', 'lat', 'lng', 'city_id', 'order', 'is_online', 'order_type', 'subscribe_until', 'subscribe_from', 'plan_id', 'is_active'];

    protected $casts = [
        'subscribe_until' => 'datetime',
        'subscribe_form' => 'datetime'
    ];

    protected static function booted()
    {
        static::addGlobalScope(new orderScope);
    }

    use HasFactory, ImageOperations, SetPass;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function rates()
    {
        return $this->hasManyThrough(Rate::class, Cart::class, 'family_id', 'rateable_id');
    }

    public function cities()
    {
        return $this->belongsToMany(City::class, FamilyCity::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function subCategories()
    {
        return $this->belongsToMany(SubCategory::class, Product::class);
    }

    public function devices()
    {
        return $this->morphMany(Device::class, 'user');
    }


    public function selfNotify($data)
    {
        $devices = $this->devices;
        $android_tokens = $devices->where('device', 'android')->pluck('token')->toArray();
        $ios_tokens = $devices->where('device', 'ios')->pluck('token')->toArray();

        if (is_array($android_tokens) and !empty($android_tokens)) {
            FireBase::notifyByFirebase($data['title'], $data['body'], $android_tokens, $data, false);
        }
        if (is_array($ios_tokens) and !empty($ios_tokens)) {
            FireBase::notifyByFirebase($data['title'], $data['body'], $ios_tokens, $data, true);
        }
    }

    /**
     * Subscribe user to a new plan.
     *
     * @param string $subscription
     * @param Plan $plan
     *
     * @return Model
     */
    public function newSubscription($subscription, Plan $plan, $start_date = null): Model
    {
        $start_date = $start_date ?? now();
        $period = new Period($plan->invoice_interval, $plan->invoice_period, $start_date);

        return $this->subscriptions()->create([
            'name' => $subscription,
            'plan_id' => $plan->getKey(),
            'trial_ends_at' => now(),
            'starts_at' => $start_date,
            'ends_at' => $period->getEndDate(),
        ]);
    }

    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function scopeValid(Builder $builder)
    {
        $builder->where('subscribe_until', '>=', now())->where('is_active', 1);
    }
}
