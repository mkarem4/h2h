<?php

namespace App\Models;

use App\Traits\ImageOperations;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Validation\ValidationException;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $info
 * @property int $family_id
 * @property int $sub_category_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $is_active
 * @property-read Collection|ProductColor[] $colors
 * @property-read int|null $colors_count
 * @property-read Family $family
 * @property-read mixed $default_size
 * @property-read mixed $is_liked
 * @property-read mixed $rate
 * @property mixed $thubnail
 * @property-read Collection|Like[] $likes
 * @property-read int|null $likes_count
 * @property-read MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read Collection|Rate[] $rates
 * @property-read int|null $rates_count
 * @property-read Collection|ProductSize[] $sizes
 * @property-read int|null $sizes_count
 * @property-read SubCategory $sub_category
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereFamilyId($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereImage($value)
 * @method static Builder|Product whereInfo($value)
 * @method static Builder|Product whereIsActive($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereSubCategoryId($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Product extends Model implements HasMedia
{
    use HasFactory ,ImageOperations ,InteractsWithMedia;

    protected $fillable=['name','info','image','family_id','sub_category_id','is_active'];

protected $with=['sizes','colors'];

    public function family()
    {
        return $this->belongsTo(Family::class);
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class)->with('category');
    }

    public function sizes()
    {
        return $this->hasMany(ProductSize::class);
    }

    public function size($id)
    {
      $size = $this->sizes->where('id',$id)->first();
      throw_if(!$size,ValidationException::withMessages([sprintf('عفواً خطا في مقاس المنتج %s',$this->name)]));
       return $size;
    }

    public function colors()
    {
        return $this->hasMany(ProductColor::class);
    }

    public function getDefaultSizeAttribute()
    {
        return $this->sizes->where('is_default',1)->first();
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }
    public function getIsLikedAttribute()
    {
        if (auth()->check()){
            return $this->likes()->where('user_id',auth()->id())->exists();
        }
        return  false;
    }

    public function rates()
    {
        return $this->hasManyThrough(Rate::class,CartItem::class,'product_id','rateable_id');
    }

    public function getRateAttribute()
    {
        return $this->rates()->avg('value');
    }

    public static function toSelect()
    {
        return self::with('family')->get()->mapWithKeys(function ($q){
            return [$q['id']=>"$q->name || {$q->family->name}"];
        });
    }
}
