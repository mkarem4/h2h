<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Coupon
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property int $is_active
 * @property string $expire_at
 * @property string $discount
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Coupon newModelQuery()
 * @method static Builder|Coupon newQuery()
 * @method static Builder|Coupon query()
 * @method static Builder|Coupon valid($code)
 * @method static Builder|Coupon whereCode($value)
 * @method static Builder|Coupon whereCreatedAt($value)
 * @method static Builder|Coupon whereDiscount($value)
 * @method static Builder|Coupon whereExpireAt($value)
 * @method static Builder|Coupon whereId($value)
 * @method static Builder|Coupon whereIsActive($value)
 * @method static Builder|Coupon whereName($value)
 * @method static Builder|Coupon whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Coupon extends Model
{
    use HasFactory;

    protected $fillable=['name', 'code', 'is_active', 'expire_at', 'discount'];

    public function scopeValid($coupon,$code)
    {
        $coupon->where('code',$code)->whereDate('expire_at','>=',now()->toDateString())->where('is_active',1);
    }
    public function families()
    {
        return $this->belongsToMany(Family::class,FamilyCoupon::class);
    }
}
