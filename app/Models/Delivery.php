<?php

namespace App\Models;

use App\Traits\FireBase;
use App\Traits\ImageOperations;
use App\Traits\SetPass;
use Database\Factories\DeliveryFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * App\Models\Delivery
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $password
 * @property string $email
 * @property string $image
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $fcm_token_android
 * @property string|null $fcm_token_ios
 * @property int|null $city_id
 * @property-read Collection|Cart[] $carts
 * @property-read int|null $carts_count
 * @property-read City|null $city
 * @property mixed $thubnail
 * @property-read Collection|DeliveryRequest[] $requests
 * @property-read int|null $requests_count
 * @method static Builder|Delivery newModelQuery()
 * @method static Builder|Delivery newQuery()
 * @method static Builder|Delivery query()
 * @method static Builder|Delivery whereCityId($value)
 * @method static Builder|Delivery whereCreatedAt($value)
 * @method static Builder|Delivery whereEmail($value)
 * @method static Builder|Delivery whereFcmTokenAndroid($value)
 * @method static Builder|Delivery whereFcmTokenIos($value)
 * @method static Builder|Delivery whereId($value)
 * @method static Builder|Delivery whereImage($value)
 * @method static Builder|Delivery whereName($value)
 * @method static Builder|Delivery wherePassword($value)
 * @method static Builder|Delivery wherePhone($value)
 * @method static Builder|Delivery whereUpdatedAt($value)
 * @mixin Eloquent
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @method static DeliveryFactory factory(...$parameters)
 */
class Delivery extends Authenticatable implements JWTSubject
{
    use HasFactory, ImageOperations, SetPass,Notifiable;

    protected $with = ['city'];

    protected $fillable = [
        'name',
        'phone',
        'password',
        'email',
        'image',
        'fcm_token_android',
        'fcm_token_ios',
        'city_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function requests()
    {
        return $this->hasMany(DeliveryRequest::class);
    }

    public function carts()
    {
        return $this->belongsToMany(Cart::class, DeliveryRequest::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }


    public function selfNotify($data)
    {
        FireBase::notification($this,$data['title'],$data['body'],$data);
    }
}
