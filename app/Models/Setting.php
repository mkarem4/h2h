<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Setting
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $ar_value
 * @property string $en_value
 * @property string $page
 * @property string $slug
 * @property string $title
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $value
 * @method static Builder|Setting newModelQuery()
 * @method static Builder|Setting newQuery()
 * @method static Builder|Setting query()
 * @method static Builder|Setting whereArValue($value)
 * @method static Builder|Setting whereCreatedAt($value)
 * @method static Builder|Setting whereEnValue($value)
 * @method static Builder|Setting whereId($value)
 * @method static Builder|Setting whereName($value)
 * @method static Builder|Setting wherePage($value)
 * @method static Builder|Setting whereSlug($value)
 * @method static Builder|Setting whereTitle($value)
 * @method static Builder|Setting whereType($value)
 * @method static Builder|Setting whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Setting extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'type', 'ar_value', 'en_value', 'page', 'slug', 'title'];

    protected $appends = ['value'];

    public function getValueAttribute()
    {
        if (app()->getLocale() == 'en')
            return $this->en_value;
        return $this->ar_value;
    }
}
