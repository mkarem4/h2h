<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductSize
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property float $price
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $is_default
 * @property int $discount
 * @property-read mixed $final_total
 * @property-read Product $product
 * @method static Builder|ProductSize newModelQuery()
 * @method static Builder|ProductSize newQuery()
 * @method static Builder|ProductSize query()
 * @method static Builder|ProductSize whereCreatedAt($value)
 * @method static Builder|ProductSize whereDiscount($value)
 * @method static Builder|ProductSize whereId($value)
 * @method static Builder|ProductSize whereIsDefault($value)
 * @method static Builder|ProductSize whereName($value)
 * @method static Builder|ProductSize wherePrice($value)
 * @method static Builder|ProductSize whereProductId($value)
 * @method static Builder|ProductSize whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ProductSize extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'price', 'product_id', 'discount', 'is_default'];

    protected $casts=[
        'price'=>'double',
        'discount'=>'integer'
    ];
    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getFinalTotalAttribute()
    {
        return round( $this->price - ($this->price * $this->discount / 100),2);
    }
}
