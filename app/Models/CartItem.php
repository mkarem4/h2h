<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\CartItem
 *
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 * @property int $product_size_id
 * @property int|null $product_color_id
 * @property float $price
 * @property float $discount
 * @property int $quantity
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Cart $cart
 * @property-read ProductColor|null $color
 * @property-read mixed $total
 * @property-read Product $product
 * @property-read Rate|null $rate
 * @property-read ProductSize $size
 * @method static Builder|CartItem newModelQuery()
 * @method static Builder|CartItem newQuery()
 * @method static Builder|CartItem query()
 * @method static Builder|CartItem whereCartId($value)
 * @method static Builder|CartItem whereCreatedAt($value)
 * @method static Builder|CartItem whereDiscount($value)
 * @method static Builder|CartItem whereId($value)
 * @method static Builder|CartItem wherePrice($value)
 * @method static Builder|CartItem whereProductColorId($value)
 * @method static Builder|CartItem whereProductId($value)
 * @method static Builder|CartItem whereProductSizeId($value)
 * @method static Builder|CartItem whereQuantity($value)
 * @method static Builder|CartItem whereUpdatedAt($value)
 * @mixin Eloquent
 */
class CartItem extends Model
{
    use HasFactory;
    protected $fillable=['cart_id', 'product_id', 'product_size_id', 'product_color_id', 'price', 'discount', 'quantity'];

    protected $casts=[
        'price'=>'double',
        'discount'=>'double',
        'quantity'=>'integer'
    ];
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function size()
    {
     return $this->belongsTo(ProductSize::class,'product_size_id');
    }

    public function color()
    {
        return $this->belongsTo(ProductColor::class,'product_color_id');
    }
    public function rate()
    {
        return $this->morphOne(Rate::class,'rateable');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function getTotalAttribute()
    {
        return ($this->price - ($this->price * $this->discount/100)) * $this->quantity;
    }
}
