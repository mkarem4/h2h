<?php

namespace App\Models;

use App\Traits\ImageOperations;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Staudenmeir\EloquentHasManyDeep\HasRelationships;

/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $image
 * @property mixed $thubnail
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @property-read Collection|SubCategory[] $sub_categories
 * @property-read int|null $sub_categories_count
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereImage($value)
 * @method static Builder|Category whereName($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @mixin Eloquent
 */
class Category extends Model
{
    use HasFactory , ImageOperations  ,HasRelationships;
    protected $fillable=['name','image','description'];

    public function sub_categories()
    {
        return $this->hasMany(SubCategory::class);
    }

    public function families()
    {
        return $this->hasManyDeep(Family::class,[SubCategory::class,Product::class],['category_id','sub_category_id','id'],['id','id','family_id']);
    }

    public function products()
    {
        return $this->hasManyThrough(Product::class,SubCategory::class) ;
    }
}
