<?php

namespace App\Models;

use App\Traits\ImageOperations;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\SubCategory
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string $image
 * @property-read Category $category
 * @property mixed $thubnail
 * @property-read Collection|Product[] $products
 * @property-read int|null $products_count
 * @method static Builder|SubCategory newModelQuery()
 * @method static Builder|SubCategory newQuery()
 * @method static Builder|SubCategory query()
 * @method static Builder|SubCategory whereCategoryId($value)
 * @method static Builder|SubCategory whereCreatedAt($value)
 * @method static Builder|SubCategory whereId($value)
 * @method static Builder|SubCategory whereImage($value)
 * @method static Builder|SubCategory whereName($value)
 * @method static Builder|SubCategory whereUpdatedAt($value)
 * @mixin Eloquent
 */
class SubCategory extends Model
{
    use HasFactory , ImageOperations;

    protected $fillable=['name','category_id','image'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
