<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\ProductColor
 *
 * @property int $id
 * @property int $product_id
 * @property string $name
 * @property string $code
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Product $product
 * @method static Builder|ProductColor newModelQuery()
 * @method static Builder|ProductColor newQuery()
 * @method static Builder|ProductColor query()
 * @method static Builder|ProductColor whereCode($value)
 * @method static Builder|ProductColor whereCreatedAt($value)
 * @method static Builder|ProductColor whereId($value)
 * @method static Builder|ProductColor whereName($value)
 * @method static Builder|ProductColor whereProductId($value)
 * @method static Builder|ProductColor whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ProductColor extends Model
{
    use HasFactory;
    protected $fillable=['name','code','product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
