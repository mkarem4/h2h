<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class NewOrderNotification extends Notification
{
    use Queueable;

    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        //
        $this->data = $data;
        $this->data['type'] = 'order';
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [FireBaseChannel::class];
    }

    public function toFireBase($notifiable)
    {
        $this->data['tile'] = 'تطبيق من البيت للبيت';
        $this->data['body'] = 'هناك طلب جديد';
        $notifiable->selfNotify($this->data);

    }

}
