<?php

namespace App\Notifications;

use App\Traits\FireBase;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class OrderStatusChanged extends Notification
{
    use Queueable;

    private $order;

    private $data;
    /**
     * Create a new notification instance.
     *
     * @param $order
     * @param $status
     */
    public function __construct($order)
    {
        //
        $this->order = $order;
        $this->data=[
            'title'=>'تطبيق من البيت للبيت|| هناك تحديث علي حالة الطلب ',
            'body'=>sprintf("حالة الطلب رقم %s هي %s",$order->id,orderStatuses()[$order->status]),
            'order_id'=>$order->id,
            'status'=>$order->status,
            'type'=>'changeOrderStatus'
        ];
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database',FireBaseChannel::class];
    }



    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->data;
    }

    public function toFireBase($notifiable)
    {
        $data=$this->data;
        FireBase::notification($notifiable,$data['title'],$data['body'],$data);
    }
}
