<?php
namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('permissions')->delete();

        DB::table('permissions')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'admins',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الادارة',
            ),
            1 =>
            array (
                'id' => 2,
                'name' => 'add admin',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة مدير',
            ),
            2 =>
            array (
                'id' => 3,
                'name' => 'users',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'العملاء',
            ),
            3 =>
            array (
                'id' => 4,
                'name' => 'add user',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة عميل',
            ),
            4 =>
            array (
                'id' => 5,
                'name' => 'families',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الاسر المنتجة',
            ),
            5 =>
            array (
                'id' => 6,
                'name' => 'add family',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة اسرة',
            ),
            6 =>
            array (
                'id' => 7,
                'name' => 'cities',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'المدن',
            ),
            7 =>
            array (
                'id' => 8,
                'name' => 'add city',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة مدينة',
            ),
            8 =>
            array (
                'id' => 9,
                'name' => 'categories',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الاقسام الرئيسة',
            ),
            9 =>
            array (
                'id' => 10,
                'name' => 'add category',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة قسم رئيسى',
            ),
            10 =>
            array (
                'id' => 11,
                'name' => 'sub-categories',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الاقسام الفرعية',
            ),
            11 =>
            array (
                'id' => 12,
                'name' => 'add subcategory',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة قسم فرعى',
            ),
            12 =>
            array (
                'id' => 13,
                'name' => 'products',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'المنتجات',
            ),
            13 =>
            array (
                'id' => 14,
                'name' => 'add product',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة منتج',
            ),
            14 =>
            array (
                'id' => 15,
                'name' => 'deliveries',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'مناديب التوصيل',
            ),
            15 =>
            array (
                'id' => 16,
                'name' => 'add delivery',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة مندوب',
            ),
            16 =>
            array (
                'id' => 17,
                'name' => 'banners',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'البنرات',
            ),
            17 =>
            array (
                'id' => 18,
                'name' => 'add banner',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة بنر',
            ),
            18 =>
            array (
                'id' => 19,
                'name' => 'coupons',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الكوبونات',
            ),
            19 =>
            array (
                'id' => 20,
                'name' => 'add coupons',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'اضافة كوبون',
            ),
            20 =>
            array (
                'id' => 21,
                'name' => 'orders',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الطلبات',
            ),
            21 =>
            array (
                'id' => 22,
                'name' => 'contacts',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'رشائل التواصل',
            ),
            22 =>
            array (
                'id' => 23,
                'name' => 'roles',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'الصلاحيات',
            ),
            23 =>
            array (
                'id' => 24,
                'name' => 'rates',
                'guard_name' => 'Admin',
                'created_at' => NULL,
                'updated_at' => NULL,
                'ar_name' => 'التقيمات',
            ),
        ));


    }
}
