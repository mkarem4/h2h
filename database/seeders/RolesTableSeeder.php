<?php
namespace Database\Seeders;
use DB;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('roles')->delete();

        DB::table('roles')->insert(array (
            0 =>
            array (
                'id' => 4,
                'name' => 'super-admin',
                'guard_name' => 'Admin',
                'created_at' => '2021-01-08 22:11:25',
                'updated_at' => '2021-01-08 22:11:25',
            ),
        ));


    }
}
