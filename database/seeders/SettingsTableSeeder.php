<?php
namespace Database\seeders;
use DB;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        DB::table('settings')->delete();

        DB::table('settings')->insert(array (
            0 =>
            array (
                'ar_value' => '10',
                'created_at' => '2020-11-10 01:01:28',
                'en_value' => '10',
                'id' => 1,
                'name' => 'tax',
                'page' => 'اعدادات',
                'slug' => 'fees',
                'title' => 'الضريبة',
                'type' => 'number',
                'updated_at' => '2021-02-01 14:58:37',
            ),
            1 =>
            array (
                'ar_value' => '25',
                'created_at' => '2020-11-10 01:01:28',
                'en_value' => '25',
                'id' => 2,
                'name' => 'delivery_cost',
                'page' => 'اعدادات',
                'slug' => 'fees',
                'title' => 'تكلفة التوصيل',
                'type' => 'number',
                'updated_at' => '2021-02-01 14:58:37',
            ),
            2 =>
            array (
                'ar_value' => '40',
                'created_at' => '2020-11-10 01:01:28',
                'en_value' => '40',
                'id' => 3,
                'name' => 'max_delivery',
                'page' => 'اعدادات',
                'slug' => 'fees',
                'title' => 'الحد الاقصي لتكلفة التوصيل',
                'type' => 'number',
                'updated_at' => '2021-02-01 14:58:37',
            ),
            3 =>
            array (
                'ar_value' => '18',
                'created_at' => '2020-11-10 01:01:28',
                'en_value' => '18',
                'id' => 4,
                'name' => 'min_delivery',
                'page' => 'اعدادات',
                'slug' => 'fees',
                'title' => 'الحد الادني لتكلفة التوصيل',
                'type' => 'number',
                'updated_at' => '2021-02-01 14:58:37',
            ),
            4 =>
            array (
                'ar_value' => '0.5',
                'created_at' => '2020-11-10 01:01:28',
                'en_value' => '0.5',
                'id' => 5,
                'name' => 'km_cost',
                'page' => 'اعدادات',
                'slug' => 'fees',
                'title' => 'تكلفة كليو متر التوصيل',
                'type' => 'number',
                'updated_at' => '2021-02-01 14:58:37',
            ),
        ));


    }
}
