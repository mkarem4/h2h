<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::truncate();
        Setting::create([
            'name'=>'tax',
            'type'=>'number',
            'ar_value'=>10,
            'en_value'=>10,
            'page'=>'اعدادات',
            'slug'=>'tax',
            'title'=>'الضريبة'
        ]); Setting::create([
        'name'=>'delivery_cost',
        'type'=>'number',
        'ar_value'=>10,
        'en_value'=>10,
        'page'=>'اعدادات',
        'slug'=>'delivery_cost',
        'title'=>'تكلفة التوصيل'
    ]);
    }
}
