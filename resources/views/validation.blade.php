@if (isset($errors) and  $errors->count() > 0)
    <div class="alert alert-danger alert-dismissible fade show" role="alert" style="opacity:.8">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <ul class="list-group">
            @foreach ($errors->all() as $error)
                <li class="list-group-item center" style="color:#000;font-weight:bold;">{{ $error }}</li>
            @endforeach
        </ul>

    </div>
@endif
