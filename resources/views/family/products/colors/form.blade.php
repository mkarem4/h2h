<x-text name="name" title="اسم اللون"></x-text>
<div class="form-group form-float">
    <label class="form-label">اختار اللون</label>
    {!! Form::color('code',null,['class'=>'form-control','placeholder'=>'حدد اللون'])!!}
    <div class="form-line">
    </div>
</div>
@isset($product)
    {!! Form::hidden('product_id',$product->id) !!}
@endisset


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

