@extends('family.layout.app')

@section('title')
    الوان المنتج {{$product->name}}
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    @include('validation')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        الوان المنتج {{$product->name}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('family.product-colors.create',['product'=>$product])}}">
                            <button class="btn btn-success">اضافة لون جديد  </button>
                        </a>
                    </ul>
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم اللون </th>
                            <th>اللون </th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product->colors as $key=>$color)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$color->name}}</td>
                                <td><i class="fa fa-square fa-3x" style="color: {{$color->code}}"></i></td>
                                <td>
                                <a href="{{route('family.product-colors.edit',$color->id)}}"
                                   class="btn btn-info btn-circle "
                                   data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="Delete('{{$color->id}}')" class="btn btn-info btn-circle "
                                   data-original-title="حذف">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
                                </a>
                                {!! Form::open(['route'=>['family.product-colors.destroy',$color->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$color->id]) !!}
                                {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('family.layout.datatable-scripts')

    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا القسم الرئيسي ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });
        }
    </script>


@endsection
