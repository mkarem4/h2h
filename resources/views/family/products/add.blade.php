@extends('family.layout.app')

@section('title')
إضافة  منتج
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>إضافة  منتج جديد</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('family.products.index')}}">   <button class="btn btn-danger">كل المنتجات </button></a>
</ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'family.products.store', 'method' => 'Post','files'=>true]) !!}
            @include('family.products.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
