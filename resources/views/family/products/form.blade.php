<x-text title="اسم المنتج" name="name" />
<x-text title="وصف المنتج" name="info" />
<x-select title="القسم الفرعي" :items="sub_categories()" name="sub_category_id"/>
<x-select title="الحالة" :items="[0=>'غير مفعل',1=>'مفعل']" name="is_active"/>
<x-file name="image" title="الصورة الرئيسية"/>
@if (isset($product))
    <div class="form-group ">
        <label>الصورة الرئيسية: </label>
        <a href="{{$product->image}}" target="_blank"><img src="{{$product->image}}" class="img-responsive thumbnail"
                                                           style="width:100px; height:100px"></a>
    </div>
@endif
<div class="form-group ">
    <label class="form-label">صور المعرض</label>
    {!! Form::file('images[]',['class'=>'form-control','multiple'])!!}
    <div class="form-line">
    </div>
</div>
@if (isset($product))
    <div class="row clearfix">
        <label>صور المعرض: </label>
        <br>
        @foreach($product->getMedia('images') as $image)
            <div class="col-xs-1 align-center">
                <a href="{{$image->getUrl()}}" target="_blank"><img src="{{$image->getUrl()}}" class="img-responsive thumbnail" style="width:100px; height:100px"></a>
                <a href="{{route('family.gallery.destroy',$image)}}" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></a>
            </div>
        @endforeach

    </div>
@endif

@if (!isset($product))
    <x-text name="size_name" title="اسم المقاس"></x-text>
    <x-number name="price" title="السعر"></x-number>
    <x-text name="color_name" title="اسم اللون"></x-text>
    <div class="form-group form-float">
        <label class="form-label">اختار اللون</label>
        {!! Form::color('color_code','#010101',['class'=>'form-control','placeholder'=>'حدد اللون'])!!}
        <div class="form-line">
        </div>
    </div>
@endif



<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

