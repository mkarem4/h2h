@extends('family.layout.app')

@section('title')
    كل  المنتجات
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    @include('validation')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل المنتجات
                    </h2>
            {{--        <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.products.create')}}">
                            <button class="btn btn-success">إضافة منتج جديد</button>
                        </a>
                    </ul>--}}
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الاسم </th>
                            <th>الوصف </th>
                            <th>القسم الرئيسي </th>
                            <th>القسم الفرعي </th>
                            <th>عدد المقاسات </th>
                            <th>عدد الالوان </th>
                            <th>الحالة </th>
                            <th>التقييم </th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $key=>$product)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{\Illuminate\Support\Str::limit($product->info,100)}}</td>
                                <td>{{$product->sub_category->category->name}}</td>
                                <td>{{$product->sub_category->name}}</td>
                                <td><a class="btn btn-danger" href="{{route('family.product-sizes.show',$product->id)}}">{{$product->sizes->count()}}</a></td>
                                <td><a class="btn btn-primary" href="{{route('family.product-colors.show',$product->id)}}">{{$product->colors->count()}}</a></td>
                                <td>{{$product->is_active?'مفعل':'غير مفعل'}}</td>
                                <td>{{$product->rates->avg('value')}}</td>
                                <td>
                                <a href="{{route('family.products.edit',$product->id)}}"
                                   class="btn btn-info btn-circle "
                                   data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="Delete('{{$product->id}}')" class="btn btn-info btn-circle "
                                   data-original-title="حذف">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
                                </a>
                                {!! Form::open(['route'=>['family.products.destroy',$product->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$product->id]) !!}
                                {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('family.layout.datatable-scripts')

    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المنتج ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });
        }
    </script>


@endsection
