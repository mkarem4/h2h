@extends('family.layout.app')

@section('title')
    إضافة  مقاس جديد للمنتج
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>إضافة  مقاس جديد للمنتج</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('family.product-sizes.show',$product)}}">   <button class="btn btn-danger">كل مقاسات المنتج </button></a>
</ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'family.product-sizes.store', 'method' => 'Post']) !!}
            @include('family.products.sizes.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
