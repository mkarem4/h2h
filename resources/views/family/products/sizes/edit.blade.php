@extends('family.layout.app')

@section('title')
تعديل مقاس المنتج
{{ $size->product->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>تعديل مقاس المنتج       {{ $size->product->name }}</h2>
      </div>
      <div class="body">
        {!!Form::model($size , ['route' => ['family.product-sizes.update' , $size->id] ,'method' => 'PATCH']) !!}
        @include('family.products.sizes.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
