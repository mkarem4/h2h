@extends('family.layout.app')

@section('title')
    مقاسات المنتج {{$product->name}}
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    @include('validation')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        مقاسات المنتج {{$product->name}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.product-sizes.create',['product'=>$product])}}">
                            <button class="btn btn-success">اضافة لون جديد</button>
                        </a>
                    </ul>
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم المقاس</th>
                            <th>السعر</th>
                            <th>الخصم</th>
                            <th>السعر النهائي</th>
                            <th>المقاس الافتراضي</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($product->sizes as $key=>$size)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$size->name}}</td>
                                <td>{{$size->price}}</td>
                                <td>{{$size->discount}}</td>
                                <td>{{$size->final_total}}</td>
                                <td>
                                    @if ($size->is_default)
                                        <i class="fa fa-2x fa-times"></i>
                                    @else
                                        <a href="{{route('family.product-size-toggle',['product'=>$product,'productSize'=>$size])}}">
                                            <i class="fa fa-2x fa-check-circle"></i>
                                        </a>
                                    @endif
                                </td>
                                <td>
                                    <a href="{{route('family.product-sizes.edit',$size->id)}}"
                                       class="btn btn-info btn-circle "
                                       data-original-title="تعديل">
                                        <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                    </a>
                                    <a href="#" onclick="Delete('{{$size->id}}')" class="btn btn-info btn-circle "
                                       data-original-title="حذف">
                                        <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
                                    </a>
                                    {!! Form::open(['route'=>['family.product-sizes.destroy',$size->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$size->id]) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('family.layout.datatable-scripts')

    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المقاس ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });
        }
    </script>


@endsection
