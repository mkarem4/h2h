@extends('family.layout.app')

@section('title')
    الطلبات
@endsection
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        تصفية الطلبات
                    </h2>
                </div>
                <div class="body">
                    {!! Form::open(['route'=>'family.orders.index','method'=>'GET']) !!}
                    <div class="row">
                        <div class="form-group">
                            <label class="form-label">الحالة</label>
                            {!! Form::select('status[]', orderStatuses() , null , ['class' => 'form-control select2 ','multiple']) !!}
                            <div class="form-line">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <label class="form-label"></label>
                            {!! Form::date('from',null,['class'=>'form-control']) !!}
                            <div class="form-line">
                            </div>
                        </div>
                        <div class="form-group form-float">
                            <label class="form-label"></label>
                            {!! Form::date('to',null,['class'=>'form-control']) !!}
                            <div class="form-line">
                            </div>
                        </div>
                        {!! Form::submit('ارسل',['class'=>'btn btn-success']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل الطلبات
                    </h2>
                </div>
                <div class="body">
                    <table class="table display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم العميل</th>
                            <th>رقم جوال العميل</th>
                            <th>الحالة</th>
                            <th>اجمالي الطلب</th>
                            <th>تاريخ الانشاء</th>
                            <th>تاريخ الطلب</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($carts as $key=>$cart)
                            <tr>
                                <td>{{$cart->id}}</td>
                                <td>{{$cart->user->name}}</td>
                                <td>{{$cart->user->phone}}</td>
                                <td>{{__($cart->status)}}</td>
                                <td>{{$cart->grand_total}}</td>
                                <td>{{$cart->created_at->toDateString()}}</td>
                                <td>{{$cart->date}}</td>
                                <td><a href="{{route('family.orders.show',$cart)}}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    @include('family.layout.datatable-scripts')

@endpush
