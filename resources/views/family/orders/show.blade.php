@extends('family.layout.app')

@section('title')
    عرض الطلب رقم {{$cart->id }}
@endsection
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (in_array($cart->status,['pending','accepted']))
            <div class="card animated slideInUp">
                <div class="header">
                    العمليات
                </div>
                <div class="body clearfix">
                    @if($cart->status=='pending')
                    <div class="col-xs-6"><a href="{{route('family.order.change.status',['order'=>$cart,'status'=>'rejected'])}}" id="reject" class="btn btn-danger btn-lg form-control">رفض</a></div>
                    <div class="col-xs-6"><a href="{{route('family.order.change.status',['order'=>$cart,'status'=>'accepted'])}}" id="accept" class="btn btn-success btn-lg form-control">قبول</a></div>
                    @endif
                    @if ($cart->status=='accepted')
                    <div class="col-xs-12"><a href="{{route('family.order.change.status',['order'=>$cart,'status'=>'wait_for_delivery'])}}" id="ready" class="btn btn-primary btn-lg form-control">تم التجهيز</a></div>
                        @endif
                </div>
            </div>
            @endif
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        تفاصيل الطلب
                    </h2>
                    <br>

                    <table class="table table-hover table-striped responsive-table table-bordered">
                        <tr class="bold">
                            <td><b>البيان</b></td>
                            <td><b>القيمة</b></td>
                        </tr>
                        <tr>
                            <td>رقم الطلب</td>
                            <td>{{$cart->id}}</td>
                        </tr>
                        <tr>
                            <td>حالة الطلب</td>
                            <td>{{__($cart->status)}}</td>
                        </tr>
                        <tr>
                            <td>سبب الرفض ان وجد</td>
                            <td>{{$cart->reject_reason}}</td>
                        </tr>
                        <tr>
                            <td>اسم العميل</td>
                            <td>{{$cart->user->name}}</td>
                        </tr>
                        <tr>
                            <td>رقم تيلفون العميل</td>
                            <td>{{$cart->user->phone}}</td>
                        </tr>
                        <tr>
                            <td>اسم الاسرة</td>
                            <td>{{$cart->family->phone}}</td>
                        </tr>
                        <tr>
                            <td>رقم تيلفون الاسرة</td>
                            <td>{{$cart->family->phone}}</td>
                        </tr>
                        <tr>
                            <td>العنوان</td>
                            <td>{{$cart->address->address}}</td>
                        </tr>
                        <tr>
                            <td>ملاحظات العميل</td>
                            <td>{{$cart->notes}}</td>
                        </tr>
                        <tr>
                            <td>اجمالي المنتجات</td>
                            <td>{{$cart->total}}</td>
                        </tr>
                        <tr>
                            <td>الضريبة</td>
                            <td>{{$cart->tax}}</td>
                        </tr>
                        <tr>
                            <td>تكلفة التوصيل</td>
                            <td>{{$cart->delivery_cost}}</td>
                        </tr>
                        <tr>
                            <td>الخصم</td>
                            <td>{{$cart->discount}}</td>
                        </tr>
                        <tr>
                            <td>الاجمالي النهائي</td>
                            <td>{{$cart->grand_total}}</td>
                        </tr>
                    </table>
                </div>
                <div class="body">
                    <table class="dttable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم المنتج</th>
                            <th>مقاس المنتج</th>
                            <th>اللون</th>
                            <th>الكمية</th>
                            <th>السعر</th>
                            <th>الخصم</th>
                            <th>الاجمالي</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cart->items as $item)
                            <tr>
                                <td>{{++$loop->index}}</td>
                                <td>{{$item->product->name}}</td>
                                <td>{{$item->size->name}}</td>
                                <td>{{optional($item->color)->name}}</td>
                                <td>{{$item->quantity}}</td>
                                <td>{{$item->price}}</td>
                                <td>{{$item->discount}}</td>
                                <td>{{$item->total}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript"
            src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function () {
            var table = $('.dttable').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'searching': true
            });

            $('#accept').click(function (e){
                e.preventDefault();
                swal('تأكيد','#accept')
            });
            $('#reject').click(function (e){
                e.preventDefault();
                console.log('test')
                Swal.fire({
                    title: "هل انت متأكد ؟",
                    text: "هل تريد إلغاء هذا الطلب ؟",
                    icon: 'question',
                    input:"text",
                    inputAttributes: {
                        autocapitalize: 'off',
                        placeholder:"سبب الرفض",
                        name:"reject_reason"
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'موافق !',
                    cancelButtonText:'الغاء'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url =$('#reject').attr('href');
                        if (result.value !='')
                        {
                            url+='?reject_reason='+encodeURI(result.value);
                        }
                        console.log(url)
                        document.location.href=url;
                    }
                });

            });



            $('#ready').click(function (e){
                e.preventDefault();
                swal('تأكيد تجهيز','#ready')
            });

        });

        function swal(text,selector){
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد "+text+" هذا الطلب ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText:'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.location.href= $(selector).attr('href');
                }else{
                    Swal.fire(
                        'تم الالغاء!',
                        'success'
                    )
                }
            });

        }
    </script>
@endpush
