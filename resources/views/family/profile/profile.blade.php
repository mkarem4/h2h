@extends('family.layout.app')

@section('title')
    تعديل الاسرة
    {{ $user->name }}
@endsection
@section('content')
    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card ">
                <div class="header">
                    <h2>تعديل الاسرة       {{ $user->name }}</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.families.index')}}">   <button class="btn btn-danger">كل الاسر</button></a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($user , ['route' => ['family.profile.store' ] , 'method' => 'POST']) !!}
                    <x-text name="name" title="الاسم"></x-text>
                    <x-text name="email" title="البريد الالكتروني"></x-text>
                    <x-password/>
                    <x-text name="phone" title="رقم الهاتف"></x-text>
                    <x-text name="address" title="العنوان"></x-text>
                    <x-select title="الحالة" :items="[0=>'غير مفعل',1=>'مفعل']" name="is_online"/>
                    <div class="form-group form-float">
                        <label class="form-label">المدينة</label>
                        {!! Form::select('city_id',cities(),null,['class'=>'form-control ms select2 '])!!}
                        <div class="form-line">
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="form-label">موقعها</label>
                        {!! Form::hidden("lat",isset($user) && $user->lat ? null : 24.6921856,['id'=>'lat'])!!}
                        {!! Form::hidden("lng",isset($user) && $user->lng ? null : 46.7378258,['id'=>'lng'])!!}
                        <div id="map" style="height: 400px"></div>
                    </div>
                    <x-text-area name="bio" title="نبذة عن الاسرة"/>
                    <x-file name="image" title="الصورة الشخصية" />
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>

                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
@endsection
@push('scripts')
    {!! Html::script('_dashboard/js/map.js') !!}
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&callback=initMap&libraries=&v=weekly"></script>
@endpush
