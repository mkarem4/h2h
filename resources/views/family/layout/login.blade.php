<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title>لوحة التحكم - تسجيل الدخول   </title>
  <!-- Favicon-->
{{--    <link rel="icon" href="{{asset('assets/images/logo.png')}}">--}}
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

  <!-- Bootstrap Core Css -->
  {!!Html::style('_dashboard/plugins/bootstrap/css/bootstrap.css')!!}

  <!-- Waves Effect Css -->
  {!!Html::style('_dashboard/plugins/node-waves/waves.css')!!}

  <!-- Animation Css -->
  {!!Html::style('_dashboard/plugins/animate-css/animate.css')!!}

  <!-- Custom Css -->
  {!!Html::style('_dashboard/css/style.css')!!}
{{--  {!!Html::style('_dashboard/css/custom.css')!!}--}}

  <style>
*
{
    font-family: 'Changa', sans-serif;
}
  </style>

</head>

<body class="login-page" dir="rtl">
  <div class="login-box">
    <div class="logo">
{{--      <a href="javascript:void(0);"><img src="{{asset('assets/images/logo.png')}}"></a>--}}
    </div>
    <div class="card animated slideInUp">
      <div class="body">

        <form id="sign_in" role="form" method="POST" action="{{ route('family.login') }}">
          @if (count($errors) > 0)
          <div class="alert alert-info">
            <ul>
              @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
              @endforeach
            </ul>
          </div>
          @endif

          {{ csrf_field() }}
          <div class="msg">تسجيل الدخول || الاسر المنتجة</div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">email</i>
            </span>
            <div class="form-line">
              <input type="email" class="form-control" id="mail" name="email" value="{{ old('email') }}"  placeholder="البريد الالكترونى"  autofocus>
            </div>
          </div>
          <div class="input-group">
            <span class="input-group-addon">
              <i class="material-icons">lock</i>
            </span>
            <div class="form-line">
              <input type="password" class="form-control" name="password" placeholder="كلمة المرور" >
            </div>

          </div>
          <div class="row">

            <div class="col-xs-12">
              <button class="btn btn-block bg-blue waves-effect" type="submit">دخول</button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>


  <!-- Jquery Core Js -->
  {!!Html::script('_dashboard/plugins/jquery/jquery.min.js')!!}

  <!-- Bootstrap Core Js -->
  {!!Html::script('_dashboard/plugins/bootstrap/js/bootstrap.js')!!}

  <!-- Waves Effect Plugin Js -->
  {!!Html::script('_dashboard/plugins/node-waves/waves.js')!!}

  <!-- Validation Plugin Js -->
  {!!Html::script('_dashboard/plugins/jquery-validation/jquery.validate.js')!!}

  <!-- Custom Js -->
  {!!Html::script('_dashboard/js/admin.js')!!}
  {!!Html::script('_dashboard/js/pages/examples/sign-in.js')!!}
</body>

</html>
