<li>
    <a class="check_active" href="{{route('family.dashboard')}}">
        <i class="material-icons">home</i>
        <span>الصفحه الرئيسيه </span>
    </a>
</li>
<li>
    <a class="check_active" href="{{route('family.profile.index')}}">
        <i class="material-icons">person</i>
        <span> الملف الشخصي </span>
    </a>
</li>
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>المنتجات </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('family.products.index')}}">
                <span> كل المنتجات </span>
            </a>
        </li>
      <li>
            <a class="check_active" href="{{route('family.products.create')}}">
                <span>اضافه منتج</span>
            </a>
        </li>
    </ul>
</li>

<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الطلبات</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('family.orders.index')}}">
                <span> الطلبات </span>
            </a>
        </li>
    </ul>
</li>


<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>مناديب التوصيل</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('family.delivery.index')}}">
                <span> مناديب التوصيل </span>
            </a>
        </li>
    </ul>
</li>
