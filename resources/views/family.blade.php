<!DOCTYPE html>
<html lang="{{  app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>سجل كأسرة</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
    {!!Html::style('_dashboard/icofont/icofont.min.css')!!}

    <!-- Multi Select Css -->
    {!!Html::style('_dashboard/plugins/multi-select/css/multi-select.css')!!}
    <!-- Bootstrap Core Css -->
    {!!Html::style('_dashboard/plugins/bootstrap/css/bootstrap.min.css')!!}
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-cSfiDrYfMj9eYCidq//oGXEkMc0vuTxHXizrMOFAaPsLt1zoCUVnSsURN+nef1lj" crossorigin="anonymous">
    <!-- Waves Effect Css -->
    {!!Html::style('_dashboard/plugins/node-waves/waves.min.css')!!}
    <!-- Animation Css -->
    {!!Html::style('_dashboard/plugins/animate-css/animate.min.css')!!}
    <!-- Custom Css -->
    {!!Html::style('_dashboard/css/style.min.css')!!}
    @yield('header')
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    {!!Html::style('_dashboard/css/themes/theme-red.min.css')!!}
    {!!Html::style('_dashboard/css/rtl.css')!!}
    {!!Html::style('_dashboard/css/materialize.css')!!}
    {{-- {!!Html::style('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!}--}}
    {!!Html::style('_dashboard/plugins/bootstrap-select/css/bootstrap-select.css')!!}
    {{-- {!!Html::style('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')!!}--}}


    {!!Html::style('_dashboard/css/themes/all-themes.css')!!}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"/>

    <!--- new style -->
    {!!Html::style('_dashboard/css/custom.css')!!}
    {!!Html::style('_dashboard/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!}

    @stack('style')
    <style>
        .dropdown-menu > li > a {
            color: darkred !important;
            background-color: white !important;
        }

        .navbar {
            background-color: #7F1212 !important;
        }

        .sidebar .user-info {
            height: 82px;
        }

        .img100 i {
            color: #7F1212 !important;
            font-size: 2em;
        }

        .img200 i {
            color: #7F1212 !important;
            font-size: 13em;
        }

        .image-preview {
            width: 75px;
            height: 75px;
        }
    </style>

</head>

<body>
<p class="theme-red"></p>
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="preloader">
            <div class="spinner-layer pl-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div>
                <div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
        <p>رجاءً انتظر...</p>
    </div>
</div>
<!-- #END# Page Loader -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->

<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="bars"></a>
            <a class="navbar-brand" href="/">الرئيسية</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <div class="nav-logo">
                <a href="/"><img src="{{asset('_dashboard/images/h2h_logo.jpg')}}"></a>
            </div>
            <!--  <div class="skwed"><h3> من البيت للبيت</h3></div> -->
            <ul class="nav navbar-nav navbar-left">

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->


<section class="content">
    <div class="container-fluid" style="width: 130%">
        @include('dashboard.layout.validation')

        <!-- Basic Validation -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card ">
                    <div class="header">
                        <h2>إضافة اسرة</h2>
                    </div>
                    <div class="body">
                        {!!Form::open( ['route' => 'web.store_family' ,'class'=>'form', 'method' => 'Post','files'=>true]) !!}

                        <div class="row">

                            <x-text name="name" title="اسم المشروع"></x-text>
                            <x-text name="email" title="البريد الالكتروني"></x-text>
                            <x-password/>
                            <x-text name="phone" title="رقم الهاتف"></x-text>
                            <div class="col-md-6">
                                <x-file name="image" title="شعار المشروع"/>
                            </div>
{{--                            <x-number name="order" title="الترتيب"></x-number>--}}
{{--                            <div class="col-md-6">--}}
{{--                                <div class="form-group ">--}}
{{--                                    <label class="form-label">تاريخ انتهاء الترتيب</label>--}}
{{--                                    {!! Form::date('order_until',null,['class'=>'form-control','placeholder'=>'تاريخ انتهاء الترتيب'])!!}--}}
{{--                                    <div class="form-line">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
                            <x-text name="address" title="العنوان التفصيلي"></x-text>


                            <x-select title="المدينة" name="city_id" :items="cities()"></x-select>


{{--                            @if(!isset($user) or !$user->subscriptions()->exists())--}}
{{--                                <x-plans :model="@$user"></x-plans>--}}
{{--                            @endif--}}

                            {{--                            <x-text title="نوع الطلب" name="order_type" :float="false"></x-text>--}}
                            <x-select title="نوع الطلب" name="order_type"
                                      :items="['on_delivery' => 'عند الطلب' ,'immediate_delivery' => 'تسليم فوري']"
                                      :float="false"></x-select>

                            <div class="form-group col-md-12 ">
                                <label class="form-label">موقعها</label>
                                {!! Form::text("lat",isset($user) && $user->lat ? null : 31.95080183791635,['id'=>'lat'])!!}
                                {!! Form::text("lng",isset($user) && $user->lng ? null : 35.91058039003076,['id'=>'lng'])!!}
                                <div id="map" style="height: 400px"></div>
                            </div>
                            <x-text-area name="bio" title="نبذة عن الاسرة"/>
                            {{--                            <x-select title="التفعيل" name="is_active" :items="['غير مفعل','مفعل']"></x-select>--}}

                        </div>

                        <button class="btn btn-primary waves-effect" type="submit">حفظ</button>


                        @push('scripts')
                            {!! Html::script('_dashboard/js/map.js') !!}
                            <script
                                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV3ZKirEWJIMW8T4SWoK9rXd7VThciUtw&callback=initMap&libraries=&v=weekly"></script>
                        @endpush


                        {!!Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Validation -->
    </div>
</section>

<!-- Jquery Core Js -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js"
        integrity="sha512-DUC8yqWf7ez3JD1jszxCWSVB0DMP78eOyBpMa5aJki1bIRARykviOuImIczkxlj1KhVSyS16w2FSQetkD4UU2w=="
        crossorigin="anonymous"></script>
<!-- Bootstrap Core Js -->
{!!Html::script('_dashboard/plugins/bootstrap/js/bootstrap.min.js')!!}
<!-- Select Plugin Js -->
{!!Html::script('_dashboard/plugins/bootstrap-select/js/bootstrap-select.min.js')!!}
<!-- Slimscroll Plugin Js -->
{!!Html::script('_dashboard/plugins/jquery-slimscroll/jquery.slimscroll.min.js')!!}
<!-- Waves Effect Plugin Js -->
{!!Html::script('_dashboard/plugins/node-waves/waves.min.js')!!}
<!-- Custom Js -->
{!!Html::script('_dashboard/js/admin.js')!!}

{!!Html::script('_dashboard/js/pages/ui/modals.js')!!}
{!!Html::script('_dashboard/js/demo.js')!!}
{{--{!!Html::script('assets/js/pages/forms/basic-form-elements.js')!!}--}}
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>--}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script type="text/javascript">
    var lis = $('.check_active');
    lis.each(function (index) {
        if ($(this).attr('href') === "{!!url('/')!!}" + window.location.pathname) {
            $(this).parent().addClass('active');
            $(this).parent().parent().parent().addClass('active');
        }
    });
</script>
<script type="text/javascript">
    $(window).load(function () {
        $('.select2').select2();
        $('input[type="date"]').bootstrapMaterialDatePicker({
            time: false,
            clearButton: true
        });
        if ($(window).width() < 481) {
            $('.dt-buttons').hide();
        }
        $('.select').selectpicker();
    });
</script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
{!! Html::script('/_dashboard/plugins/momentjs/moment.js') !!}
{!! Html::script('_dashboard/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}
@include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

@yield('footer')

@stack('scripts')
</body>

</html>
