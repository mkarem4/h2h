<div class="col-md-6">

    <div class="form-group ">
        <label class="form-label">الاسم </label>
        <div class="form-line">
            {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اسم القسم'])!!}
        </div>
    </div>
</div>

<div class="col-md-6">

    <div class="form-group ">
        <label class="form-label">الوصف </label>
        <div class="form-line">
            {!! Form::text("description",null,['class'=>'form-control','placeholder'=>'وصف القسم'])!!}
        </div>
    </div>
</div>
<x-file name="image" title="صورة القسم"></x-file>


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

