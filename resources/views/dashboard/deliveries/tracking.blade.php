@extends('dashboard.layout.app')

@section('title')
    مناديب التوصيل
@endsection
@section('content')
    <!-- Exportable Table -->

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        مناديب التوصيل
                    </h2>
                </div>
                <div class="body">
                    <div id="map" style="height: 480px;width: 100%"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    @include('family.layout.datatable-scripts')
    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.20/lodash.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.1.3/firebase.js"></script>
    <script>
        let markers = {};
        let map
        let infowindows = [];
        function initMap() {
            let myLatLng = {lat: 23.363, lng: 32.044};

            map = new google.maps.Map(document.getElementById('map'), {
                zoom: 4,
                center: myLatLng
            });

            // TODO: Replace the following with your app's Firebase project configuration
            // For Firebase JavaScript SDK v7.20.0 and later, `measurementId` is an optional field
            var firebaseConfig = {
                apiKey: "AIzaSyCyjt6kCtPcQRUQTek1EN6m0fGfdapmdJU",
                authDomain: "basma-4c711.firebaseapp.com",
                databaseURL: "https://basma-4c711.firebaseio.com",
                projectId: "basma-4c711",
                storageBucket: "basma-4c711.appspot.com",
                messagingSenderId: "845060088915",
                appId: "1:845060088915:web:0c1f5a990d51cafad5d2a6",
                measurementId: "G-8FG9RCP6FX"
            };
            firebase.initializeApp(firebaseConfig);
            // Get a reference to the database service
            let database = firebase.database();
            let orders = database.ref('/Tracking');
            orders.on('value', function (snapshot) {
                let data = snapshot.val();
                console.log('init')
                if (data){
                    let values = Object.values(data);
                    let orders = _.flatten(values.map(function (value, key) {
                        return Object.values(value)
                    }));
                    let orders_id=[];
                    for (let order of orders) {
                        if (order.driver && order.order.cartId && order.lat && order.lng) {
                            setMarkers(order);
                            orders_id.push(`${order.order.cartId}`);
                        }
                    }
                        let deleted_markers = _.differenceBy(Object.keys(markers), orders_id);
                        for (deleted_marker of deleted_markers) {
                            markers[deleted_marker].setMap(null);
                        }
                } else {
                    console.log('delete all')
                      for (mark of Object.keys(markers)) {
                           markers[mark].setMap(null);
                       }
                }
            });
        }


        function setMarkers(data) {


            let contentString =
                `
                 <div style="padding: 20px">
                    <h5 class="text-right" style="margin-right:8px;">طلب رقم ${data.order.cartId}</h5>
                        <div>
اسم السائق : ${data.driver.name}
</div>
<div>تيلفون السائق : ${data.driver.phone}</div>
<div>صورة السائق السائق : <a target="_blank" href="${data.driver.image}"> <img class="img-responsive img-thumbnail" style="width: 50px; height: 50px"  src="${data.driver.image}" alt=""></a></div>

                    <a style="margin-right:8px;" href="/dashboard/admin/orders/${data.order.cartId}" target="_blank">
                        اضغط هنا لعرض تفاصيل الطلب
                    </a>
                 </div>
                 `;



            let infowindow = new google.maps.InfoWindow({
                content: contentString
            });
            infowindows.push(infowindow)

            let image = {
                url: (typeof data.driver.image !== 'undefined') ? data.driver.image : 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                // size: new google.maps.Size(100, 100),
                scaledSize: new google.maps.Size(25, 25), // scaled size
                origin: new google.maps.Point(0, 0), // origin
                anchor: new google.maps.Point(0, 0) // anchor
            };

            if (typeof markers[data.order.cartId] === 'undefined') {
                let marker = new google.maps.Marker({
                    position: {lat: data.lat, lng: data.lng},
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image,
                    title: (data.driver.name) ? data.driver.name : '',
                    snippet: 'Snippet'
                });
                markers[data.order.cartId] = marker;
                marker.addListener('click', function () {
                    closeInfoWindows();
                    map.panTo( {lat: data.lat, lng: data.lng})
                    infowindow.open(map, marker);
                });
                var myoverlay = new google.maps.OverlayView();
                myoverlay.draw = function () {
                    this.getPanes().markerLayer.id = 'markerLayer';
                };
                myoverlay.setMap(map);
            } else {
                markers[data.order.cartId].setPosition(new google.maps.LatLng(data.lat, data.lng))
            }
        }
        function closeInfoWindows(){
            for(var i = 0; i<infowindows.length; i++){
                infowindows[i].close();
            }
        }
    </script>

    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKHwO4kHCnokqGtz8gsFRqUFOYC0cKOnA&callback=initMap"
            type="text/javascript"></script>

@endpush
