<x-text name="name" title="الاسم"></x-text>
<x-text name="phone" title="رقم الهاتف"></x-text>
<x-password/>
<x-file name="image" title="الصورة الشخصية"></x-file>
<x-text name="email" title="البريد الالكتروني" ></x-text>
<div class="form-group form-float">
    <label class="form-label">المدينة</label>
    {!! Form::select('city_id',cities(),null,['class'=>'form-control','placeholder'=>'اختار المدينة'])!!}
    <div class="form-line">
    </div>
</div>
<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
