@extends('dashboard.layout.app')

@section('title')
إضافة  مندوب تصويل
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card ">
      <div class="header">
        <h2>إضافة  مندوب تصويل</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.deliveries.index')}}">   <button class="btn btn-danger">كل مناديب التوصيل </button></a>
</ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'admin.deliveries.store' ,'class'=>'form', 'method' => 'Post','files'=>true]) !!}
            @include('dashboard.deliveries.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
