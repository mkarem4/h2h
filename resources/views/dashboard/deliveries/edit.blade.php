@extends('dashboard.layout.app')

@section('title')
تعديل مندوب التوصيل
{{ $user->name }}
@endsection
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card ">
      <div class="header">
        <h2>تعديل المندوب التوصيل       {{ $user->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.deliveries.index')}}">   <button class="btn btn-danger">كل مناديب التوصيل</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($user , ['route' => ['admin.deliveries.update' , $user->id] , 'method' => 'PATCH','files'=>true]) !!}
          @include('dashboard.deliveries.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
