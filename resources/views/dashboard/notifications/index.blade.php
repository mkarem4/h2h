@extends('dashboard.layout.app')

@section('title')
    الاشعارات العامة
@endsection
@section('header')

@endsection

@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>الاشعارات العامة</h2>
                </div>
                <div class="body">
                    {!!Form::open( ['route' => 'admin.notifications.store', 'method' => 'Post',]) !!}
                    <x-text name="title" title="العنوان" :float="false"/>
                    <x-text-area name="body" title="النص"/>
                    <x-select title="النوع" name="type" :float="false" :items="['users'=>'المستخدمين','deliveries'=>'مناديب التوصيل','families'=>'الاسر','all'=>'الكل']" />
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
