<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        #printMeBill {
            text-align: right;
        }

        .flex-bill {
            display: flex;
            justify-content: space-between;
        }

        .block.refuse {
            /*border: 1px solid #000;*/
            padding: 5px;
            min-height: 25px;
            margin-top: 7px;
            /*background-color: #f3f3f3;*/
            margin-bottom: 10px;
        }

        .content-text span {
            font-weight: bold;
        }


        .aligne-center {
            text-align: center;
        }

        table.table-print {
            width: 100%;
            direction: rtl;
            margin-top: 15px !important
        }

        table.table-print thead {
            background-color: #fbfbfb
        }

        table.table-print thead th {
            text-align: right;
            padding: 10px 3px;
        }

        table.table-print thead td {
            text-align: right;
            padding: 3px 3px;
        }

        .have-orders-table {
            margin-top: 20px
        }

        table.table-print {
            border: 1px solid #ddd;
            padding: 0
        }

        table.table-print thead {
            background-color: #fbfbfb;
        }

        td {
            padding: 3px
        }

        @media print {
            body {
                text-align: right !important;
                direction: rtl;
            }

            #printMeBill {
                text-align: right !important;
                direction: rtl;
            }

            .block.refuse {
                background-color: #f3f3f3;
            }
        }
    </style>
</head>

<body>


    <div id="printMeBill">
        <div class="div-data">
            <div class="col-sm-12 fatoura-tit" style="margin-top: 20px;">
                <h3>فاتورة الطلبات</h3>
            </div>
            <div class="flex-bill">
                <p>
                    <span>رقم الطلب</span> : <span>{{$cart->id}}</span>
                </p>
                <p>
                    <span>حالة الطلب</span> : <span>{{__($cart->status)}}</span>
                </p>
                @if($cart->deliveries()->exists())
                <p>
                    <span>اسم السائق</span> : <span>{{$cart->deliveries()->latest()->first()->name}}</span>
                </p>
                    @endif
            </div>
            @if($cart->status=='reject')
            <div class="content-text">
                <span class="block">سبب الرفض :- </span>
                <div class="block refuse">{{$cart->reject_reason}}</div>
            </div>
            @endif
            <div class="flex-bill">
                <p>
                    <span> اسم العميل</span> : <span>{{$cart->user->name}}</span>
                </p>
                <p>
                    <span> رقم تليفون العميل</span> : <span>{{$cart->user->phone}}</span>
                </p>
            </div>
            <div class="flex-bill">
                <p>
                    <span> اسم الاسرة</span> : <span> {{$cart->family->name}}</span>
                </p>
                <p>
                    <span> رقم تليفون الاسرة</span> : <span>{{$cart->family->phone}}</span>
                </p>
            </div>
            <div class="flex-bill">
                <p>
                    <span> العنوان</span> : <span> {{$cart->address->address}}
                    </span>
                </p>
            </div>
            <div class="flex-bill">

                <p>
                    <span> اجمالي المنتجات</span> : <span>{{$cart->total}}</span>
                </p>
                <p>
                    <span> الضريبة</span> : <span>{{$cart->tax}}</span>
                </p>
            </div>
            <div class="flex-bill">
                <p>
                    <span> تكلفة التوصيل</span> : <span> {{$cart->delivery_cost}} </span>
                </p>
                <p>
                    <span>الخصم</span> : <span>{{$cart->discount}}</span>
                </p>
                <p>
                    <span> الاجمالي النهائي</span> : <span>{{$cart->grand_total}}</span>
                </p>
            </div>
            <div class="content-text">
                <span class="block"> ملاحظات العميل :-</span>
                <div class="block refuse">{{$cart->notes}}</div>
            </div>
            <div class="have-orders-table">
                <div class="content-text">
                    <span class="block"> المنتجات :-</span>
                </div>
                <table class="table-print">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم المنتج</th>
                            <th>مقاس المنتج</th>
                            <th>اللون</th>
                            <th>الكمية</th>
                            <th>السعر</th>
                            <th>الخصم</th>
                            <th>الاجمالي</th>
                        </tr>
                    </thead>
                    @foreach($cart->items as $item)
                        <tr>
                            <td>{{++$loop->index}}</td>
                            <td>{{$item->product->name}}</td>
                            <td>{{$item->size->name}}</td>
                            <td>{{optional($item->color)->name}}</td>
                            <td>{{$item->quantity}}</td>
                            <td>{{$item->price}}</td>
                            <td>{{$item->discount}}</td>
                            <td>{{$item->total}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            window.print();
        });
    </script>
</body>

</html>
