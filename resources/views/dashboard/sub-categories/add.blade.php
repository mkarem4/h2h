@extends('dashboard.layout.app')

@section('title')
إضافة  قسم فرعي
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>إضافة  قسم فرعيي جديد</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.sub-categories.index')}}">   <button class="btn btn-danger">كل الاقسام الفرعية </button></a>
</ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'admin.sub-categories.store', 'method' => 'Post','files'=>true]) !!}
            @include('dashboard.sub-categories.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
