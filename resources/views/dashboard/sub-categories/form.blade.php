<div class="form-group form-float">
  <label class="form-label">الاسم </label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اسم القسم'])!!}
  </div>
</div>
<div class="form-group form-float ">
    <label class="form-label">الاسم </label>
    <div class="form-line">
        {!! Form::select("category_id",categories(),null,['class'=>'form-control','placeholder'=>'القسم الرئيسي'])!!}
    </div>
</div>

<x-file name="image" title="صورة القسم"></x-file>



<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

