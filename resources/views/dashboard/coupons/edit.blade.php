@extends('dashboard.layout.app')

@section('title')
تعديل الكوبون
{{ $coupon->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>تعديل الكوبون       {{ $coupon->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.coupons.index')}}">   <button class="btn btn-danger">كل الكوبونات</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($coupon , ['route' => ['admin.coupons.update' , $coupon->id] , 'files'=>true,'method' => 'PATCH']) !!}
        @include('dashboard.coupons.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
