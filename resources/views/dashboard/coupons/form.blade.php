<x-text name="name" title="اسم الكوبون"></x-text>
<x-text name="code" title="الكود"></x-text>
<div class="form-group form-float">
    <label class="form-label">تاريخ انتهاء الكوبون</label>
    {!! Form::date('expire_at',null,['class'=>'form-control'])!!}
    <div class="form-line">
    </div>
</div>
<div class="form-group form-float">
    <label class="form-label">حالة الكوبون</label>
    {!! Form::select('is_active',[1=>'مفعل' ,0=>'غير مفعل'],null,['class'=>'form-control','placeholder'=>'حالة الكوبون'])!!}
    <div class="form-line">
    </div>
</div>
<div class="form-group ">
    <label class="form-label">نسبة الخصم </label>
    {!! Form::number('discount',null,['class'=>'form-control','placeholder'=>'نسبة الخصم'])!!}
    <div class="form-line">
    </div>
</div>
<div class="form-group ">
    <label class="form-label">الاسر المشتركة </label>
    {!! Form::select('families[]',families(),null,['class'=>'form-control select2','multiple'])!!}
    <div class="form-line">
    </div>
</div>
<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
