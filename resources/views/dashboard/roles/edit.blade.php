@extends('dashboard.layout.app')

@section('title')
    تعديل منصب
    {{ $role->name }}
@endsection
@section('header')

@endsection

@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="card animated slideInUp">
                <div class="header">
                    <h2>تعديل المنصب {{ $role->name }}</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.roles.index')}}">
                            <button class="btn btn-danger">كل الصلاحيات والمناصب</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($role , ['route' => ['admin.roles.update' , $role->id] , 'method' => 'PUT']) !!}


                    <div class="form-group ">
                        <label class="form-label">اسم المنصب</label>
                        <div class="form-line">
                            {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
                        </div>
                    </div>

                    <div class="form-group ">
                        <label class="form-label">الصلاحيات</label>
                        <div class="demo-checkbox">
                            @foreach($permission as $value)
                                {{ Form::checkbox('permission[]', $value->name, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'filled-in chk-col-teal','id'=> $value->name )) }}
                                <label for="{{ $value->name }}">{{ $value->ar_name }}</label>
                            @endforeach
                        </div>
                    </div>

                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>


                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
