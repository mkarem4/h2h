@extends('dashboard.layout.app')

@section('title')
    كل  الصلاحيات والمناصب
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css"/>
@endsection
@section('content')

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل الصلاحيات والمناصب
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.roles.create')}}">
                            <button class="btn btn-success">إضافة منصب جديد</button>
                        </a>
                    </ul>
                </div>
                <div class="header">
                    {!!Form::open( ['route'=>'admin.roles.index','class'=>'form phone_validate', 'method' => 'get']) !!}
                    <div class="col-sm-3 col-xs-12">
                        <label class="form-label">بحث</label>
                        {!! Form::text("search",null,['class'=>'form-control','placeholder'=>'بحث','id'=>'example-date'])!!}
                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">بحث</button>
                    {!!Form::close() !!}
                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم المنصب</th>
{{--                            <th> الصلاحيات</th>--}}
{{--                            <th>الحالة</th>--}}
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($roles as $key=>$role)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$role->name}}</td>


                                <td>
{{--                                    @if(!$role->hasAllPermissions(\Spatie\Permission\Models\Permission::all()))--}}
                                        <a href="{{route('admin.roles.edit',$role->id)}}"
                                           class="btn btn-info btn-circle" data-original-title="تعديل">
                                            <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                        </a>
                                    {{--@endif--}}

                                    @if(!$role->hasAllPermissions(\Spatie\Permission\Models\Permission::all()))
                                            @if( !auth()->user()->hasRole($role))
                                        <a href="#" onclick="Delete({{$role->id}})" data-toggle="tooltip"
                                           data-original-title="حذف" class="btn btn-danger btn-circle">
                                            <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                        </a>
                                        {!!Form::open( ['route' => ['admin.roles.destroy',$role->id] ,'id'=>'delete-form'.$role->id, 'method' => 'Delete']) !!}
                                        {!!Form::close() !!}
                                            @else
                                                <a href="#" disabled="disabled" class="btn btn-danger btn-circle">
                                                    <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                                </a>
                                            @endif
                                    @else
                                        <a href="#" disabled="disabled" class="btn btn-danger btn-circle">
                                            <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                        </a>
                                    @endif

                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذا المنصب ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });

        }

        $(document).ready(function () {
            var table = $('.table').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'searching': true
            });
        });
    </script>

    <!-- Jquery DataTable Plugin Js -->
{{--    @include('dashboard.layout.datatable-scripts')--}}

@endpush
