@extends('dashboard.layout.app')

@section('title')
    عرض  الصلاحيات
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css"/>
@endsection
@section('content')

    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        عرض صلاحيات المنصب  {{$role->name}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.roles.index')}}">   <button class="btn btn-danger">كل الصلاحيات والمناصب </button></a>
                    </ul>
                </div>

                <div class="body">
                    <div class="row">
                        @if(count($role->permissions))
                            @foreach($role->permissions as $permission)
                                <div class="col-lg-3">
                                       * {{$permission->ar_name}}
                                </div>
                            @endforeach
                        @else
                            لا يوجد
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('.table').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'searching': true
            });
        });
    </script>

    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')

@endpush
