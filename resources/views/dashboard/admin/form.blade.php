<x-text name="name" title="الاسم"></x-text>
<x-text name="email" title="البريد الالكتروني"></x-text>
<x-password/>

    <select name="role_id" class="form-control " id="role_id" >
        <option selected disabled>اختر الصلاحية/المنصب</option>
        @foreach($roles as $role)
            <option value="{{$role->id}}">{{$role->name}}</option>
        @endforeach
    </select>

<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
