@extends('dashboard.layout.app')

@section('title')
كل اعضاء الادارة
@endsection
@section('header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
" />
@endsection
@section('content')
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card ">
            <div class="header">
                <h2>
                    كل اعضاء الادارة
                </h2>
                <ul class="header-dropdown m-r--5">
                    <a href="{{route('admin.admins.create')}}"> <button class="btn btn-success">إضافة مدير جديد</button></a>
                </ul>
            </div>
            <div class="body">
                <table class="table display nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>الإسم</th>
                            <th>البريد الإلكتروني</th>
                            <th>العمليات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>

                            <td>
                                <a href="{{route('admin.admins.edit',['admin'=>$user->id])}}" class="btn btn-info btn-circle" data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>

                                @if(auth()->check() and auth()->id()!=$user->id)
                                <a href="#" onclick="Delete({{$user->id}})" data-toggle="tooltip" data-original-title="حذف" class="btn btn-danger btn-circle">
                                    <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                </a>
                                {!!Form::open( ['route' => ['admin.admins.destroy',$user->id] ,'id'=>'delete-form'.$user->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                              @endif

                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@push('scripts')
<script>
    function Delete(id) {
        var item_id = id;
        // console.log(item_id);
        Swal.fire({
            title: "هل أنت متأكد ",
            text: "هل تريد حذف هذا العضو ؟",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'موافق !',
            cancelButtonText:'الغاء'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-form' + item_id).submit();
            }else{
                Swal.fire(
                    'تم الالغاء!',
                    'تم الغاء الحذف بنجاح !',
                    'success'
                )
            }
        });

    }

</script>
@include('dashboard.layout.datatable-scripts')
@endpush
