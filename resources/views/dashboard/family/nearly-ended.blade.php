@extends('dashboard.layout.app')

@section('title')
الاشتراكات التي قاربت علي الإنتهاء
@endsection
@section('header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
" />
@endsection
@section('content')
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card ">
            <div class="header">
                <h2>
                    الاشتراكات التي قاربت علي الإنتهاء
                </h2>
            </div>
            <div class="body">
                <table class="table display nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>الإسم</th>
                            <th>البريد الإلكتروني</th>
                            <th>الموبيل</th>
                            <th>الصورة</th>
                            <th>المدينة</th>
                            <th>التقييم</th>
                            <th>تاريخ التسجيل</th>
                            <th>الترتيب</th>
                            <th>العمليات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subscriptions as $key=>$subscription)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$subscription->user->name}}</td>
                            <td>{{$subscription->user->email}}</td>
                            <td>{{$subscription->user->phone}}</td>
                            <td><a href="{{$subscription->user->image}}" target="_blank"><img src="{{$subscription->user->image}}" class="img-responsive img-thumbnail"   style="width:75px; height: 75px" alt=""></a></td>
                            <td>{{@$subscription->user->city->name}}</td>
                            <td>{{$subscription->user->rates->avg('value')}}</td>
                            <td>{{$subscription->user->created_at}}</td>
                            <td>{{$subscription->user->order}}</td>

                            <td>
                                <a href="{{route('admin.families.show',$subscription->user)}}" class="btn btn-info btn-circle" data-original-title="عرض">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-eye"></i>
                                </a>
                                <a href="{{route('admin.families.edit',$subscription->user->id)}}" class="btn btn-info btn-circle" data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="Delete({{$subscription->user->id}})" data-toggle="tooltip" data-original-title="حذف" class="btn btn-danger btn-circle">
                                    <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                </a>
                                {!!Form::open( ['route' => ['admin.families.destroy',$subscription->user->id] ,'id'=>'delete-form'.$subscription->user->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@push('scripts')
<script>
    function Delete(id) {
        var item_id = id;
        // console.log(item_id);
        Swal.fire({
            title: "هل أنت متأكد ",
            text: "هل تريد حذف هذا العضو ؟",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'موافق !',
            cancelButtonText:'الغاء'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-form' + item_id).submit();
            }else{
                Swal.fire(
                    'تم الالغاء!',
                    'تم الغاء الحذف بنجاح !',
                    'success'
                )
            }
        });

    }

</script>
@include('dashboard.layout.datatable-scripts')
@endpush
