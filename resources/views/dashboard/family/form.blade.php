<div class="row">

    <x-text name="name" title="الاسم"></x-text>
    <x-text name="email" title="البريد الالكتروني"></x-text>
    <x-password/>
    <x-text name="phone" title="رقم الهاتف"></x-text>
    <div class="col-md-6">
        <x-file name="image" title="الصورة الشخصية"/>
    </div>
    <x-number name="order" title="الترتيب"></x-number>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="form-label">تاريخ انتهاء الترتيب</label>
            {!! Form::date('order_until',null,['class'=>'form-control','placeholder'=>'تاريخ انتهاء الترتيب'])!!}
            <div class="form-line">
            </div>
        </div>
    </div>
    <x-text name="address" title="العنوان"></x-text>


    <x-select title="المدينة" name="city_id" :items="cities()"></x-select>


    @if(!isset($user) or !$user->subscriptions()->exists())
        <x-plans :model="@$user"></x-plans>
    @endif

    {{--    <x-select title="نوع الطلب" name="order_type" :items="['عند الطلب','تسليم فوري']" :float="false"></x-select>--}}
    <x-select title="نوع الطلب" name="order_type"
              :items="['on_delivery' => 'عند الطلب' ,'immediate_delivery' => 'تسليم فوري']"
              :float="false"></x-select>

    <div class="form-group col-md-12 ">
        <label class="form-label">موقعها</label>
        {!! Form::text("lat",isset($user) && $user->lat ? null : 31.95080183791635,['id'=>'lat'])!!}
        {!! Form::text("lng",isset($user) && $user->lng ? null : 35.91058039003076,['id'=>'lng'])!!}
        <div id="map" style="height: 400px"></div>
    </div>
    <x-text-area name="bio" title="نبذة عن الاسرة"/>
    <x-select title="التفعيل" name="is_active" :items="['غير مفعل','مفعل']"></x-select>

</div>

<button class="btn btn-primary waves-effect" type="submit">حفظ</button>


@push('scripts')
    {!! Html::script('_dashboard/js/map.js') !!}
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAV3ZKirEWJIMW8T4SWoK9rXd7VThciUtw&callback=initMap&libraries=&v=weekly"></script>
@endpush
