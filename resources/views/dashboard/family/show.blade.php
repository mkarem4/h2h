@extends('dashboard.layout.app')

@section('title')
    اشتراك الاسرة {{$user->name}}
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card ">
                <div class="header">
                    <h2>
                        تفاصيل اشتراك الاسرة {{$user->name}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.families.edit',$user)}}">
                            <button class="btn btn-success">تعديل بيانات الاسرة</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    <table class="table display nowrap center">
                        <tbody>
                    @if($user->plan)

                            <tr>
                                <td>اسم الباقة</td>
                                <td>{{$user->plan->name}}</td>
                            </tr>
                            <tr>
                                <td>تفاصيل الباقة</td>
                                <td>{{$user->plan->description}}</td>
                            </tr>
                            <tr>
                                <td>سعر الباقة</td>
                                <td>{{$user->plan->price}}</td>
                            </tr>
                            <tr>
                                <td>عدد ايام الاشتراك</td>
                                <td>{{$user->plan->invoice_period}}</td>
                            </tr>
                            <tr>
                                <td>تاريخ بداية الاشتراك</td>
                                <td>{{$user->subscribe_from}}</td>
                            </tr>
                            <tr>
                                <td>تاريخ نهاية الاشتراك</td>
                                <td>{{$user->subscribe_until}}</td>
                            </tr>
                            @if($user->subscribe_until)
                            <tr>
                                <td>عدد الايام المتبقية </td>
                                <td>{{$user->subscribe_until->diffInDays(now())}}</td>
                            </tr>
@endif
                                <tr>
                                    <td>حالة الاشتراك</td>
                                    <td>{{now()->between($user->subscribe_from,$user->subscribe_until)?'ساري':'غير مفعل'}}</td>
                                </tr>

                            <tr>
                                <td>
                                    {!! Form::open(['route'=>['admin.subscription',['family'=>$user->id,'type'=>'renew']],'method'=>'post']) !!}
                                    <button class="btn btn-lg btn-info" type="submit">
                                        <i class="fa fa-repeat"></i>
                                    </button>
                                {!! Form::close() !!}
                                <td>

                                    {!! Form::open(['route'=>['admin.subscription',['family'=>$user->id,'type'=>'cancel']],'method'=>'post']) !!}
                                    <button class="btn btn-lg btn-danger" type="submit">
                                        <i class="fa fa-ban"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>

                    @endif
                        <tr>
                            <td>
                                تعديل الاشتراك
                            </td>
                            <td>

                                {!! Form::model($user,['route'=>['admin.update.subscription',['family'=>$user->id]],'method'=>'post']) !!}
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-label">الباقة</label>
                                        {!! Form::select('plan_id',$plans,null,['class'=>'form-control  select2 ','placeholder'=>'اختار الباقة'])!!}
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-label">تاريخ بداية الاشتراك الجديد</label>
                                        {!! Form::date('subscribe_from',null,['class'=>'form-control  date '])!!}
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn-lg btn-success" type="submit">
                                            حفظ
                                        </button>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

