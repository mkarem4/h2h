@extends('dashboard.layout.app')

@section('title')
تعديل الاسرة
{{ $user->name }}
@endsection
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card ">
      <div class="header">
        <h2>تعديل الاسرة       {{ $user->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.families.index')}}">   <button class="btn btn-danger">كل الاسر</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($user , ['route' => ['admin.families.update' , $user->id] , 'method' => 'PATCH','files'=>true]) !!}
          @include('dashboard.family.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection
