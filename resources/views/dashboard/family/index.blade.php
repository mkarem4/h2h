@extends('dashboard.layout.app')

@section('title')
كل الاسر
@endsection
@section('header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
" />
@endsection
@section('content')
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card ">
            <div class="header">
                <h2>
                    كل الاسر المنتجة
                </h2>
                <ul class="header-dropdown m-r--5">
                    <a href="{{route('admin.families.create')}}"> <button class="btn btn-success">إضافة اسرة جديدة</button></a>
                </ul>
            </div>
            <div class="body">
                <table class="table display nowrap">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>الإسم</th>
                            <th>البريد الإلكتروني</th>
                            <th>الموبيل</th>
                            <th>الصورة</th>
                            <th>المدينة</th>
                            <th>التقييم</th>
                            <th>تاريخ انتهاء الاشتراك</th>
                            <th>منشئ الاسرة</th>
                            <th>الترتيب</th>
                            <th>عدد الطلبات</th>
                            <th>اجمالي المبيعات</th>
                            <th>العمليات</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $key=>$user)
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->phone}}</td>
                            <td><a href="{{$user->image}}" target="_blank"><img src="{{$user->image}}" class="img-responsive img-thumbnail"   style="width:75px; height: 75px" alt=""></a></td>
                            <td>{{optional($user->city)->name}}</td>
                            <td>{{$user->rates_avg_value}}</td>
                            <td>{{$user->subscribe_until}}</td>
                            <td>{{$user->creator_name}}</td>
                            <td>{{$user->order}}</td>
                            <td>{{$user->carts_count}}</td>
                            <td>{{$user->carts_sum_grand_total}}</td>

                            <td>
                                <a href="{{route('admin.families.login',$user)}}" target="_blank" class="btn btn-info btn-circle" data-original-title="تسجيل دخول">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-key"></i>
                                </a>
                                <a href="{{route('admin.families.show',$user)}}" class="btn btn-info btn-circle" data-original-title="عرض">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-eye"></i>
                                </a>
                                <a href="{{route('admin.families.edit',$user->id)}}" class="btn btn-info btn-circle" data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="Delete({{$user->id}})" data-toggle="tooltip" data-original-title="حذف" class="btn btn-danger btn-circle">
                                    <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
                                </a>
                                {!!Form::open( ['route' => ['admin.families.destroy',$user->id] ,'id'=>'delete-form'.$user->id, 'method' => 'Delete']) !!}
                                {!!Form::close() !!}
                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@push('scripts')
<script>
    function Delete(id) {
        var item_id = id;
        // console.log(item_id);
        Swal.fire({
            title: "هل أنت متأكد ",
            text: "هل تريد حذف هذا العضو ؟",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'موافق !',
            cancelButtonText:'الغاء'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-form' + item_id).submit();
            }else{
                Swal.fire(
                    'تم الالغاء!',
                    'تم الغاء الحذف بنجاح !',
                    'success'
                )
            }
        });

    }

</script>
@include('dashboard.layout.datatable-scripts')
@endpush
