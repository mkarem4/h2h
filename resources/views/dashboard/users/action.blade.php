<a href="{{route('admin.users.edit',$id)}}" class="btn btn-info btn-circle" data-original-title="تعديل">
    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
</a>

<a href="#" onclick="Delete({{$id}})" data-toggle="tooltip" data-original-title="حذف" class="btn btn-danger btn-circle">
    <i style="padding-top: 5px;padding-left: 4px;" class="fa fa-trash-o"></i>
</a>
{!!Form::open( ['route' => ['admin.users.destroy',$id] ,'id'=>'delete-form'.$id, 'method' => 'Delete']) !!}
{!!Form::close() !!}
