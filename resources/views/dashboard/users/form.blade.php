<x-text name="name" title="الاسم"></x-text>
<x-text name="phone" title="رقم الهاتف"></x-text>
<x-password/>
<x-file name="image" title="الصورة الشخصية"></x-file>
<x-text name="email" title="البريد الالكتروني" :float="false"></x-text>
<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
