@extends('dashboard.layout.app')

@section('title')
تعديل العميل
{{ $user->name }}
@endsection
@section('content')
<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card ">
      <div class="header">
        <h2>تعديل العميل       {{ $user->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.users.index')}}">   <button class="btn btn-danger">كل العملاء</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($user , ['route' => ['admin.users.update' , $user->id] , 'method' => 'PATCH','files'=>true]) !!}
          @include('dashboard.users.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
