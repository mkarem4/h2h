@extends('dashboard.layout.app')

@section('title')
كل المستخدمين
@endsection
@section('header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
" />
@endsection
@section('content')
<!-- Exportable Table -->
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card ">
            <div class="header">
                <h2>
                    كل المستخدمين
                </h2>
                <ul class="header-dropdown m-r--5">
                    <a href="{{route('admin.users.create')}}"> <button class="btn btn-success">إضافة عميل جديد</button></a>
                </ul>
            </div>
            <div class="body">
                {!! $dataTable->table() !!}
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->

@endsection

@push('scripts')
    @include('components.yajra-datatable-scripts')
    <script>
    function Delete(id) {
        var item_id = id;
        // console.log(item_id);
        Swal.fire({
            title: "هل أنت متأكد ",
            text: "هل تريد حذف هذا العضو ؟",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'موافق !',
            cancelButtonText:'الغاء'
        }).then((result) => {
            if (result.value) {
                document.getElementById('delete-form' + item_id).submit();
            }else{
                Swal.fire(
                    'تم الالغاء!',
                    'تم الغاء الحذف بنجاح !',
                    'success'
                )
            }
        });

    }

</script>
@endpush
