<x-Text title='الاسم' name="name" ></x-Text>
<x-Text title='رقم الهاتف' name="phone" ></x-Text>
<x-Text title='الايميل' name="email" ></x-Text>
<div class="form-group form-float">
    <label class="form-label">الرقم السري</label>
    {!! Form::password('password',['class'=>'form-control','placeholder'=>'الرقم السري'])!!}
    <div class="form-line">
    </div>
</div>

<x-Number title='سعر مقطع الفديو' name="price" ></x-Number>
<x-Number title='سعر المقطع الصوتي' name="voice_price" ></x-Number>
<x-Number title="عدد الايام" name="response_time"></x-Number>
<x-Number title="الترتيب" name="order"></x-Number>
<x-Number title="رقم فودافون كاش" name="phone_cash"></x-Number>
<x-File title="الفديو التعريفي" name="bio_video"></x-File>
@if (isset($user) and $user->bio_video)
    <a href="{{$user->bio_video}}" target="_blank">الفديو التعريفي</a>
@endif

<x-Text-Area title="الوصف" name="bio"></x-Text-Area>
<x-Select title="القسم الرئيسي" name="category_id" :items='categories()'></x-Select>
<x-Text title="الجنسية" name="nationality"></x-Text>
<x-File title="الصورة الشخصية" name="image"></x-File>
@if (isset($user) and $user->image)
    <a href="{{$user->image}}" target="_blank"><img height="100" width="100" class="thumbnail" src="{{getImg($user->image)}}" alt=""></a>
@endif

<x-Select :items="is_active()" name="is_active" title="التفعيل"></x-Select>
<x-Select :items="is_active()" name="is_special" title="التمييز"></x-Select>
<x-Text title='اسم البنك' name="bank_name" ></x-Text>
<x-Text title='اسم صاحب الحساب' name="bank_owner_name" ></x-Text>
<x-Text title='رقم الحساب' name="bank_account_number" ></x-Text>
<x-Text title='رقم الiban' name="bank_iban" ></x-Text>
<x-Text title='عنوان البنك' name="bank_address" ></x-Text>


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
