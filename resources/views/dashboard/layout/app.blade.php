<!DOCTYPE html>
<html lang="{{  app()->getLocale() }}">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>لوحة التحكم - @yield('title')</title>
    <!-- Favicon-->
    <!-- <link rel="icon" href="favicon.ico" type="image/x-icon"> -->

    <!-- Google Fonts -->
    {{-- <link href="{{asset('assets/images/logo.png')}}" rel="shortcut icon">--}}
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Changa" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    {!!Html::style('_dashboard/icofont/icofont.min.css')!!}

    <!-- Multi Select Css -->
    {!!Html::style('_dashboard/plugins/multi-select/css/multi-select.css')!!}
    <!-- Bootstrap Core Css -->
    {!!Html::style('_dashboard/plugins/bootstrap/css/bootstrap.min.css')!!}
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-cSfiDrYfMj9eYCidq//oGXEkMc0vuTxHXizrMOFAaPsLt1zoCUVnSsURN+nef1lj" crossorigin="anonymous">
    <!-- Waves Effect Css -->
    {!!Html::style('_dashboard/plugins/node-waves/waves.min.css')!!}
    <!-- Animation Css -->
    {!!Html::style('_dashboard/plugins/animate-css/animate.min.css')!!}
    <!-- Custom Css -->
    {!!Html::style('_dashboard/css/style.min.css')!!}
    @yield('header')
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    {!!Html::style('_dashboard/css/themes/theme-red.min.css')!!}
    {!!Html::style('_dashboard/css/rtl.css')!!}
    {!!Html::style('_dashboard/css/materialize.css')!!}
    {{-- {!!Html::style('assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!}--}}
    {!!Html::style('_dashboard/plugins/bootstrap-select/css/bootstrap-select.css')!!}
    {{-- {!!Html::style('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')!!}--}}


    {!!Html::style('_dashboard/css/themes/all-themes.css')!!}
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />

    <!--- new style -->
    {!!Html::style('_dashboard/css/custom.css')!!}
    {!!Html::style('_dashboard/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css')!!}

    @stack('style')
    <style>
        .dropdown-menu>li>a {
            color: darkred !important;
            background-color: white !important;
        }

        .navbar {
            background-color: #7F1212 !important;
        }

        .sidebar .user-info {
            height: 82px;
        }

        .img100 i {
            color: #7F1212 !important;
            font-size: 2em;
        }

        .img200 i {
            color: #7F1212 !important;
            font-size: 13em;
        }

        .image-preview {
            width: 75px;
            height: 75px;
        }
    </style>

</head>

<body>
    <p class="theme-red"></p>
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>رجاءً انتظر...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->

    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="/">الرئيسية</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <div class="nav-logo">
                    <a href="{{route('admin.index')}}"><img src="{{asset('_dashboard/images/h2h_logo.jpg')}}"></a>
                </div>
                <!--  <div class="skwed"><h3> من البيت للبيت</h3></div> -->
                <ul class="nav navbar-nav navbar-left">

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <div class="user-info">
                <div class="info-container">
                    <div class="name ar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"></div>
                    <div class="email ar"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="color:#e99fae;     margin-bottom: 14px; font-size: 33px;">list</i>
                        <ul class="dropdown-menu pull-left">




                            <form id="logout-form" action="{{route('logout')}}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            <li><a class="" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();" href="{{ route('logout') }}">
                                    <i class="material-icons">input</i>تسجيل الخروج</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">القائمة الرئيسية</li>
                    @include('dashboard.layout.nav')
                </ul>
            </div>
            <!-- #Menu -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            @include('dashboard.layout.validation')

            @yield('content')
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.min.js" integrity="sha512-DUC8yqWf7ez3JD1jszxCWSVB0DMP78eOyBpMa5aJki1bIRARykviOuImIczkxlj1KhVSyS16w2FSQetkD4UU2w==" crossorigin="anonymous"></script>
    <!-- Bootstrap Core Js -->
    {!!Html::script('_dashboard/plugins/bootstrap/js/bootstrap.min.js')!!}
    <!-- Select Plugin Js -->
    {!!Html::script('_dashboard/plugins/bootstrap-select/js/bootstrap-select.min.js')!!}
    <!-- Slimscroll Plugin Js -->
    {!!Html::script('_dashboard/plugins/jquery-slimscroll/jquery.slimscroll.min.js')!!}
    <!-- Waves Effect Plugin Js -->
    {!!Html::script('_dashboard/plugins/node-waves/waves.min.js')!!}
    <!-- Custom Js -->
    {!!Html::script('_dashboard/js/admin.js')!!}

    {!!Html::script('_dashboard/js/pages/ui/modals.js')!!}
    {!!Html::script('_dashboard/js/demo.js')!!}
    {{--{!!Html::script('assets/js/pages/forms/basic-form-elements.js')!!}--}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>--}}

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

    <script type="text/javascript">
        var lis = $('.check_active');
        lis.each(function(index) {
            if ($(this).attr('href') === "{!!url('/')!!}" + window.location.pathname) {
                $(this).parent().addClass('active');
                $(this).parent().parent().parent().addClass('active');
            }
        });
    </script>
    <script type="text/javascript">
        $(window).load(function() {
            $('.select2').select2();
            $('input[type="date"]').bootstrapMaterialDatePicker({
                time: false,
                clearButton: true
            });
            if ($(window).width() < 481) {
                $('.dt-buttons').hide();
            }
            $('.select').selectpicker();
        });
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {!! Html::script('/_dashboard/plugins/momentjs/moment.js') !!}
    {!! Html::script('_dashboard/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js') !!}
    @include('sweetalert::alert', ['cdn' => "https://cdn.jsdelivr.net/npm/sweetalert2@9"])

    @yield('footer')

    @stack('scripts')
</body>

</html>
