<li>
    <a class="check_active" href="{{route('admin.index')}}">
        <i class="material-icons">home</i>
        <span>الصفحه الرئيسيه </span>
    </a>
</li>


{{--@can('admins')--}}
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الإدارة </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.admins.index')}}">
                <span> اعضاء الإدارة </span>
            </a>
        </li>
        @can('add admin')
        <li>
            <a class="check_active" href="{{route('admin.admins.create')}}">
                <span>اضافه مدير جديد</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
{{--@endcan--}}
@can('add admin')
    <li>
        <a href="javascript:void(0);" class="menu-toggle">
            <i class="material-icons">person</i>
            <span>الباقات و الاشراكات </span>
        </a>
        <ul class="ml-menu">
            <li>
                <a class="check_active" href="{{route('admin.plans.index')}}">
                    <span> الباقات </span>
                </a>
            </li>
            <li>
                <a class="check_active" href="{{route('admin.nearlyEnded')}}">
                    <span> قارب علي الانتهاء </span>
                </a>
            </li>
        </ul>
    </li>
@endcan
@can('users')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>العملاء </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.users.index')}}">
                <span> العملاء </span>
            </a>
        </li>
        @can('add user')
        <li>
            <a class="check_active" href="{{route('admin.users.create')}}">
                <span>اضافه عميل جديد</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('families')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الاسر المنتجة </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.families.index')}}">
                <span> كل الاسر </span>
            </a>
        </li>
{{--        @can('add family')--}}
        <li>
            <a class="check_active" href="{{route('admin.families.create')}}">
                <span>اضافه اسرة جديدة</span>
            </a>
        </li>
{{--        @endcan--}}
    </ul>
</li>
@endcan
@can('cities')

<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>المدن </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.cities.index')}}">
                <span> كل المدن </span>
            </a>
        </li>
        @can('add city')
        <li>
            <a class="check_active" href="{{route('admin.cities.create')}}">
                <span>اضافه مدينة جديدة</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('categories')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الاقسام الرئيسية </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.categories.index')}}">
                <span> كل الاقسام الرئيسية </span>
            </a>
        </li>
        @can('add category')
        <li>
            <a class="check_active" href="{{route('admin.categories.create')}}">
                <span>اضافه قسم رئيسي جديد</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('sub-categories')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الاقسام الفرعية </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.sub-categories.index')}}">
                <span> كل الاقسام الفرعية </span>
            </a>
        </li>
        @can('add subcategory')
        <li>
            <a class="check_active" href="{{route('admin.sub-categories.create')}}">
                <span>اضافه قسم فرعي جديد</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('products')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>المنتجات </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.products.index')}}">
                <span> كل المنتجات </span>
            </a>
        </li>
        @can('add product')
        <li>
            <a class="check_active" href="{{route('admin.products.create')}}">
                <span>اضافه منتج</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('deliveries')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>مناديب التوصيل </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.deliveries.index')}}">
                <span> كل مناديب التوصيل </span>
            </a>
        </li>
        <li>
            <a class="check_active" href="{{route('admin.deliveries.tracking')}}">
                <span> التتبع </span>
            </a>
        </li>
        @can('add delivery')
        <li>
            <a class="check_active" href="{{route('admin.deliveries.create')}}">
                <span>اضافه مندوب التوصيل</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('banners')

<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>البنرات </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.banners.index')}}">
                <span> كل البنرات </span>
            </a>
        </li>
        <li>
            <a class="check_active" href="{{route('admin.banners.create')}}">
                <span>اضافه بنر جديد</span>
            </a>
        </li>
    </ul>
</li>
@endcan
@can('coupons')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الكوبونات </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.coupons.index')}}">
                <span> كل الكوبونات </span>
            </a>
        </li>
        @can('add coupons')
        <li>
            <a class="check_active" href="{{route('admin.coupons.create')}}">
                <span>اضافه كوبون جديد</span>
            </a>
        </li>
        @endcan
    </ul>
</li>
@endcan
@can('rates')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>التقييمات </span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.rate.product')}}">
                <span> تقييمات المنتجات </span>
            </a>
        </li>
        <li>
            <a class="check_active" href="{{route('admin.rate.family')}}">
                <span>تقييمات الاسر</span>
            </a>
        </li>
    </ul>
</li>
@endcan
@can('orders')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>الطلبات</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.orders.index')}}">
                <span> الطلبات </span>
            </a>
        </li>
    </ul>
</li>
@endcan
@can('contacts')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>رسائل التواصل</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.contacts.index')}}">
                <span> رسائل التواصل </span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span> الاشعارات العامة</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.notifications.index')}}">
                <span> الاشعارات العامة </span>
            </a>
        </li>
    </ul>
</li>
@endcan
@can('add admin')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span>  الاعدادات</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.settings.index')}}">
                <span> الاعدادات </span>
            </a>
        </li>
    </ul>
</li>
@endcan
@can('roles')
<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">person</i>
        <span> الصلاحيات والمهام</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.roles.index')}}">
                <span> الصلاحيات </span>
            </a>
        </li>
    </ul>
</li>
@endcan
