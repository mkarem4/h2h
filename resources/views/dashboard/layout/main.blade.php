@extends('dashboard.layout.app')

@section('title')
    الرئيسية
@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>@endpush
@section('content')
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="body">
                    {!!Form::open( ['route' => 'admin.index' ,'class'=>'form', 'method' => 'get']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="form-label">من</label>
                                {!! Form::date('from',null,['class'=>'form-control date'])!!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group ">
                                <label class="form-label">الي</label>
                                {!! Form::date('to',null,['class'=>'form-control date'])!!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>

                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>

                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">عدد الاسر</div>
                <div class="number count-to" data-from="0" data-to="{{$new_families}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">اجمالي عدد الاسر</div>
                <div class="number count-to" data-from="0" data-to="{{$families}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">عدد الطلبات</div>
                <div class="number count-to" data-from="0" data-to="{{$carts}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">عدد الطلبات المنتهية</div>
                <div class="number count-to" data-from="0" data-to="{{$carts_finished}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">عدد الطلبات المعلقة</div>
                <div class="number count-to" data-from="0" data-to="{{$carts_pending}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">اجمالي فواتير الطلبات</div>
                <div class="number count-to" data-from="0" data-to="{{$carts_total}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box bg-pink hover-expand-effect">
            <div class="icon">
                <i class="material-icons">account_box</i>
            </div>
            <div class="content">
                <div class="text">اجمالي فواتير طلبات التوصيل</div>
                <div class="number count-to" data-from="0" data-to="{{$carts_delivery}}" data-speed="3"
                     data-fresh-interval="20"></div>
            </div>
        </div>
    </div>

    <div class="row clearfix">

        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        الطلبات المعلقة
                    </h2>
                </div>
                <div class="body">
                    <table class="table display nowrap">
                        <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>اسم العميل</th>
                            <th>رقم جوال العميل</th>
                            <th>اسم الاسرة</th>
                            <th>الحالة</th>
                            <th>اجمالي الطلب</th>
                            <th>تاريخ الانشاء</th>
                            <th>تاريخ الطلب</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pending_carts as $key=>$cart)
                            <tr>
                                <td>{{$cart->id}}</td>
                                <td>{{$cart->user->name}}</td>
                                <td>{{$cart->user->phone}}</td>
                                <td>{{$cart->family->name}}</td>
                                <td>{{__($cart->status)}}</td>
                                <td>{{$cart->grand_total}}</td>
                                <td>{{$cart->created_at->toDateString()}}</td>
                                <td>{{$cart->date}}</td>
                                <td><a href="{{route('admin.orders.show',$cart)}}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        الطلبات السارية
                    </h2>
                </div>
                <div class="body">
                    <table class="table display nowrap">
                        <thead>
                        <tr>
                            <th>رقم الطلب</th>
                            <th>اسم العميل</th>
                            <th>رقم جوال العميل</th>
                            <th>اسم الاسرة</th>
                            <th>الحالة</th>
                            <th>اجمالي الطلب</th>
                            <th>تاريخ الانشاء</th>
                            <th>تاريخ الطلب</th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($going_carts as $key=>$cart)
                            <tr>
                                <td>{{$cart->id}}</td>
                                <td>{{$cart->user->name}}</td>
                                <td>{{$cart->user->phone}}</td>
                                <td>{{$cart->family->name}}</td>
                                <td>{{__($cart->status)}}</td>
                                <td>{{$cart->grand_total}}</td>
                                <td>{{$cart->created_at->toDateString()}}</td>
                                <td>{{$cart->date}}</td>
                                <td><a href="{{route('admin.orders.show',$cart)}}"><i class="fa fa-eye"></i></a></td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        الطلبات علي مدار الشهر
                    </h2>
                </div>
                <div class="body">
                    <div id="carts"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card animated slideInUp">
            <div class="body">
                <div id="map" style="height:500px"></div>
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    @include('dashboard.layout.datatable-scripts')
    <script src="{{asset('_dashboard/plugins/raphael/raphael.min.js')}}"></script>
    <script src="{{asset('morris.js/dist/morris.min.js')}}"></script>
    {!! Html::script('_dashboard/plugins/jquery-countto/jquery.countTo.js') !!}
    <script async
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKHwO4kHCnokqGtz8gsFRqUFOYC0cKOnA&libraries=visualization&callback=initMap">
    </script>

    <script>
        $(document).ready(function () {
            $('.count-to').countTo();
            Morris.Line({
                element: 'carts',
                data:@json($carts_in_days),
                xkey: 'created_at_date',
                ykeys: ['carts'],
                labels: ['العدد'],
                parseTime: false,
                resize: true,
                xLabelAngle:90

            });
        });
        function initMap() {
            var heatmapData =@json($carts_locations);
            var latLngClass=[];
            for (const heatmapDataKey in heatmapData) {
                latLngClass.push(new google.maps.LatLng(heatmapData[heatmapDataKey].lat,heatmapData[heatmapDataKey].lng))
            }
            var geddah = new google.maps.LatLng(21.4505289,39.4913353);

            map = new google.maps.Map(document.getElementById('map'), {
                center: geddah,
                zoom: 10,
                // mapTypeId: 'satellite'
            });
            var heatmap = new google.maps.visualization.HeatmapLayer({
                data: latLngClass
            });
            heatmap.setMap(map);
        }


    </script>


@endpush
