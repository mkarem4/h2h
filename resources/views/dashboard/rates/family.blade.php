@extends('dashboard.layout.app')

@section('title')
    تقييم  الاسر
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    @include('validation')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        تقييم الاسر
                    </h2>
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>اسم العميل </th>
                            <th>اسم الاسرة </th>
                            <th>التقييم </th>
                            <th>تاريخ التقييم </th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rates as $key=>$rate)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$rate->user->name}}</td>
                                <td>{{$rate->rateable->family->name}}</td>
                                <td>{{$rate->value}}</td>
                                <td>{{$rate->created_at}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')


@endsection
