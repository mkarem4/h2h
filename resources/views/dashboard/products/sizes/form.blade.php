<div class="row">


    <x-text name="name" title="اسم المقاس"></x-text>
    <x-number name="price" title="سعر المقاس"></x-number>
    <x-number name="discount" title="نسبة الخصم"></x-number>
</div>
@isset($product)
    {!! Form::hidden('product_id',$product->id) !!}
@endisset

<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

