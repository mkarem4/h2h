@extends('dashboard.layout.app')

@section('title')
    إضافة  لون جديد للمنتج
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>إضافة  لون جديد للمنتج</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.product-colors.show',$product)}}">   <button class="btn btn-danger">كل الوان المنتج </button></a>
</ul>
      </div>
      <div class="body">
          {!!Form::open( ['route' => 'admin.product-colors.store', 'method' => 'Post','files'=>true]) !!}
            @include('dashboard.products.colors.form')
          {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>
<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
