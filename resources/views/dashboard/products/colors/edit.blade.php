@extends('dashboard.layout.app')

@section('title')
تعديل لون المنتج
{{ $color->product->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>تعديل لون المنتج       {{ $color->product->name }}</h2>
      </div>
      <div class="body">
        {!!Form::model($color , ['route' => ['admin.product-colors.update' , $color->id] ,'method' => 'PATCH']) !!}
        @include('dashboard.products.colors.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
