<a href="{{route('admin.products.edit',$product->id)}}"
   class="btn btn-info btn-circle "
   data-original-title="تعديل">
    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
</a>
<a href="#" onclick="Delete('{{$product->id}}')" class="btn btn-info btn-circle "
   data-original-title="حذف">
    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
</a>
{!! Form::open(['route'=>['admin.products.destroy',$product->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$product->id]) !!}
{!! Form::close() !!}
