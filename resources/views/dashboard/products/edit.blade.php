@extends('dashboard.layout.app')

@section('title')
    تعديل المنتج
    {{ $product->name }}
@endsection
@section('header')

@endsection

@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>تعديل المنتج {{ $product->name }}</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.products.index')}}">
                            <button class="btn btn-danger">كل المنتجات</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($product , ['route' => ['admin.products.update' , $product->id] , 'files'=>true,'method' => 'PATCH']) !!}
                    @include('dashboard.products.form')
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>


    <!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
