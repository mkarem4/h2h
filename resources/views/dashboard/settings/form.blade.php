@extends('dashboard.layout.app')
@section('title')
    {{$settings_page}}
@endsection

@section('content')
    @include('validation')
    <div class="body">
    <!-- Basic Validation -->
     <div class="row clearfix">

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            {!!Form::open( ['route' => 'admin.settings.store' , 'method' => 'Post','files'=>true]) !!}
            <div class="card">
                <div class="header">
                    <h2> <span>التحكم ب </span> {{$settings_page}}</h2>
                    <ul class="header-dropdown m-r--5">
                    </ul>
                </div>
                <div class="body">
                    @foreach($settings as $setting)
                         @if($setting->type == 'text')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}}</label>
                                <div class="form-line">
                                    {!! Form::text($setting->name.'[]',$setting->ar_value,['class'=>'form-control'])!!}
                                </div>
                            </div>

                         @elseif($setting->type=='file')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}}</label>
                                <div class="form-line">
                                    <a href="{{getImg($setting->ar_value)}}" target="_blank"><img class="image-preview" src="{{getImg($setting->ar_value)}}" alt=""></a>
                                    {!! Form::file($setting->name.'[]',['class'=>'form-control'])!!}
                                </div>
                            </div>
                        @elseif($setting->type == 'email')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}}</label>
                                <div class="form-line">
                                    {!! Form::email($setting->name.'[]',$setting->ar_value,['class'=>'form-control'])!!}
                                </div>
                            </div>
                        @elseif($setting->type == 'number')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}} </label>
                                <div class="form-line">
                                    {!! Form::number($setting->name.'[]',$setting->ar_value,['class'=>'form-control','step'=>'.01'])!!}
                                </div>
                            </div>
                        @elseif($setting->type == 'radio')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}} </label>
                                {!! Form::radio($setting->name.'[]','static',null,['class'=>'form-control','id'=>'static','checked'=>($setting->ar_value=='static')?true:false])!!}
                                <label for="static" class="form-label">{{__('static')}}</label>
                                {!! Form::radio($setting->name.'[]','dynamic',null,['class'=>'form-control','id'=>'dynamic','checked'=>($setting->ar_value=='dynamic')?true:false])!!}
                                <label for="dynamic" class="form-label">{{__('dynamic')}}</label>
                                <div class="form-line"></div>
                            </div>
                        @elseif ($setting->type=='long_text')
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}}<span>{{__('by arabic')}}</span></label>
                                <div class="form-line">
                                    {!! Form::textarea($setting->name.'[]',$setting->ar_value,['class'=>'ck img-lg img-rounded'])!!}
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="form-label">{{$setting->title}} <span>{{__('by english')}}</span> </label>
                                <div class="form-line">
                                    {!! Form::textarea($setting->name.'[]',$setting->en_value,['class'=>'ck img-lg img-rounded'])!!}
                                </div>
                            </div>
                        @elseif ($setting->type=='map')
                            {!! Form::hidden($setting->name.'[]',$setting->ar_value,['class'=>'form-control ','id'=>'la','placeholder'=>__("dashboard.lat"),'required'=>'true'])!!}
                            {!! Form::hidden($setting->name.'[]',$setting->en_value,['class'=>'form-control ','id'=>'lo','placeholder'=>__("dashboard.lng"),'required'=>'true'])!!}

                            @include('admin.setting.map')
                        @endif

                    @endforeach

                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>

                </div>
                {!!Form::close() !!}
            </div>
        </div>
    </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('admin/plugins/ckeditor/ckeditor.js') !!}
    <script>
        $(document).ready(function () {
            CKEDITOR.replaceClass = 'ck';
        });
    </script>

@endsection
