<div class="form-group ">
  <label class="form-label">الاسم </label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اسم المدينة'])!!}
  </div>
</div>
<div class="form-group ">
    <label class="form-label">تكلفة التوصيل </label>
    <div class="form-line">
        {!! Form::number("delivery_cost",null,['class'=>'form-control','placeholder'=>'تكلفة التوصيل'])!!}
    </div>
</div>


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

