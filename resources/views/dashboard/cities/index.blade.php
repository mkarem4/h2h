
@extends('dashboard.layout.app')

@section('title')
    كل  المدن
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    @include('validation')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل المدن
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.cities.create')}}">
                            <button class="btn btn-success">إضافة مدينة جديدة</button>
                        </a>
                    </ul>
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الاسم </th>
                            <th>تكلفة التوصيل </th>
                            <th>المدن المدعومة </th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($items as $key=>$city)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$city->name}}</td>
                                <td>{{$city->delivery_cost}}</td>
                                <td>
                                    <a href="{{route('admin.supported-cities.show',$city->id)}}" class="btn btn-warning btn-circle"><i style="padding-top:5px;padding-left: 6px;" class="fa fa-support"></i></a>
                                </td>
                                <td>
                                <a href="{{route('admin.cities.edit',$city->id)}}"
                                   class="btn btn-info btn-circle "
                                   data-original-title="تعديل">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-pencil"></i>
                                </a>
                                <a href="#" onclick="Delete('{{$city->id}}')" class="btn btn-info btn-circle "
                                   data-original-title="حذف">
                                    <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
                                </a>
                                {!! Form::open(['route'=>['admin.cities.destroy',$city->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$city->id]) !!}
                                {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')

    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذة المدينة ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });
        }
    </script>


@endsection
