
@extends('dashboard.layout.app')

@section('title')
    المدن المدعومة لمدينة {{$city->name}}
@endsection
@section('header')
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/v/bs/dt-1.10.18/b-1.5.6/r-2.2.2/datatables.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>إضافة  مدينة مدعومة</h2>
                </div>
                <div class="body">
                    {!!Form::open( ['route' => ['admin.supported-cities.update',$city->id], 'method' => 'PUT']) !!}
                    <div class="form-group ">
                        <label class="form-label">المدينة </label>
                        <div class="form-line">
                            {!! Form::select("supported[$city->id][supported_city_id]",\App\Models\City::toSelect($city),null,['class'=>'form-control','placeholder'=>'اسم المدينة'])!!}
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="form-label">تكلفة التوصيل </label>
                        <div class="form-line">
                            {!! Form::number("supported[$city->id][delivery_cost]",null,['class'=>'form-control','placeholder'=>'تكلفة التوصيل'])!!}
                        </div>
                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        المدن المدعومة لمدينة {{$city->name}}
                    </h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.cities.index')}}">
                            <button class="btn btn-success">كل المدن</button>
                        </a>
                    </ul>
                </div>
                <div class="header">

                </div>
                <div class="body">
                    <table
                        class="table table-bordered table-striped table-hover dataTable js-exportable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الاسم </th>
                            <th>تكلفة التوصيل </th>
                            <th>العمليات</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($city->supportedCities as $key=>$support_city)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$support_city->name}}</td>
                                <td>{{$support_city->pivot->delivery_cost}}</td>
                                <td>
                                    <a href="#" onclick="Delete('{{$support_city->id}}')" class="btn btn-info btn-circle "
                                       data-original-title="حذف">
                                        <i style="padding-top:5px;padding-left: 6px;" class="fa fa-trash"></i>
                                    </a>
                                    {!! Form::open(['route'=>['admin.supported-cities.destroy',$city->id],'method'=>'Delete','class'=>'hidden','id'=>'delete-form'.$support_city->id]) !!}
                                    {!! Form::hidden('supported_city_id',$support_city->id) !!}
                                    {!! Form::close() !!}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@section('footer')


    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')

    <script>
        function Delete(id) {
            var item_id = id;
            // console.log(item_id);
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد حذف هذة المدينة ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.getElementById('delete-form' + item_id).submit();
                } else {
                    Swal.fire(
                        'تم الالغاء!',
                        'تم الغاء الحذف بنجاح !',
                        'success'
                    )
                }
            });
        }
    </script>


@endsection
