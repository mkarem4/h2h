<div class="form-group  form-float">
  <label class="form-label">الاسم </label>
  <div class="form-line">
    {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اسم القسم'])!!}
  </div>
</div>
<div class="form-group  form-float">
    <label class="form-label">الرابط </label>
    <div class="form-line">
        {!! Form::text("url",null,['class'=>'form-control','placeholder'=>'الرابط'])!!}
    </div>
</div>
<div class="col-md-6">

<div class="form-group ">
    <label class="form-label">المدينة </label>
    <div class="form-line">
        {!! Form::select("city_id",\App\Models\City::pluck('name','id'),null,['class'=>'form-control select2','placeholder'=>'المدينة'])!!}
    </div>
</div>
</div>
<div class="col-md-6">

<div class="form-group ">
    <label class="form-label">المنتج </label>
    <div class="form-line">
        {!! Form::select("product_id",\App\Models\Product::toSelect(),null,['class'=>'form-control select2','placeholder'=>'المنتج'])!!}
    </div>
</div>
</div>
<x-file name="image" title="صورة البنر"></x-file>


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

