@extends('dashboard.layout.app')

@section('title')
تعديل البنر
{{ $banner->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>تعديل البنر       {{ $banner->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.banners.index')}}">   <button class="btn btn-danger">كل البنرات</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($banner , ['route' => ['admin.banners.update' , $banner->id] , 'files'=>true,'method' => 'PATCH']) !!}
        @include('dashboard.banners.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
