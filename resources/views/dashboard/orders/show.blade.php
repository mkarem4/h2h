@extends('dashboard.layout.app')

@section('title')
عرض الطلب رقم {{$cart->id }}
@endsection
@section('header')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
" />
@endsection
@section('content')
<!-- Exportable Table -->


    <div class="row clearfix">
        <div  class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if(in_array($cart->status,['wait_for_delivery','on_deliver']))
                <div class="card animated slideInUp">
                    <div class="header">
                        اعادة تعيين مندوب
                    </div>
                    <div class="body clearfix">
                        {!! Form::open(['route'=>['admin.delivery.assign',$cart->id],'method'=>'PUT']) !!}
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-label">المندوب</label>
                                    {!! Form::select('delivery_id', deliveries() , request('delivery_id') , ['class' => 'form-control select2 ','placeholder'=>'اختار المندوب']) !!}
                                    <div class="form-line">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                            <div class="form-group">
                                <label class="form-label"></label>
                                {!! Form::submit('تعيين',['class'=>'btn btn-success']) !!}
                            </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    @endif
                </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if (in_array($cart->status,['pending','accepted']))
            <div class="card animated slideInUp">
                <div class="header">
                    العمليات
                </div>
                <div class="body clearfix">
                    @if($cart->status=='pending')
                    <div class="col-xs-6"><a href="{{route('admin.order.change.status',['order'=>$cart,'status'=>'accepted'])}}" id="accept" class="btn btn-success btn-lg form-control">قبول</a></div>
                        <div class="col-xs-6"><a href="{{route('admin.order.change.status',['order'=>$cart,'status'=>'rejected'])}}" id="reject" class="btn btn-danger btn-lg form-control">رفض</a></div>
                    @endif
                    @if ($cart->status=='accepted')
                    <div class="col-xs-12"><a href="{{route('admin.order.change.status',['order'=>$cart,'status'=>'wait_for_delivery'])}}" id="ready" class="btn btn-primary btn-lg form-control">تم التجهيز</a></div>
                        @endif
                </div>
            </div>
            @endif
            @if(!in_array($cart->status,['force_finish','force_cancel','finished','canceled','rejected']))
                    <div class="card animated slideInUp">
                        <div class="header">
                            عمليات الادارة
                        </div>
                        <div class="body clearfix">

                                <div class="col-xs-6"><a href="{{route('admin.order.change.status',['order'=>$cart,'status'=>'force_cancel'])}}" id="accept" class="btn btn-success btn-lg form-control">الغاء اجباري</a></div>
                                <div class="col-xs-6"><a href="{{route('admin.order.change.status',['order'=>$cart,'status'=>'force_finish'])}}" id="reject" class="btn btn-danger btn-lg form-control">انهاء اجباري</a></div>
                        </div>
                    </div>
                @endif

            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        تفاصيل الطلب
                    </h2>
                    <br>

                <table class="table table-hover table-striped responsive-table table-bordered">
                    <tr class="bold">
                        <td><b>البيان</b></td>
                        <td><b>القيمة</b></td>
                    </tr>
                    <tr>
                        <td>رقم الطلب</td>
                        <td>{{$cart->id}}</td>
                    </tr>
                    <tr>
                        <td>حالة الطلب</td>
                        <td>{{__($cart->status)}}</td>
                    </tr>
                    <tr>
                        <td>سبب الرفض ان وجد</td>
                        <td>{{$cart->reject_reason}}</td>
                    </tr>
                    <tr>
                        <td>اسم العميل</td>
                        <td>{{$cart->user->name}}</td>
                    </tr>
                    <tr>
                        <td>رقم تيلفون العميل</td>
                        <td>{{$cart->user->phone}}</td>
                    </tr>
                    <tr>
                        <td>اسم الاسرة</td>
                        <td>{{$cart->family->name}}</td>
                    </tr>
                    <tr>
                        <td>رقم تيلفون الاسرة</td>
                        <td>{{$cart->family->phone}}</td>
                    </tr>
                    @if($cart->requests->sortByDesc('created_at')->first())
                    <tr>
                        <td>اسم المندوب</td>
                        <td>{{$cart->requests->sortByDesc('created_at')->first()->delivery->name}}</td>
                    </tr>
                    <tr>
                        <td>رقم تيلفون المندوب</td>
                        <td>{{$cart->requests->sortByDesc('created_at')->first()->delivery->name}}</td>
                    </tr>
                    @endif
                    <tr>
                        <td>العنوان</td>
                        <td>{{$cart->address->address}}</td>
                    </tr>
                    <tr>
                        <td>اجمالي المنتجات</td>
                        <td>{{$cart->total}}</td>
                    </tr>
                    <tr>
                        <td>الضريبة</td>
                        <td>{{$cart->tax}}</td>
                    </tr>
                    <tr>
                        <td>ملاحظات العميل</td>
                        <td>{{$cart->notes}}</td>
                    </tr>
                    <tr>
                        <td>نوع الدفع</td>
                        <td>{{$cart->payment=='electronic'?'الكتروني':'كاش'}}</td>
                    </tr>
                    @if($cart->payment=='electronic')
                        <tr>
                            <td>رقم فاتورة الدفع</td>
                            <td>{{$cart->payment_id}}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>تكلفة التوصيل</td>
                        <td>{{$cart->delivery_cost}}</td>
                    </tr>
                    <tr>
                        <td>الخصم</td>
                        <td>{{$cart->discount}}</td>
                    </tr>
                    <tr>
                        <td>الاجمالي النهائي</td>
                        <td>{{$cart->grand_total}}</td>
                    </tr>
                </table>

                <div class="aligne-center">
                    <a class="btn btn-primary waves-effect print-nw button-print" target="_blank" href="{{route('admin.order.print',$cart->id)}}" id="print-bill">طباعة الفاتورة
                    </a>
                </div>

                <div class="body">
                    @if($cart->logs)
                    <table class="dttable display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الحالة</th>
                            <th>التاريخ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($cart->logs as $log)
                            <tr>
                                <td>{{++$loop->index}}</td>
                                <td>{{__($log->status)}}</td>
                                <td>{{$log->created_at}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    @endif
                    <table class="dttable display nowrap">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>اسم المنتج</th>
                                <th>مقاس المنتج</th>
                                <th>اللون</th>
                                <th>الكمية</th>
                                <th>السعر</th>
                                <th>الخصم</th>
                                <th>الاجمالي</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($cart->items as $item)
                            <tr>
                                <td>{{++$loop->index}}</td>
                                <td>{{$item->product->name}}</td>
                                <td>{{$item->size->name}}</td>
                                <td>{{optional($item->color)->name}}</td>
                                <td>{{$item->quantity}}</td>
                                <td>{{$item->price}}</td>
                                <td>{{$item->discount}}</td>
                                <td>{{$item->total}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

    @endsection

    @push('scripts')

    <!--  -->
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script>
        $(document).ready(function() {
            var table = $('.dttable').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'searching': true
            });

            $('#accept').click(function(e) {
                e.preventDefault();
                swal('تأكيد', '#accept')
            });
            $('#reject').click(function(e) {
                e.preventDefault();
                console.log('test')
                Swal.fire({
                    title: "هل انت متأكد ؟",
                    text: "هل تريد إلغاء هذا الطلب ؟",
                    icon: 'question',
                    input: "text",
                    inputAttributes: {
                        autocapitalize: 'off',
                        placeholder: "سبب الرفض",
                        name: "reject_reason"
                    },
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'موافق !',
                    cancelButtonText: 'الغاء'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var url = $('#reject').attr('href');
                        if (result.value != '') {
                            url += '?reject_reason=' + encodeURI(result.value);
                        }
                        console.log(url)
                        document.location.href = url;
                    }
                });

            });

            $('#ready').click(function(e) {
                e.preventDefault();
                swal('تأكيد تجهيز', '#ready')
            });

        });

        function swal(text, selector) {
            Swal.fire({
                title: "هل أنت متأكد ",
                text: "هل تريد " + text + " هذا الطلب ؟",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'موافق !',
                cancelButtonText: 'الغاء'
            }).then((result) => {
                if (result.value) {
                    document.location.href = $(selector).attr('href');
                }
            });

        }
    </script>

    @endpush
