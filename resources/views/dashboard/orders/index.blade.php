@extends('dashboard.layout.app')

@section('title')
    الطلبات
@endsection
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->

    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        تصفية الطلبات
                    </h2>
                </div>
                <div class="body">
                    {!! Form::open(['route'=>'admin.orders.index','method'=>'GET']) !!}
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">المندوب</label>
                                {!! Form::select('delivery_id', deliveries() , request('delivery_id') , ['class' => 'form-control select2 ','placeholder'=>'اختار المندوب']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">الاسرة</label>
                                {!! Form::select('family_id', families() , request('family_id') , ['class' => 'form-control select2 ','placeholder'=>'اختار الاسرة']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-label">العميل</label>
                                {!! Form::select('user_id', users() , request('user_id') , ['class' => 'form-control select2 ','placeholder'=>'اختار العميل']) !!}
                                <div class="form-line"></div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label class="form-label">الكوبون</label>
                                {!! Form::select('coupon_id', coupons() , null , ['class' => 'form-control select2 ','placeholder'=>'اختار الكوبون']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">الحالة</label>
                                {!! Form::select('status[]', orderStatuses() , null , ['class' => 'form-control select2 ','multiple']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group ">
                                <label class="form-label">من</label>
                                {!! Form::date('from',null,['class'=>'form-control']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="form-label">الي</label>
                                {!! Form::date('to',null,['class'=>'form-control']) !!}
                                <div class="form-line">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="form-label"></label>
                        {!! Form::submit('بحث',['class'=>'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل الطلبات
                    </h2>
                </div>
                <div class="body">
                    {{$dataTable->table()}}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    @include('components.yajra-datatable-scripts')

@endpush
