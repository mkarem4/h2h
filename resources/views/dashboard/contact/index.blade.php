@extends('dashboard.layout.app')

@section('title')
    كل رسائل العملاء
@endsection
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        كل رسائل العملاء
                    </h2>

                </div>
                <div class="body">
                    <table class="table display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>الإسم</th>
                            <th>الرسالة</th>
                            <th>البريد الإلكتروني</th>
                            <th>رقم الجوال</th>
                            <th>التاريخ</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($contacts as $key=>$contact)
                            <tr>
                                <td>{{++$key}}</td>
                                <td>{{$contact->name}}</td>
                                <td><a href="#"  data-text="{{$contact->message}}" onclick="popup(this)" class="message">{{\Illuminate\Support\Str::limit($contact->message,50,'...')}}</a></td>
                                <td>{{$contact->email}}</td>
                                <td>{{$contact->phone}}</td>

                                <td>{{$contact->created_at . ' || '.$contact->created_at->diffForHumans()}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    <!-- Jquery DataTable Plugin Js -->
    @include('dashboard.layout.datatable-scripts')
    <script>

        function popup(e){
            Swal.fire({
                icon:'info',
                text:$(e).data('text')
            })
        }
       /* $(document).ready(function (){
            $('.message').on('click',function (){
                Swal.fire({
                    icon:'info',
                    text:$(this).data('text')
                })
            });
        });*/
    </script>
@endpush
