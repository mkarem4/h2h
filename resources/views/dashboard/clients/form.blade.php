
<div class="form-group  ">
    <label class="form-label">الإسم</label>
    <div class="form-line">
        {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'  الاسم  '])!!}
    </div>
</div>

<div class="form-group form-float">
    <label class="form-label">رقم التيلفون</label>
    {!! Form::tel("phone",null,['class'=>'form-control','placeholder'=>'  رقم التيلفون '])!!}
    <div class="form-line">
    </div>
</div>
<div class="form-group form-float">
    <label class="form-label">الإيميل</label>
    {!! Form::email("email",null,['class'=>'form-control','placeholder'=>'  الايميل '])!!}
    <div class="form-line">
    </div>
</div>


<div class="form-group form-float">
    <label class="form-label">كلمة المرور</label>
    {!! Form::password('password',['class'=>'form-control','placeholder'=>'كلمة المرور']) !!}
    <div class="form-line">
    </div>
</div>

<div class="form-group form-float">
    <label class="form-label">تكرار كلمة المرور</label>
    {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'تكرار كلمة المرور']) !!}
    <div class="form-line">
    </div>
</div>
<x-File title="الصورة الشخصية" name="image"></x-File>
@if (isset($user) and $user->image)
    <a href="{{$user->image}}" target="_blank"><img height="100" width="100" class="thumbnail" src="{{getImg($user->image)}}" alt=""></a>
@endif


<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
