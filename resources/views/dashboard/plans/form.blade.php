<div class="row">
    <div class="col-md-6">
        <div class="form-group ">
            <label class="form-label">الاسم </label>
            <div class="form-line">
                {!! Form::text("name",null,['class'=>'form-control','placeholder'=>'اسم الباقة'])!!}
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="form-label">الوصف </label>
            <div class="form-line">
                {!! Form::text("description",null,['class'=>'form-control','placeholder'=>'وصف الباقة'])!!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group ">
            <label class="form-label">مدة الاشتراك بالايام</label>
            <div class="form-line">
                {!! Form::number("invoice_period",null,['class'=>'form-control','placeholder'=>'مدة الاشتراك بالايام'])!!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group ">
            <label class="form-label">سعر الاشتراك </label>
            <div class="form-line">
                {!! Form::number("price",null,['class'=>'form-control','placeholder'=>'سعر الاشتراك'])!!}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group ">
            <label class="form-label">الترتيب </label>
            <div class="form-line">
                {!! Form::number("sort_order",null,['class'=>'form-control','placeholder'=>'الترتيب'])!!}
            </div>
        </div>
    </div>
</div>

<button class="btn btn-primary waves-effect" type="submit">حفظ</button>

