@extends('dashboard.layout.app')

@section('title')
تعديل الباقة
{{ $plan->name }}
@endsection
@section('header')

@endsection

@section('content')

<!-- Basic Validation -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card animated slideInUp">
      <div class="header">
        <h2>تعديل الباقة       {{ $plan->name }}</h2>
        <ul class="header-dropdown m-r--5">
            <a href="{{route('admin.plans.index')}}">   <button class="btn btn-danger">كل الباقات</button></a>
         </ul>
      </div>
      <div class="body">
        {!!Form::model($plan , ['route' => ['admin.plans.update' , $plan->id] , 'files'=>true,'method' => 'PATCH']) !!}
        @include('dashboard.plans.form')
        {!!Form::close() !!}
      </div>
    </div>
  </div>
</div>


<!-- #END# Basic Validation -->
@endsection

@section('footer')

@endsection
