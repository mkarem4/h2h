@extends('dashboard.layout.app')

@section('title')
    سجل حركات {{$user->name}}
@endsection
@section('header')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css
"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css
"/>
@endsection
@section('content')
    <!-- Exportable Table -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="header">
                    <h2>
                        سجل حركات ||{{$user->name}}
                    </h2>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="body">
                   {!! Form::open(['route'=>['admin.transactions.add','id'=>$user->id,'type'=>$type],'method'=>'POST']) !!}
                    <div class="form-group form-float">
                        <label class="form-label"> مبلغ العملية</label>
                        {!! Form::number('amount',null,['class'=>'form-control','placeholder'=>'الرصيد']) !!}
                        <div class="form-line">
                        </div>
                    </div>
                    <div class="form-group form-float">
                        <label class="form-label">وصف العملية</label>
                        {!! Form::text('info',null,['class'=>'form-control','placeholder'=>'الرصيد']) !!}
                        <div class="form-line">
                        </div>
                    </div>
                    <div class="form-group ">
                        <label class="form-label">نوع العملية</label>
                        {!! Form::select('type',['deposit'=>'اضافة','withdraw'=>'خصم'],null,['class'=>'form-control','placeholder'=>'نوع العملية']) !!}
                        <div class="form-line">
                        </div>
                    </div>
                    <button class="btn btn-primary waves-effect" type="submit">حفظ</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card animated slideInUp">
                <div class="body">
                    <table class="table display nowrap">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>المبلغ</th>
                            <th>النوع</th>
                            <th>الوصف</th>
                            <th>التاريخ و الوقت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($user->transactions as $key=>$transaction)
                            <tr>
                                <td>{{$transaction->id}}</td>
                                <td>{{$transaction->amount}}</td>
                                <td>{{$transaction->type=='deposit'?'ايداع':'سحب'}}</td>
                                <td>{{$transaction->info}}</td>
                                <td>{{$transaction->created_at->toDateTimeString()}}</td>
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Exportable Table -->

@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('.table').DataTable({
                responsive: true,
                dom: 'Bfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                'searching': true
            });
        });
    </script>
    @include('dashboard.layout.datatable-scripts')

@endpush
