<div class="col-md-6">
    <div class="form-group ">
        <label class="form-label">{{$title}}</label>
        {!! Form::number($name,null,['class'=>'form-control','placeholder'=>$title,'step'=>0.01])!!}
        <div class="form-line">
        </div>
    </div>

</div>
