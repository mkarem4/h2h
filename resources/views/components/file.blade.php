<div class="form-group {{$is_float?'form-float':''}}">
    <label class="form-label">{{$title}}</label>
    {!! Form::file($name,['class'=>'form-control'])!!}
    <div class="form-line">
    </div>
</div>
