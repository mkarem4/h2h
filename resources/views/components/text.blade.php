<div class="col-md-{{$float?'6':'12'}}">
    <div class="form-group ">
        <label class="form-label">{{$title}}</label>
        {!! Form::text($name,null,['class'=>'form-control','placeholder'=>$title])!!}
        <div class="form-line">
        </div>
    </div>
</div>
