<div class="col-md-12">
    <div class="form-group">
        <label class="form-label">{{$title}}</label>
        {!! Form::textarea($name,null,['class'=>'form-control','placeholder'=>$title])!!}
        <div class="form-line">
        </div>
    </div>
</div>
