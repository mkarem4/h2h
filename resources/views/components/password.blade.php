<div>
    <div class="col-md-6">
        <div class="form-group ">
            <label class="form-label">كلمة المرور</label>
            {!! Form::password('password',['class'=>'form-control','placeholder'=>'كلمة المرور']) !!}
            <div class="form-line">
            </div>
        </div>
    </div>
    <div class="col-md-6">

        <div class="form-group ">
            <label class="form-label">تكرار كلمة المرور</label>
            {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'تكرار كلمة المرور']) !!}
            <div class="form-line">
            </div>
        </div>
    </div>
</div>
