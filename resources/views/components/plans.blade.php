<div class="col-md-12">
   <div class="col-md-6">
       <div class="form-group">
           <label class="form-label">الباقة</label>
           {!! Form::select('plan_id',$plans,null,['class'=>'form-control ms select2 ','placeholder'=>'اختار الباقة'])!!}
           <div class="form-line">
           </div>
       </div>
   </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="form-label">تاريخ بداية الاشتراك</label>
            {!! Form::date('start_subscription_at',null,['class'=>'form-control ','placeholder'=>'تاريخ بداية الاشتراك'])!!}
            <div class="form-line">
            </div>
        </div>
    </div>
</div>
