<div class="col-md-{{$float?'6':'12'}}">
    <div class="form-group ">
        <label class="form-label">{{$title}}</label>
        {!! Form::select($name,$items,null,['class'=>'form-control select2','placeholder'=>$title])!!}
        <div class="form-line">
        </div>
    </div>
</div>
