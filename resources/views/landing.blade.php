<!DOCTYPE html>
<html lang="ar">

<head>
    <!------------------------Start-Layout------------------------>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>مؤسسة من البيت للبيت</title>
    <meta name="copyright" content="من البيت للبيت">
    <meta name="robots" content="index,follow">
    <meta name="owner" content="Ba9mah">
    <meta name="category" content="ecommrce">
    <meta name="author" content="من البيت للبيت">
    <meta name="description" content="توصيل الطعام,موقع من البيت للبيت,لطلب الاكل">
    <!------------------------Start-Favicon------------------------>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('landing/asseets/img/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{asset('landing/asseets/img/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('landing/asseets/img/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('landing/asseets/img/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('landing/asseets/img/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{asset('landing/asseets/img/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('landing/asseets/img/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{asset('landing/asseets/img/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('landing/landing/asseets/img/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192" href="{{asset('landing/asseets/img/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('landing/asseets/img/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{asset('landing/asseets/img/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('landing/asseets/img/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{asset('landing/asseets/img/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{asset('landing/asseets/img/favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <!------------------------End-Favicon------------------------>

    <!---------Plugins--------->
    <link rel="stylesheet" href="{{asset('landing/asseets/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/asseets/css/animate.4.0.0.css')}}">
    <link rel="stylesheet" href="{{asset('landing/asseets/css/swiper-bundle.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/asseets/css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('landing/asseets/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('landing/asseets/scss/main.css')}}">
    <!------------------------End-Layout------------------------>
</head>

<body>

<!------------------------Start-Navbar-Section------------------------>
<nav>
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center">
            <a class="logo" href="#">
                <img src="{{asset('landing/asseets/img/h2h_logo.jpg')}}" alt="logo">
            </a>

            <div>
                <a href="{{route('web.add_family')}}" class="login-btn">سجل كأسرة</a>
            </div>


            <div>
                <a href="{{route('family.login')}}" class="login-btn">تسجيل الدخول</a>
            </div>
        </div>
    </div>
</nav>
<!------------------------End-Navbar-Section------------------------>

<!------------------------Start-header-Section------------------------>
<header class="all">
    <div class="row d-flex justify-content-center align-items-center px-5 no-margin">
        <div class="swiper col-md-12 ">
            <!-- Slider main container -->
            <div class="swiper-container">
                <!-- Additional required wrapper -->
                <div class="swiper-wrapper">
                    <!-- Slides -->
                    <div class="swiper-slide">
                        <img src="{{asset('landing/asseets/img/slide1.jpeg')}}" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="swiper-slide">
                        <img src="{{asset('landing/asseets/img/slide2.jpeg')}}" alt="">
                        <div class="overlay"></div>
                    </div>
                    <div class="swiper-slide">
                        <img src="{{asset('landing/asseets/img/slide3.jpeg')}}" alt="">
                        <div class="overlay"></div>
                    </div>
                </div>
                <!--Slide captions-->
                <div class="slide-captions wow animate__fadeInRight" data-wow-delay="0.3s"></div>

                <!--pagination-->
                <div class="swiper-pagination"></div>
            </div>
        </div>
    </div>
</header>
<!------------------------End-Header-Section------------------------>

<!------------------------Start-Download-Section------------------------>
<section class="download all">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 right-download wow animate__fadeInRight" data-wow-delay="0.5s"
                 data-wow-duration="2s">
                <h1>تطبيق الجوال</h1>
                <p>حمل تطبيق من البيت للبيت على جوالك واطلب بكل سهولة ليصلك فورا الى بيتك</p>
                <a href="https://apps.apple.com/us/app/%D9%85%D8%AA%D8%AC%D8%B1-%D8%A8%D8%B5%D9%85%D8%A9/id1545731020?app=itunes&ign-mpt=uo%3D4" rel="noopener" target="_blank"
                   class="download-btn"><img src="{{asset('landing/asseets/img/appstore.svg')}}" alt="img"></a>
                <a href="https://play.google.com/store/apps/details?id=com.elkrnshawy.basma_user" rel="noopener" target="_blank"
                   class="download-btn"><img src="{{asset('landing/asseets/img/google-play.svg')}}" alt="img"></a>
            </div>
            <div class=" col-md-6 col-12 left-download wow animate__fadeInUp" data-wow-delay="0.7s"
                 data-wow-duration="2s">
                <img src="{{asset('landing/asseets/img/mobile-bg.png')}}" alt="mobile">
                <img class="logo" src="{{asset('landing/asseets/img/h2h_logo.jpg')}}" alt="logo">
            </div>
        </div>
    </div>
</section>
<!------------------------End-Download-Section------------------------>

<!------------------------Start-About-Section------------------------>
<section class="about all">
    <div class="row">
        <div class="col-md-12 about-desc text-center">
            <h1 class="wow animate__backInDown" data-wow-delay="0.3s">من نحن<i
                    class="fas fa-question fa-flip-horizontal"></i></h1>
            <p class="wow animate__zoomInUp" data-wow-delay="0.6s">متجر متخصص فى جمع الاسر المنتجة لتحقيق هدف الوصول
                لاشباع رغبة العميل والسعى لخدمة المجتمع</p>
        </div>
    </div>
</section>
<!------------------------End-About-Section------------------------>

<!------------------------Start-Footer-Section------------------------>
<footer>
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center">
            <ul>
                <li><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                <li><a href="#"><i class="fab fa-instagram"></i></a></li>
            </ul>
            <p><span>&copy;</span> developed by <a style="color: #db5969   " href="mailto:info@se01.tech">se01.tech</a></p>
        </div>
    </div>
</footer>
<!------------------------End-Footer-Section------------------------>

<!------------------------Start-Scripts------------------------>
<script src="{{asset('landing/asseets/js/jquery-2.1.4.min.js')}}"></script>
<script src="{{asset('landing/asseets/js/bootstrap.min.js')}}"></script>
<script src="{{asset('landing/asseets/js/swiper-bundle.min.js')}}"></script>
<script src="{{asset('landing/asseets/js/wow.min.js')}}"></script>
<script src="{{asset('landing/asseets/js/main.js')}}"></script>
<!------------------------End-Scripts------------------------>
</body>

</html>
