<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>مؤسسة من البيت للبيت التجارية || استعادة كلمة السر</title>
    <!-- Favicon-->
    <link rel="icon" href="{{asset('favicon.ico')}}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{asset('_dashboard/plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{asset('_dashboard/plugins/node-waves/waves.css')}}" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="{{asset('_dashboard/plugins/animate-css/animate.css')}}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{asset("reset-password/css/style.css")}}" rel="stylesheet">

</head>

<body class="signup-page">
<div class="signup-box">
    <div class="logo">
        <a href="javascript:void(0);">متجر من البيت للبيت</a>
        <small>مؤسسة من البيت للبيت التجارية</small>
    </div>
    <div class="card">
        <div class="body">
            <form id="sign_up" action="{{route('password.update')}}" method="POST">
                @csrf
                {!! Form::hidden('token',request('token')) !!}
                <div class="msg">استعادة كلمة السر</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line @error('email') error @enderror">
                        {!! Form::email('email',request('email'),['class'=>'form-control','placeholder'=>'البريد الالكتروني']) !!}
                    </div>
                    @error('email')
                    <label id="email-error" class="error" for="email">{{$message}}</label>
                    @enderror
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line  @error('password') error @enderror">
                        {!! Form::password('password',['class'=>'form-control','placeholder'=>'كلمة المرور']) !!}
                    </div>
                    @error('password')
                    <label id="password-error" class="error" for="password">{{$message}}</label>
                    @enderror
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line  @error('password_confirmation') error @enderror">
                        {!! Form::password('password_confirmation',['class'=>'form-control','placeholder'=>'تأكيد كلمة المرور']) !!}
                    </div>
                    @error('password_confirmation')
                    <label id="passowrd-confirmation-error" class="error" for="passowrd-confirmation">{{$message}}</label>
                    @enderror
                </div>

                <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">حفظ</button>

            </form>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="{{asset('_dashboard/plugins/jquery/jquery.min.js')}}"></script>

<!-- Bootstrap Core Js -->
<script src="{{asset('_dashboard/plugins/bootstrap/js/bootstrap.js')}}"></script>

<!-- Waves Effect Plugin Js -->
<script src="{{asset('_dashboard/plugins/node-waves/waves.js')}}"></script>

<!-- Validation Plugin Js -->
<script src="{{asset('_dashboard/plugins/jquery-validation/jquery.validate.js')}}"></script>

<!-- Custom Js -->
<script src="{{asset("_dashboard/js/admin.js")}}"> </script>
<script src="{{asset("_dashboard/js/pages/examples/sign-up.js")}}"> </script>
</body>

</html>
