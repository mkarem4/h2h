<?php

namespace Tests\Feature\Http\Controllers;

use App\Delivery;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\DeliveryController
 */
class DeliveryControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $deliveries = Delivery::factory()->count(3)->create();

        $response = $this->get(route('delivery.index'));

        $response->assertOk();
        $response->assertViewIs('dashboard.deliveries.index');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('delivery.create'));

        $response->assertOk();
        $response->assertViewIs('dashboard.deliveries.add');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\DeliveryController::class,
            'store',
            \App\Http\Requests\DeliveryStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $name = $this->faker->name;
        $phone = $this->faker->phoneNumber;
        $password = $this->faker->password;
        $email = $this->faker->safeEmail;
        $image = $this->faker->text;

        $response = $this->post(route('delivery.store'), [
            'name' => $name,
            'phone' => $phone,
            'password' => $password,
            'email' => $email,
            'image' => $image,
        ]);

        $deliveries = Delivery::query()
            ->where('name', $name)
            ->where('phone', $phone)
            ->where('password', $password)
            ->where('email', $email)
            ->where('image', $image)
            ->get();
        $this->assertCount(1, $deliveries);
        $delivery = $deliveries->first();

        $response->assertRedirect(route('delivery.index'));
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $delivery = Delivery::factory()->create();

        $response = $this->get(route('delivery.edit', $delivery));

        $response->assertOk();
        $response->assertViewIs('dashboard.deliveries.edit');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\DeliveryController::class,
            'update',
            \App\Http\Requests\DeliveryUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_saves_and_redirects()
    {
        $delivery = Delivery::factory()->create();
        $name = $this->faker->name;
        $phone = $this->faker->phoneNumber;
        $password = $this->faker->password;
        $email = $this->faker->safeEmail;
        $image = $this->faker->text;

        $response = $this->put(route('delivery.update', $delivery), [
            'name' => $name,
            'phone' => $phone,
            'password' => $password,
            'email' => $email,
            'image' => $image,
        ]);

        $deliveries = Delivery::query()
            ->where('name', $name)
            ->where('phone', $phone)
            ->where('password', $password)
            ->where('email', $email)
            ->where('image', $image)
            ->get();
        $this->assertCount(1, $deliveries);
        $delivery = $deliveries->first();

        $response->assertRedirect(route('delivery.index'));
        $response->assertSessionHas('delivery.id', $delivery->id);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $delivery = Delivery::factory()->create();

        $response = $this->delete(route('delivery.destroy', $delivery));

        $response->assertRedirect(route('post.index'));

        $this->assertDeleted($delivery);
    }
}
