let map;
let marker;

function initMap() {

    let lat = parseFloat(document.getElementById("lat").value);
    let lng = parseFloat(document.getElementById("lng").value);
    let zoom = 13;
    var location = {lat: lat, lng: lng};
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: zoom,
        center: location,
        mapTypeId: "terrain"
    });
    // The marker, positioned at Uluru
    marker = new google.maps.Marker({
        position: location,
        map: map,
        draggable: true,
    });


    google.maps.event.addListener(map, "click", updateLocation);

    google.maps.event.addListener(marker, "dragend", updateLocation);
    google.maps.event.addListener(marker, "drag", updateLocation);

    function updateLocation(event) {
        // get lat/lon of click
        var clickLat = event.latLng.lat();
        var clickLon = event.latLng.lng();

        // show in input box
        document.getElementById("lat").value = clickLat;
        document.getElementById("lng").value = clickLon;
        // document.getElementById("radius").value = 20;
        /*******************/
        //GetAddress(clickLat.toFixed(7), clickLon.toFixed(7));
        /****************/
        var latlng = new google.maps.LatLng(clickLat, clickLon);
        marker.setPosition(latlng);

    }

}




