$(document).ready(function () {
    $("#add").click(function () {
        var lastField = $("#multi .fieldwrapper:last");
        var intId = (lastField && lastField.length && parseInt(lastField.attr("data-idx")) + 1) || 1;
        var fieldWrapper = $(`<div class="fieldwrapper row" id="field${intId}"/>`);
        fieldWrapper.attr("data-idx", intId);

        var products_id = $(`  <div class="col-sm-4 pull-right">
                <div class="form-group">
                    <label class="form-label">الصنف</label>
                    <div class="form-line">
                        <select name="products[${intId}][product_id]" id="select2-${intId}" required class="form-control select2 ms show-tick">
                            <option selected>اختار المنتج</option>
                            ${productsOptions}
                        </select>
                    </div>
                </div>
            </div>`);
        var fQuantity = $(`<div class="col-sm-4 pull-right">
                <div class="form-group">
                    <label class="form-label">الكمية</label>
                    <div class="form-line">
                        <input type="number" min="1" value="1" class="form-control quantity input" required name="products[${intId}][quantity]">
                    </div>
                </div>
            </div>`);
        var fPrice = $(`<div class="col-sm-3 pull-right">
                <div class="form-group">
                    <label class="form-label">السعر</label>
                    <div class="form-line">
                        <input type="number" min="0" class="form-control price input" id="price-${intId}" required name="products[${intId}][price]">
                    </div>
                </div>
            </div>`);
        var removeButton = $(`<div class="col-sm-1 pull-right">
                <div class="form-group">
                <button class="btn btn-danger remove"><i class="fa fa-trash"></i></button>
                </div>
            </div>`);
        removeButton.click(function () {
            $(this).parent().remove();
        });
        fQuantity.change(updateTotal);
        fPrice.change(updateTotal)
        fieldWrapper.append(products_id);
        fieldWrapper.append(fQuantity);
        fieldWrapper.append(fPrice);
        fieldWrapper.append(removeButton);
        $("#multi").append(fieldWrapper);


    });
});

