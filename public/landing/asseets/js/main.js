/***************Start-Swiper-JS***************/
var mySwiper = new Swiper(".swiper-container", {
  direction: "horizontal",
  loop: true,
  speed: 1200,
  grabCursor: true,
  rtl: true,
  centeredSlides: true,
  autoplay: {
    delay: 3500,
    disableOnInteraction: false,
  },
  pagination: {
    el: ".swiper-pagination",
    type: "bullets",
    clickable: true,
  },
});
/***************End-Swiper-JS***************/

/***************Start-WoW-JS***************/
new WOW().init();
/***************End-WoW-JS***************/
